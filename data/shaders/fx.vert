#version 140

layout(std140) uniform;

in vec3 position;
in vec2 texCoord;

uniform float alphaTime;
uniform int texScale;

smooth out vec2 colorCoord;
smooth out vec3 cameraSpacePosition;

uniform Projection
{
	mat4 Pmatrix; // cameraToClip
};

uniform mat4 MVmatrix; // worldToCamera * modelToWorld (model to camera) with already removed rotation in view transformation (causes effect always face to camera)

void main()
{
	int texNum = int(alphaTime * float(texScale * texScale));
	colorCoord = texCoord / float(texScale);
	int texX = texNum % texScale;
	int texY = texNum / texScale;
	colorCoord.x += texX * (1.f / texScale);
	colorCoord.y += texY * (1.f / texScale);
	cameraSpacePosition = vec3(MVmatrix  * vec4(position, 1.f));
	gl_Position = Pmatrix * vec4(cameraSpacePosition, 1.f);

}
