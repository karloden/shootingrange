#version 140

const float maxFogIntensity = 0.02f;

layout(std140) uniform;

smooth in vec2 colorCoord;
smooth in vec3 cameraSpacePosition;

out vec4 outputColor;


uniform LightCorrection
{
	vec3 ambientIntensity;
	float maxIntensity;
	vec3 fogColor;
	float fogIntensity;
	float gamma;
} LgtCor;

//takes final calculated color and adds fog to it, interpolating fog color with color using exponential square fog alpha
vec3 lerpFog(vec3 finalColor)
{
	//distance of skybox is very large, so we must divide it to get not absolutely fogged sky
	float dist = length(cameraSpacePosition) / 5.f;
	float fogLerpAlpha = 1.0f / exp((dist * LgtCor.fogIntensity) * (dist * LgtCor.fogIntensity));
	fogLerpAlpha = clamp(fogLerpAlpha, 0.f, 1.f);
	return mix(LgtCor.fogColor, finalColor, fogLerpAlpha);
}

uniform sampler2D textureDiffuseColor;

void main()
{
	outputColor = texture(textureDiffuseColor, colorCoord);
	float alphaChannel = outputColor.a;
	vec3 gammaVec = vec3(LgtCor.gamma);
	outputColor = vec4(pow(lerpFog(outputColor.rgb), gammaVec), alphaChannel);
}