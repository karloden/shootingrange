#version 140

layout(std140) uniform;

in vec3 position;
in vec2 texCoord;

smooth out vec2 colorCoordBig;
smooth out vec2 colorCoordSmall;
smooth out vec2 colorCoordStars;
smooth out vec3 cameraSpacePosition;

//clamped [0,1] cos between camera vector and vector pointing down
smooth out float cloudLerpAlpha;
//clamped [0,1] value used to separate horizon from zenith
smooth out float horizonLerpAlpha;
smooth out vec3 cameraSpaceMoonDir;
smooth out vec3 cameraSpaceSunDir;


uniform mat3 Vmatrix; // worldToCamera with already removed camera translation


uniform SkyBoxData
{
	vec3 sunDirection;
	float time;
	vec3 zenithColor;
	float cloudSpeed;
	vec3 horizonColor;
	float cloudOpacity;
	vec3 cloudColor;
	float horizonFalloff;
	vec3 moonDirection;
	float starsBrightness;
}SkyBox;

uniform Projection
{
	mat4 Pmatrix; // cameraToClip
};



void main()
{
	vec4 tempCamPosition = mat4(Vmatrix) * vec4(position, 1.f);
	cameraSpacePosition = tempCamPosition.xyz;

	//cos between camera vector and vector pointing down
	float cosCameraYAxis = dot(normalize(-cameraSpacePosition), normalize(Vmatrix * vec3(0.f, -1.f, 0.f)));
	cloudLerpAlpha = clamp(cosCameraYAxis, 0.f, 1.f);

	//calculating horizon value, that will be used for lerping horizon and zenith
	horizonLerpAlpha = clamp(pow(1.f - cloudLerpAlpha, SkyBox.horizonFalloff), 0.f, 1.f);
	cameraSpaceMoonDir = Vmatrix * SkyBox.moonDirection;
	cameraSpaceSunDir = Vmatrix * SkyBox.sunDirection;
	colorCoordStars = colorCoordBig = colorCoordSmall = texCoord;
	colorCoordBig.x += SkyBox.time * SkyBox.cloudSpeed;
	colorCoordSmall.x += SkyBox.time * SkyBox.cloudSpeed * 5.f;
	gl_Position = Pmatrix * tempCamPosition;
}