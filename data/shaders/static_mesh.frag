#version 140
#define MAX_LIGHT_SOURCES 20

const uint Directional  = 0x00000001u;
const uint Point		= 0x00000002u;
const uint Spot			= 0x00000004u;
const uint LgtTypeMask	= 0x0000000fu;
const uint Phong		= 0x00000010u;
const uint Gauss		= 0x00000020u;
const uint SpecMask		= 0x000000f0u;

const float maxFogIntensity = 0.02f;

layout(std140) uniform;

//TBN matrix (tangents bitangents normal) in camera space, every value from normal map must be right multiplied with this
smooth in mat3 TBN;
smooth in vec3 cameraSpaceNormal;
smooth in vec3 cameraSpacePosition;
smooth in vec2 colorCoord;


out vec4 outputColor;

//After vec3 must be padding in C++ struct version, because GLSL works with vec4
uniform Material
{
	vec3 diffuseColor;
	vec3 ambientColor;
	vec3 specularColor;
	float specularShininess;
} Mtl;

//After vec3 goes value of size 32, so no need to use padding
uniform LightCorrection
{
	vec3 ambientIntensity;
	float maxIntensity;
	vec3 fogColor;
	float fogIntensity;
	float gamma;
} LgtCor;

//After vec3 goes value of size 32, so no need to use padding
struct LightSource
{
	vec3 camSpLightPos;
	float lightIntensity;
	vec3 camSpLightDir;
	float lightAttenuation;
	vec3 lightColor;
	uint type;
}; 

uniform LightSources
{

	LightSource light[MAX_LIGHT_SOURCES];
	int lightsNum;
};

uniform sampler2D textureDiffuseColor;
uniform sampler2D normalMap;


float computePhong(in LightSource lgtSrc, in vec3 normNormal, in float cosAngFall)
{
	vec3 cameraDirection = normalize(-cameraSpacePosition);
	vec3 reflectDirection = reflect(-lgtSrc.camSpLightDir, normNormal);
	float phongTerm = max(dot(cameraDirection, reflectDirection), 0.f);
	phongTerm = pow(phongTerm, Mtl.specularShininess);

	return phongTerm;
}

float computeGauss(in LightSource lgtSrc, in vec3 normNormal, in float cosAngFall)
{
	vec3 cameraDirection = normalize(-cameraSpacePosition);
	vec3 halfAngle = normalize(lgtSrc.camSpLightDir + cameraDirection);
	float angleNormalHalf = acos(dot(halfAngle, normNormal));
	float exponent = angleNormalHalf / Mtl.specularShininess;
	exponent = -(exponent * exponent);
	float gaussianTerm = exp(exponent);

	return  gaussianTerm;
}

vec3 calculateLightComp(in LightSource lgtSrc, in vec3 diffuseColor){
	vec3 normNormal = texture(normalMap, colorCoord).rgb;
	//If normal map texel is not undefined (z coordinate, blue channel, cannot be less than 0.5) use normal map normal
	if(normNormal.b >= 0.5f)
	{
		normNormal = normalize(normNormal * 2.0 - 1.0);
		normNormal = normalize(TBN * normNormal); 
	}
	else normNormal = normalize(cameraSpaceNormal);
	float cosAngFall = dot(normNormal, lgtSrc.camSpLightDir);
	cosAngFall = max(cosAngFall, 0.0f);
	float specTerm = 0.f;
	if(cosAngFall != 0.f)
		switch(lgtSrc.type & SpecMask)
		{
			case Phong:
				specTerm = computePhong(lgtSrc, normNormal, cosAngFall);
				break;
			case Gauss:
				specTerm = computeGauss(lgtSrc, normNormal, cosAngFall);
				break;
		}
	float lightStrength = lgtSrc.lightAttenuation * lgtSrc.lightIntensity;
	return diffuseColor * lightStrength * cosAngFall * lgtSrc.lightColor
			 + Mtl.specularColor * lightStrength * specTerm;
}

//takes final calculated color and adds fog to it, interpolating fog color with color using exponential square fog alpha
vec3 lerpFog(vec3 finalColor)
{
	float dist = length(cameraSpacePosition);
	float fogLerpAlpha = 1.0f / exp((dist * LgtCor.fogIntensity) * (dist * LgtCor.fogIntensity));
	fogLerpAlpha = clamp(fogLerpAlpha, 0.f, 1.f);
	return mix(LgtCor.fogColor, finalColor, fogLerpAlpha);
}

void main(){
	vec4 textureColor = texture(textureDiffuseColor, colorCoord);
	if(textureColor.a < 0.01f) discard;
	vec3 diffuseColor = textureColor.rgb + vec3(Mtl.diffuseColor);
	vec3 accumLgt;
	for(int lgt = 0; lgt < lightsNum; lgt++)
	{
		vec3 lightDir;
		float lightAtten;
		LightSource preparedLight;
		preparedLight.lightIntensity = light[lgt].lightIntensity;
		preparedLight.lightColor = light[lgt].lightColor;
		preparedLight.type = light[lgt].type;
		switch(light[lgt].type & LgtTypeMask)
		{
			case Directional: 
				preparedLight.camSpLightDir = light[lgt].camSpLightDir;
				preparedLight.lightAttenuation = 1.f;
				break;
			case Point: 
				vec3 lightDiff = light[lgt].camSpLightPos - cameraSpacePosition;
				float lightDistSqr = dot(lightDiff, lightDiff);
				preparedLight.camSpLightDir = normalize(lightDiff * inversesqrt(lightDistSqr));
				preparedLight.lightAttenuation = (1.f / ( 1.f + light[lgt].lightAttenuation * lightDistSqr));
				break;
			case Spot: 
				lightDiff = light[lgt].camSpLightPos - cameraSpacePosition;
				lightDistSqr = dot(lightDiff, lightDiff);
				preparedLight.camSpLightDir = normalize(lightDiff * inversesqrt(lightDistSqr));

				vec3 lightDir = normalize(cameraSpacePosition - light[lgt].camSpLightPos);
				float cosAngLgtToSurf = dot(lightDir, light[lgt].camSpLightDir);
				cosAngLgtToSurf = max(cosAngLgtToSurf, 0.0f);
				float outerAtten = light[lgt].lightAttenuation * 0.92f;
				preparedLight.lightAttenuation = clamp((cosAngLgtToSurf - outerAtten) / (light[lgt].lightAttenuation - outerAtten), 0.0f, 1.0f);   
				break;
		}
		accumLgt += calculateLightComp(preparedLight, diffuseColor);

	}
	
	accumLgt += diffuseColor * LgtCor.ambientIntensity * Mtl.ambientColor;

	accumLgt /= LgtCor.maxIntensity;
	vec3 gammaVec = vec3(LgtCor.gamma);

	outputColor = vec4(pow(lerpFog(accumLgt), gammaVec), textureColor.a);
}
