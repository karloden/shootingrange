#version 140

const float maxFogIntensity = 0.02f;

layout(std140) uniform;

smooth in vec2 colorCoordBig;
smooth in vec2 colorCoordSmall;
smooth in vec2 colorCoordStars;
smooth in vec3 cameraSpacePosition;
//clamped [0,1] cos between camera vector and vector pointing down
smooth in float cloudLerpAlpha;
//clamped [0,1] value used to separate horizon from zenith
smooth in float horizonLerpAlpha;
smooth in vec3 cameraSpaceMoonDir;
smooth in vec3 cameraSpaceSunDir;

out vec4 outputColor;

uniform sampler2D textureBigClouds;
uniform sampler2D textureSmallClouds;
uniform sampler2D textureStars;

uniform LightCorrection
{
	vec3 ambientIntensity;
	float maxIntensity;
	vec3 fogColor;
	float fogIntensity;
	float gamma;
} LgtCor;

uniform SkyBoxData
{
	vec3 sunDirection;
	float time;
	vec3 zenithColor;
	float cloudSpeed;
	vec3 horizonColor;
	float cloudOpacity;
	vec3 cloudColor;
	float horizonFalloff;
	vec3 moonDirection;
	float starsBrightness;
} SkyBox;

float getCloudStrength()
{
	//special texture with big clouds, that seems further in world, uses only blue channel 
	float bigCloudsMask = texture(textureBigClouds, colorCoordBig).b;

	//special texture with small clouds, that seems closer in world, uses only red channel 
	float smallCloudsMask = texture(textureSmallClouds, colorCoordSmall).r;

	//mixes 2 textures using cos between camera vector and vector pointing down
	float lerpedClouds = mix(bigCloudsMask, smallCloudsMask, cloudLerpAlpha);

	float zenithBrightness = clamp(((max(max(SkyBox.zenithColor.r, SkyBox.zenithColor.g), SkyBox.zenithColor.b)) * 2.f), 0.f, 1.f);

	//fixes clouds quantity depending on sky brightness
	float opacity = mix(SkyBox.cloudOpacity * 0.8f, SkyBox.cloudOpacity, zenithBrightness);

	//mixed clouds cannot be used directly due to we want to control cloud opacity 
	return mix(0.f, opacity, lerpedClouds);



}

vec3 getClearSkyColor()
{
	//texture function result can be only black or near-white, 
	//so multiplying it by var affects only places where stars are
	vec3 starComponent = texture(textureStars, colorCoordStars).xyz * SkyBox.starsBrightness;
	//so our stars look about same as without fog, creating emission illusion
	starComponent *= (LgtCor.fogIntensity / maxFogIntensity) * 0.9f + 1.f;
	//separates horizon from zenith
	return mix(SkyBox.zenithColor + starComponent, SkyBox.horizonColor, horizonLerpAlpha);
}

//simply creates circle, representing sun
vec3 getSunComponent()
{
	//so our sun look about same as without fog, creating emission illusion
	float cosSunCenter = dot(normalize(cameraSpaceSunDir), normalize(-cameraSpacePosition));
	return cosSunCenter > 0.999f ? vec3(1.f, 0.8f, 0.4f) * (LgtCor.fogIntensity / maxFogIntensity) * 0.9f + 1.f : vec3(0.f);
}

//simply creates circle, representing moon
vec3 getMoonComponent()
{
	//so our moon look about same as without fog, creating emission illusion
	float cosMoonCenter = dot(normalize(cameraSpaceMoonDir), normalize(-cameraSpacePosition));
	return cosMoonCenter > 0.999f ? vec3(1.f) * (LgtCor.fogIntensity / maxFogIntensity) * 0.9f + 1.f : vec3(0.f);
}

//takes final calculated color and adds fog to it, interpolating fog color with color using exponential square fog alpha
vec3 lerpFog(vec3 finalColor)
{
	//distance of skybox is very large, so we must divide it to get not absolutely fogged sky
	float dist = length(cameraSpacePosition) / 20.f;
	float fogLerpAlpha = 1.0f / exp((dist * LgtCor.fogIntensity) * (dist * LgtCor.fogIntensity));
	fogLerpAlpha = clamp(fogLerpAlpha, 0.f, 1.f);
	return mix(LgtCor.fogColor, finalColor, fogLerpAlpha);
}


void main()
{
	float cloudStr = getCloudStrength();
	vec3 cloudComponent = SkyBox.cloudColor * cloudStr;
	vec3 unfoggedColor = mix(getClearSkyColor() + getSunComponent() + getMoonComponent(), cloudComponent, 
						 clamp(cloudStr * cloudStr, 0.f, 1.f));

	vec3 gammaVec = vec3(LgtCor.gamma);
	outputColor = vec4(pow(lerpFog(unfoggedColor), gammaVec), 1.f);
	outputColor.w = 1.f;
}