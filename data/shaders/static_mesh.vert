#version 140

layout(std140) uniform;

in vec3 position;
in vec3 normal;
in vec2 texCoord;
in vec3 tangent;
in vec3 bitangent;  

//TBN matrix (tangents bitangents normal) in camera space, every value from normal map must be right multiplied with this
smooth out mat3 TBN;
smooth out vec3 cameraSpaceNormal;
smooth out vec3 cameraSpacePosition;
smooth out vec2 colorCoord;



uniform Projection
{
	mat4 Pmatrix; // cameraToClip
};

	uniform mat4 MVmatrix; // worldToCamera * modelToWorld (model to camera)
	uniform mat3 MVmatrixNormal; // transpose of inversed  model to camera matrix
//  mat4 MVPmatrix; // cameraToClip * worldToCamera * modelToWorld (model to clip)
//	uniform mat4 modelMatrix; // modelToWorldMatrix
//	mat4 viewMatrix; // worldToCameraMatrix
//	mat4 projMatrix; // cameraToClipMatrix

void main()
{
	vec4 tempCamPosition = (MVmatrix * vec4(position, 1.0));
	gl_Position = Pmatrix * tempCamPosition;
	cameraSpaceNormal = normalize(MVmatrixNormal * normal);
	cameraSpacePosition = tempCamPosition.xyz;
	colorCoord = texCoord;

	//TBN used for transforming normals from normal maps
	vec3 T = normalize(vec3(MVmatrix * vec4(tangent, 0.f)));
	vec3 B = normalize(vec3(MVmatrix * vec4(bitangent, 0.f)));
	vec3 N = normalize(vec3(MVmatrix * vec4(normal, 0.f)));
	TBN = mat3(T, B, N);
}