#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __DRAWABLES_H
#define __DRAWABLES_H

#include <string>
#include <vector>
#include <gl_core_4_4.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "pgr.h"
#include "misc.h"
#include "exceptions.h"
#include "mesh_geometry.h"
#include "world_objects.h"
#include "camera.h"



/**
* \brief Class represents static mesh in the scene, that cannot move, but has collision, can be picked etc.
*/
class StaticMesh : public AActorBase {
public:
	/**
	 * \brief constructor builds instance of static mesh
	* @param p_FileName specifies mesh filename
	* @param p_InitWorldPos init world position
	* @param p_InitScale init scale
	* @param p_InitRawDegrees init raw degrees of rotations along x,y,z axis in vector
	* @param p_MeshInd material submesh, that is expected to load from p_FileName, -1 for whole model
	*/
	explicit StaticMesh(const std::string &p_FileName
		, const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, const glm::vec3& p_InitRawDegrees
		, int32_t p_MeshInd = -1);

	/**
	 * \brief default destructor, frees mesh
	*/
	virtual ~StaticMesh() { free(); }

	/**
	* \brief frees and loads mesh once again
	*/
	void reset() final;

	/**
	* \brief StaticMeshes cannot be translated, does nothing
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override {}
	/**
	* \brief StaticMeshes cannot be translated, does nothing
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override {}
	/**
	* \brief StaticMeshes cannot be scaled, does nothing
	*/
	virtual void scale(const glm::vec3& p_NewScale) override {}
	/**
	* \brief StaticMeshes cannot be scaled, does nothing
	*/
	virtual void scaleRelative(const glm::vec3& p_NewScale) override {}
	/**
	* \brief StaticMeshes cannot be oriented, does nothing
	*/
	virtual void orient(const glm::fquat& p_NewOrientation) override {}
	/**
	* \brief StaticMeshes cannot be rotated, does nothing
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override {}
	/**
	 * \brief uses Flyweight loader to load mesh geometry from disk or use already resident in memory 
	*/
	virtual void load();
	/**
	* \brief uses Flyweight loader to decrease user number of mesh geometry and probably free mesh geometry connected
	*/
	virtual void free();
	/**
	* \brief renders mesh using MatrixStack to perform transformations
	* @param p_ViewMatrix in this state it is just View matrix, render function will add Model transformation aswell
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const override;

	/**
	 * \brief only StaticMeshes and Player class support collisions
	 * \returns true if p_Other collides with (this)
	 * @param p_Other collidable object with base of ACollidableBase
	*/
	virtual bool collides(const ACollidableBase* p_Other) override { return m_Collision ? m_Collision->checkCollision(p_Other) : false; }

	/**
	* \brief applies additive transformation to collion right after origin mesh transform, resets previous collision transform
	*/
	void addCollTrans(const glm::mat4& p_Trans);
protected:
	ACollidableBase *m_Collision;
	const int32_t m_MaterialInd;
};

/**
* \brief Base friction coef. Affects how fast every movable mesh will stop
*/
const float g_BaseFrictForceCoef = 0.09f;

/**
* \brief Class represents movable mesh in the scene, that can move, can be scaled and rotated
* All transformations are relative to init transformation, that was provided on object construct
*/
class MovableMesh : public StaticMesh {
public:
	/**
	* \brief enum representing states of finite-automata used for movement 
	*/
	enum Action : uint8_t {
		Idle = 0x00, MovingForward = 0x01, MovingBackward = 0x02, MovingStrafeLeft = 0x04, MovingStrafeRight = 0x08,
		Jumping = 0x10,
	};
	/**
	* \brief constructor builds instance of movable mesh
	* @param p_FileName specifies mesh filename
	* @param p_InitWorldPos init world position
	* @param p_InitScale init scale
	* @param p_InitRawDegrees init raw degrees of rotations along x,y,z axis in vector
	* @param p_MeshInd material submesh, that is expected to load from p_FileName, -1 for whole model
	*/
	explicit MovableMesh(const std::string &p_FileName
		, const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, const glm::vec3& p_InitRawDegrees
		, int32_t p_MeshInd = -1);
	/**
	* \brief default destructor, frees mesh
	*/
	virtual ~MovableMesh() {}
	/**
	* \brief translates mesh using provided vector as position offset
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override { m_WorldPos += p_MoveFor; }
	/**
	* \brief translates mesh using provided vector as new position
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override { m_WorldPos = p_NewPos; }
	/**
	* \brief scales mesh using provided vector as new scale
	*/
	virtual void scale(const glm::vec3& p_NewScale) override { m_Scale = p_NewScale; }
	/**
	* \brief scales mesh using provided vector as scale multiplier
	*/
	virtual void scaleRelative(const glm::vec3& p_NewScale) override { m_Scale *= p_NewScale; }
	/**
	* \brief orients mesh using provided quaternion as new orientation
	*/
	virtual void orient(const glm::fquat& p_NewOrientation) override { m_Orientation = p_NewOrientation; }
	/**
	* \brief rotates mesh around p_AxisOfRotation for p_AngRadCCW radians
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override;
	/**
	* \brief renders mesh using MatrixStack to perform transformations
	* @param p_ViewMatrix in this state it is just View matrix, render function will add Model transformation aswell
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const override;
	/**
	* \brief MovableMesh class doesn't support collisions
	* \returns false always
	*/
	virtual bool collides(const ACollidableBase* p_Other) override { return false; }
	/**
	* \brief method used to get viewMatrix from any movable object
	* \returns viewMatrix transformation
	*/
	virtual glm::mat4 viewFromActor() const { return Camera::calcViewMatrix(m_WorldPos, m_Orientation, 0.f); }
	/**
	 * \brief accelerates MovableMesh according to m_ActionStateMachine current state
	*/
	virtual void accelerate();
	/**
	* \brief applies acceleration (making movement) and atttenuates it 
	*/
	virtual void applyAcceleration();
	/**
	* \brief registers new action, that is saved to Action state machine
	* @param p_Action Action enum value, representing new action registred
	* @param p_Start true if key is pressed, false if key is released
	*/
	void newAction(Action p_Action, bool p_Start);
	/**
	* \brief binds camera to movable mesh, so object is piloting by camera
	* @param p_Camera camera instance pointer
	*/
	void pilot(const Camera* p_Camera) { m_CameraPiloting = p_Camera; }
	/**
	* \returns true if camera is set
	*/
	bool cameraIsSet() const { return m_CameraPiloting; }
protected:
	const Camera *m_CameraPiloting;
	glm::vec3 m_WorldPos;
	glm::vec3 m_Scale;
	glm::fquat m_Orientation;
	glm::vec3 m_Acceleration;
	Action m_ActionStateMachine;
	float m_MaxSpeed;
	float m_AccelAttenuation;

};

/**
* \brief Class represents hierarchical mesh in the scene, that can move, can be scaled and rotated and can have children.
* All transformations of childs are relative to base actor
*/
class HierarchicalActor : public StaticMesh {
public:
	explicit HierarchicalActor(const std::string &p_FileName
		, const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, const glm::vec3& p_InitRawDegrees
		, int32_t p_MeshInd = -1);
	virtual void load() {}
	virtual void free() {}
private:
	//TODO
	std::vector<AActorBase*> m_ActorChilds;
};

#endif //!__DRAWABLES_H