#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __SCENE_HANDLER_H
#define __SCENE_HANDLER_H

#include <vector>
#include <gl_core_4_4.h>
#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>
#include "world_objects.h"
#include "mesh.h"
#include "collision.h"
#include "transformations.h"
#include "lights.h"
#include "interpolators.h"
#include "game_state.h"


/**
* \brief class take care of every object in scene
*/
class SceneHandler {
public:
	/**
	* \brief constructs SceneHandler
	* @param p_Shaders const reference to vector of shaders
	*/
	explicit SceneHandler(const std::vector<AShaderBase*>& p_Shaders);
	/**
	* \brief renders whole scene
	* @param p_ViewMatrix view matrix (world to camera)
	*/
	void render(MatrixStack& p_ViewMatrix);
	/**
	* \brief buffers all provided materials to uniform block buffer
	* @param p_Materials vector of materials pointers, same data alignment as in GLSL shader
	* @param p_SizeMtlBLC view matrix (world to camera)
	*/
	void bufferMaterials(const std::vector<DShaderMaterial*>& p_Materials, GLuint p_SizeMtlBLC);
	/**
	* \brief updates scene, must be called every frame
	*/
	void update();
	/**
	 * \brief saves pointer to Player instance in order to draw him regularly. Takes care of freeing memory.
	 * @param p_Player pointer to player instance implicitly casted to AActorBase
	*/
	void setPlayerPtr(AActorBase* p_Player) { m_Meshes[m_PlayerInd] = p_Player;  }
	/**
	* \brief checks if p_Movable collides with some static object
	* \returns true if p_Movable collides with at least one
	* @param p_Movable pointer to Collidable instance, representing some movable object traveling in world space
	* @param p_ActivatorsOnly true if check only activators(helpfull when doing picking)
	*/
	AActorBase* collidesWithStatic(const ACollidableBase* p_Movable, bool p_ActivatorsOnly = false) const;
	/**
	* \returns LightManager reference
	*/
	LightManager& getLightManager() { return m_LightManager; }
	/**
	* \brief enables and disables fog in scene
	*/
	void toggleFog() { m_FogDisabled = !m_FogDisabled; }
private:
	/**
	* \brief adds dynamic objects to scene (like copter)
	*/
	void addDynamics();
	/**
	* \brief adds static props like BTR, ground etc
	*/
	void addStaticProps();
	/**
	* \brief adds weapons and targets
	*/
	void addActivators();
	/**
	* \brief creates scene
	*/
	void create();
	/**
	* \brief generates and sets interpolators values
	*/
	void setInterpolators();


	const int32_t m_VintorezInd = 0;
	const int32_t m_SVUInd = 1;
	const int32_t m_AKInd = 2;
	const int32_t m_Target1Ind = 3;
	const int32_t m_Target2Ind = 4;
	const int32_t m_CopterInd = 5;
	const int32_t m_BoarInd = 6;
	const int32_t m_BoarHandleInd = 7;
	const int32_t m_PlayerInd = 8;

	const std::vector<AShaderBase*>& m_Shaders;
	LinearInterpolator<glm::vec3> m_CopterInterpolator;
	LinearInterpolator<glm::vec3> m_SkycolorInterpolator;
	LinearInterpolator<glm::vec3> m_SuncolorInterpolator;
	LinearInterpolator<float> m_SunIntensityInterpolator;
	EffectsActor *m_FireEffect;
	Timer m_CopterTimer;
	Timer m_SunTimer;
	Timer m_FireTimer;
	Timer m_CommonEndlessTimer;

	GLuint m_MtlUnifBuffer;
	uint32_t m_SizeMatBLC;
	bool m_FogDisabled;

	MeshFlyweightLoader m_MeshLoader;

	std::vector<AActorBase*> m_Meshes;
	std::vector<AActorBase*> m_TransparentObjects;
	std::vector<AActorBase*> m_Activators;
	SkyBox *m_Skybox;

	LightManager m_LightManager;
};



#endif // !__SCENE_HANDLER_H
