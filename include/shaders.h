#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __SHADER_PROGRAMS_H
#define __SHADER_PROGRAMS_H

#include <string>
#include <vector>
#include <gl_core_4_4.h>
#include "misc.h"

/**
* \brief type and usage, that actor supposed to have
*/
enum ActorType : int8_t {
	SimpleMatertialActor = -1, ComplMaterialActor = 0, SkyboxActor, FXActor, ParticleActor, ShadersNum
};

/**
* \brief abstract class represents common shader logic
*/
class AShaderBase {
public:
	/**
	* \brief default destructor that deletes shader program
	*/
	virtual ~AShaderBase();

	AShaderBase(const AShaderBase&) = delete;
	AShaderBase(AShaderBase&&) = delete;
	AShaderBase& operator=(const AShaderBase&) = delete;
	AShaderBase& operator=(AShaderBase&&) = delete;

	GLuint m_Program;
	GLuint m_PosLoc;
	GLuint m_TexCoordLoc;
	ActorType m_Type;

	static const GLuint m_ProjUnifBLCBndIdx = 0;
	static const GLuint m_DiffuseTexUnit = 0;

	static const GLuint m_LightCorrUnifBLCBndIdx = 2;
protected:
	/**
	* \brief protected constructor
	* @param p_Type type of shader (expected usage)
	*/
	AShaderBase(const ActorType& p_Type) : m_Type(p_Type) {}
	/**
	* \brief Compiles shader from file.
	* \throws ShaderException
	*/
	GLuint compileShader(GLenum p_ShaderType, const std::string &p_ShaderFilename) const;
	/**
	* \brief Links two shaders, detaches and deletes them, so shaders ought to be frag and vert.
	* \throws ShaderException
	*/
	void linkProgram(GLuint p_ShaderOne, GLuint p_ShaderTwo);
	/**
	* \brief Links bunch of shaders, detaches and deletes them, so exactly 2 main() must be in provided shaders.
	* \throws ShaderException
	*/
	void linkProgram(const std::vector<GLuint>& p_Shaders);
	/**
	* \brief Takes shaders filenames and tries to solve which one is frag or vert and compiles them
	* \throws ShaderException FileException
	*/
	void prepareAndCreateProgram(const std::vector<std::string>& p_Shaders);
	/**
	* \brief method is used to get locations of all uniforms, bind uniform blocks, texture samplers etc.
	*/
	virtual void getLocations();
private:
	/**
	 * \brief called to check if compilation and linkage are OK.
	 * @param p_Obj GL object name
	 * @param p_IsProg true if checking linkage, false if cheking compiling
	 * \throws ShaderException
	*/
	void throwIfGLError(GLuint p_Obj, bool p_IsProg) const;
};

/**
* \brief class represents shader for static mesh
*/
class StaticMeshShader : public AShaderBase {
public:
	/**
	 * \brief constructs static mesh shader instance
	*/
	explicit StaticMeshShader();
	/**
	* \brief default destructor
	*/
	virtual ~StaticMeshShader() {}
	StaticMeshShader(const StaticMeshShader&) = delete;
	StaticMeshShader(StaticMeshShader&&) = delete;
	StaticMeshShader& operator=(const StaticMeshShader&) = delete;
	StaticMeshShader& operator=(StaticMeshShader&&) = delete;

	GLuint m_NormalLoc;
	GLuint m_TangentLoc;
	GLuint m_BitangentLoc;
	GLuint m_MVmatrixUnifLoc;
	GLuint m_MVmatrixNormalUnifLoc;

	static const GLuint m_MtlUnifBLCBndIdx = 1;
	static const GLuint m_LgtSrcUnifBLCBndIdx = 3;

	static const GLuint m_NormalMapTexUnit = 1;

protected:
	/**
	* \brief method is used to get locations of all uniforms, bind uniform blocks, texture samplers etc.
	*/
	virtual void getLocations();
private:
	const std::vector<std::string> m_ShaderFilenames =
	{ "static_mesh.vert", "static_mesh.frag" };
};

/**
* \brief class represents shader for skybox
*/
class SkyboxShader : public AShaderBase {
public:
	/**
	* \brief constructs skybox shader instance
	*/
	explicit SkyboxShader();
	/**
	* \brief default destructor
	*/
	virtual ~SkyboxShader() {}
	SkyboxShader(const SkyboxShader&) = delete;
	SkyboxShader(SkyboxShader&&) = delete;
	SkyboxShader& operator=(const SkyboxShader&) = delete;
	SkyboxShader& operator=(SkyboxShader&&) = delete;


	GLuint m_SkyColorLoc;
	GLuint m_VmatrixUnifLoc;

	static const GLuint m_SkyBoxUnifBlcBndInd = 4;

	static const GLuint m_SkyboxBigCloudsTexUnit = 2;
	static const GLuint m_SkyboxSmallCloudsTexUnit = 3;
	static const GLuint m_SkyboxStarsTexUnit = 4;

protected:
	/**
	* \brief method is used to get locations of all uniforms, bind uniform blocks, texture samplers etc.
	*/
	virtual void getLocations();
private:
	const std::vector<std::string> m_ShaderFilenames =
	{ "skybox.vert", "skybox.frag" };
};

/**
* \brief class represents shader for visual effects
*/
class FXShader : public AShaderBase {
public:
	/**
	* \brief constructs effects shader instance
	*/
	explicit FXShader();
	/**
	* \brief default destructor
	*/
	virtual ~FXShader() {}
	FXShader(const FXShader&) = delete;
	FXShader(FXShader&&) = delete;
	FXShader& operator=(const FXShader&) = delete;
	FXShader& operator=(FXShader&&) = delete;


	GLuint m_AlphaTimeUnifLoc;
	GLuint m_TexScaleUnifLoc;
	GLuint m_MVmatrixUnifLoc;

protected:
	/**
	* \brief method is used to get locations of all uniforms, bind uniform blocks, texture samplers etc.
	*/
	virtual void getLocations();
private:
	const std::vector<std::string> m_ShaderFilenames =
	{ "fx.vert", "fx.frag" };
};


#endif // !__SHADER_PROGRAMS_H
