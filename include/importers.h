#ifdef _MSC_VER
#pragma once
#endif

#ifndef __IMPORTERS_H
#define __IMPORTERS_H

#include <string>
#include <fstream>
#include <gl_core_4_4.h>
#include "materials.h"
#include "exceptions.h"
#include "collision.h"

/**
 * \brief Base class for all mesh importers. Has everything, that mesh importer might need
*/
class MeshImporterBase {
public:
	/**
	 * \brief enum containing bit-wise flags, that importer might need to correctly import a mesh
	*/
	enum MeshFlags : uint8_t {
		NoFlags = 0x00, HasDifferentMaterials = 0x01, ChangeableVRAM = 0x02,
	};

	/**
	* \returns material submeshes count
	*/
	uint32_t getMeshNum() const { return m_Materials.size(); }

	/**
	* \brief sets material submesh's index that might be imported
	* @param p_MeshIdx index of material submesh
	*/
	bool setMeshIdxInScene(int32_t p_MeshIdx);

	/**
	 * \returns vectices count
	*/
	uint32_t getVerticesNum() const { return m_VerticesNum; }

	/**
	* \returns indeces count
	*/
	uint32_t getIndecesNum() const { return m_IndecesNum; }

	/**
	* \returns true if model contains normals
	*/
	bool hasNormals() const { return m_HasNormals; }

	/**
	* \returns true if model contains texture coordinates
	*/
	bool hasTexCoords() const { return m_HasTexCoords; }

	/**
	* \returns true if model contains vertex colors
	*/
	bool hasColors() const { return m_HasColors; }

	/**
	* \returns true if model contains tangents
	*/
	bool hasTangents() const { return m_HasTangents; }

	/**
	* \returns true if model contains bitangents
	*/
	bool hasBitangents() const { return m_HasBitangents; }

	/**
	* \returns material vectors with already set rendering indeces and bases
	*/
	std::vector<Material> getMaterials();

	/**
	 * \returns material to current importing material submesh, returns material number 0, if whole model might been imported (-1)
	*/
	Material getMaterial() const { return Material(m_Materials[m_MeshIdx < 0 ? 0 : m_MeshIdx]); }
protected:

	/**
	 * \brief protected constructor, that is used by inherited classes to initialize parent
	*/
	MeshImporterBase(const std::string & p_FileName, const MeshFlags & p_Flags) :
		m_ModelPath(p_FileName), m_Flags(p_Flags), m_MeshIdx(-2), m_ReturnedAllMtls(false) {}

	/**
	* \brief abstract method used to recalculate all indeces and verteces count, update bases etc. 
	* values depend on provided mesh index, use -1 for import whole model
	* @param m_MeshIdx material submesh index
	*/
	virtual void setMeshesMetadata(int32_t m_MeshIdx) = 0;


	const std::string m_ModelPath;
	const MeshFlags m_Flags;
	uint32_t m_VerticesNum;
	uint32_t m_IndecesNum;
	bool m_HasNormals;
	bool m_HasTexCoords;
	bool m_HasColors;
	bool m_HasTangents;
	bool m_HasBitangents;
	int32_t m_MeshIdx;
	bool m_ReturnedAllMtls;
	std::vector<Material> m_Materials;

	/**
	 * \brief enum represents buffer types, that have common topology - each vertex info is saved as 3 floats
	*/
	enum BufferTypeCommonTopology {
		VertexBuffer = 0, NormalBuffer, ColorBuffer, TangentBuffer, BitangentBuffer,
	};
};

/**
* \brief abstract class exposes interface to import models directly to GL context
*/
class GLContImporterBase : public MeshImporterBase {
public:
	/**
	 * \brief constructor used only by enherited classses
	 * @param p_FileName file name of model
	 * @param p_Flags flags, that can help importer to correctly do it's job
	*/
	GLContImporterBase(const std::string& p_FileName, const MeshFlags & p_Flags);

	/**
	 * \brief default destructor
	*/
	virtual ~GLContImporterBase() {}

	/**
	 * \returns GL name of created vertex buffer
	 * uses abstract method getBufferCommonTopology(), that must be implemented in inherited classes
	*/
	GLuint getVertexBuffer() { return getBufferCommonTopology(VertexBuffer); }

	/**
	* \returns GL name of created normal buffer
	* uses abstract method getBufferCommonTopology(), that must be implemented in inherited classes
	*/
	GLuint getNormalBuffer() { return getBufferCommonTopology(NormalBuffer); }

	/**
	* \returns GL name of created color buffer
	* uses abstract method getBufferCommonTopology(), that must be implemented in inherited classes
	*/
	GLuint getColorBuffer() { return getBufferCommonTopology(ColorBuffer); }

	/**
	* \returns GL name of created tangent buffer
	* uses abstract method getBufferCommonTopology(), that must be implemented in inherited classes
	*/
	GLuint getTangentBuffer() { return getBufferCommonTopology(TangentBuffer); }

	/**
	* \returns GL name of created bitangent buffer
	* uses abstract method getBufferCommonTopology(), that must be implemented in inherited classes
	*/
	GLuint getBitangentBuffer() { return getBufferCommonTopology(BitangentBuffer); }

	/**
	* \brief abstract method might be used to import texture coordinates and return GL name of new created texture coordinates buffer
	*/
	virtual GLuint getTexCoordBuffer() = 0;

	/**
	* \brief abstract method might be used to import element buffer and return GL name of new created element buffer
	*/
	virtual GLuint getElementBuffer() = 0;

	/**
	 * \brief abstract method might be used to create collision for model
	 * @param p_Type provides type of collision, that is requested
	*/
	virtual ACollidableBase* getCollision(ACollidableBase::CollisionType p_Type) = 0;
protected:
	/**
	* \brief abstract method might be used to recalculate all indeces and verteces count, update bases etc.
	* values depend on provided mesh index, use -1 for import whole model
	* @param m_MeshIdx material submesh index
	*/
	virtual void setMeshesMetadata(int32_t m_MeshIdx) = 0;

	/**
	 * \brief abstract method might be used for creating buffers that have common topology - each vertex info is saved as 3 floats
	 * @param p_Type provides enum value - type of buffer
	*/
	virtual GLuint getBufferCommonTopology(const BufferTypeCommonTopology& p_Type) = 0;
};

/**
* \brief imports different models directly to GL context
*/
class GLContImporter : public GLContImporterBase {
public:
	/**
	* \brief constructor used to create instance for importing one mesh
	* @param p_FileName file name of model
	* @param p_Flags flags, that can help importer to correctly do it's job
	*/
	GLContImporter(const std::string& p_FileName, const MeshFlags & p_Flags);

	/**
	* \brief default destructor
	*/
	virtual ~GLContImporter() { delete m_Importer; }

	/**
	* \brief mthod is used to import texture coordinates and return GL name of new created texture coordinates buffer
	* \returns texture coordinates GL buffer name, GLuint other words
	*/
	virtual GLuint getTexCoordBuffer() override;

	/**
	* \brief method is used to import element buffer and return GL name of new created element buffer
	* \returns elements GL buffer name, GLuint other words
	*/
	virtual GLuint getElementBuffer() override;

	/**
	 * \brief generates collision of provided type and returns NEW ALLOCATED instance pointer
	 * \returns NEW ALLOCATED instance pointer
	 * @param p_Type type of collision
	*/
	virtual ACollidableBase* getCollision(ACollidableBase::CollisionType p_Type = ACollidableBase::BoxCollision) override;
protected:
	/**
	* \brief method is used to recalculate all indeces and verteces count, update bases etc.
	* values depend on provided mesh index, use -1 for import whole model
	* @param m_MeshIdx material submesh index
	*/
	virtual void setMeshesMetadata(int32_t m_MeshIdx) override;
private:
	/**
	* \brief method is used for creating buffers that have common topology - each vertex info is saved as 3 floats
	* @param p_Type provides enum value - type of buffer
	*/
	GLuint getBufferCommonTopology(const BufferTypeCommonTopology& p_Type);
	Assimp::Importer *m_Importer;
	const aiScene * m_Meshes;
};

/**
* \brief imports different models directly to GL context
*/
class GLContObjImporter : public GLContImporterBase {
public:
	/**
	* \brief constructor used to create instance for importing one mesh
	* @param p_FileName file name of model
	* @param p_Flags flags, that can help importer to correctly do it's job
	*/
	GLContObjImporter(const std::string& p_FileName, const MeshFlags & p_Flags);

	/**
	* \brief default destructor
	*/
	virtual ~GLContObjImporter();

	/**
	* \brief mthod is used to import texture coordinates and return GL name of new created texture coordinates buffer
	* \returns texture coordinates GL buffer name, GLuint other words
	*/
	virtual GLuint getTexCoordBuffer();

	/**
	* \brief method is used to import element buffer and return GL name of new created element buffer
	* \returns elements GL buffer name, GLuint other words
	*/
	virtual GLuint getElementBuffer();

	/**
	* \brief generates collision of provided type and returns NEW ALLOCATED instance pointer
	* \returns NEW ALLOCATED instance pointer
	* @param p_Type type of collision
	*/
	virtual ACollidableBase* getCollision(ACollidableBase::CollisionType p_Type) override;
protected:
	/**
	* \brief method is used to recalculate all indeces and verteces count, update bases etc.
	* values depend on provided mesh index, use -1 for import whole model
	* @param m_MeshIdx material submesh index
	*/
	virtual void setMeshesMetadata(int32_t m_MeshIdx);

	/**
	* \brief method is used for creating buffers that have common topology - each vertex info is saved as 3 floats
	* @param p_Type provides enum value - type of buffer
	*/
	virtual GLuint getBufferCommonTopology(const BufferTypeCommonTopology& p_Type);
private:
	std::ifstream m_File;
	size_t m_Filesize;
	void *m_Meshes;
};
#endif // !__IMPORTERS_H
