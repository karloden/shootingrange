#ifndef _MSC_VER
#pragma once
#endif

#ifndef __MESH_GEOMETRY_H
#define __MESH_GEOMETRY_H

#include <map>
#include <utility>
#include <gl_core_4_4.h>

#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/quaternion.hpp>
#include "pgr.h"
#include "exceptions.h"
#include "misc.h"
#include "importers.h"
#include "materials.h"
#include "shaders.h"
#include "transformations.h"


/**
 * \brief this base abstract class represents geometry, connected to mesh, supports dynamic loads, dynamic frees
 * have to be used only with smart loader, that will take care of allocations.
*/
class AGeometryBase {
public:
	AGeometryBase(const AGeometryBase&) = delete;
	AGeometryBase& operator=(const AGeometryBase&) = delete;
	AGeometryBase& operator=(AGeometryBase&&) = delete;
	/**
	 * \brief default destructor
	 * frees VBO, VAO, EBO
	*/
	virtual ~AGeometryBase();
	/**
	* \brief abstract method that might be used for mesh geometry rendering
	* @param p_ModelViewMat view * model transformation mat (model to camera)
	*/
	virtual void render(MatrixStack & p_ModelViewMat) const = 0;
	/**
	* \brief abstract method that might be used for polling shader material pointers in order to upload them in GL context
	* @param p_UnifMtlBLCInd uniform material block index - reserved index in material uniform block
	*/
	virtual std::vector<DShaderMaterial*> getMtlPtrs(int32_t p_UnifMtlBLCInd) = 0;
	/**
	* \brief abstract method that might be used for polling material pointer in order to change it with it's methods
	* \returns Material pointer
	* @param p_MtlInd index of material submesh in mesh geometry
	*/
	virtual Material* getConnectedMaterialPtr(int32_t p_MtlInd = 0) = 0;
	/**
	* \brief binds mesh geometry with relevant shader program
	* @param p_Shader shader class instance
	*/
	virtual void loadShaderProgram(const AShaderBase *p_Shader);
	/**
	 * \brief must be called every time new MeshActor is starting to use this mesh geometry
	*/
	void newUser() { ++m_UsersNum; }
	/**
	 * \returns ActorType enum value - type of mesh geometry
	*/
	ActorType getType() { return m_Type; }
	/**
	 * \brief must be called every time MeshActor was unbind from of this mesh geometry
	 * \returns true => MUST BE deleted and CANNOT BE ACCESSED, false => there are more MeshActors using this MeshGeometry
	*/
	virtual bool unload();
protected:
	/**
	 * \brief protected constructor used by inherited classes to init parent
	 * @param p_Type type of mesh geometry
	*/
	AGeometryBase(ActorType p_Type);
	GLuint m_VertexBO;
	GLuint m_TexCoordBO;
	GLuint m_ElementBO;
	GLuint m_VAO;
	uint32_t m_VertNum;
	uint32_t m_IndNum;
	uint32_t m_UsersNum;
	bool m_FromOtherDestructor;
	ActorType m_Type;
private:
	const AShaderBase* m_CommonShader;
};

/**
* \brief this class represents geometry, connected to mesh, has only one simple material
* have to be used only with smart loader, that will take care of allocations.
*/
class SimpleStaticMeshGeom : public AGeometryBase
{
public:
	/**
	 * \brief constructs mesh geometry with only one material
	 * @param p_FileName mesh file name that might to be found in g_PropsPath
	 * @param p_MeshNum material submesh
	*/
	SimpleStaticMeshGeom(const std::string& p_FileName, int32_t p_MeshNum);
	/**
	 * \brief default destructor, frees normals buffer and texture coordinates buffer
	*/
	virtual ~SimpleStaticMeshGeom();
	///Prohibiting copying and moving
	SimpleStaticMeshGeom(const SimpleStaticMeshGeom&) = delete;
	SimpleStaticMeshGeom& operator=(const SimpleStaticMeshGeom&) = delete;
	SimpleStaticMeshGeom& operator=(SimpleStaticMeshGeom&&) = delete;

	/**
	* \brief method used for mesh geometry rendering
	* @param p_ModelViewMat view * model transformation mat (model to camera)
	*/
	virtual void render(MatrixStack & p_ModelViewMat) const;
	/**
	* \brief method that is used for polling shader material pointers in order to upload them in GL context
	* @param p_UnifMtlBLCInd uniform material block index - reserved index in material uniform block
	*/
	virtual std::vector<DShaderMaterial*> getMtlPtrs(int32_t p_UnifMtlBLCInd);
	/**
	* \brief method is used for polling material pointer in order to change it with it's methods
	* \returns Material pointer
	* @param p_MtlInd index of material submesh in mesh geometry
	*/
	virtual Material* getConnectedMaterialPtr(int32_t p_MtlInd = 0) { return &m_SimpleMaterial; }
	/**
	* \brief must be called every time MeshActor was unbind from of this mesh geometry
	* \returns true => MUST BE deleted and CANNOT BE ACCESSED, false => there are more MeshActors using this MeshGeometry
	*/
	virtual bool unload();
	/**
	* \brief binds mesh geometry with relevant shader program
	* @param p_Shader shader class instance
	*/
	void loadShaderProgram(const AShaderBase *p_Shader) final;
	/**
	 * \brief get collision pointer in init state. Can't be used directly, must call clone() on collision
	 * \returns pointer to collidable
	*/
	ACollidableBase* getInitCollision() const { return m_InitCollision; }
protected:
	GLuint m_NormalBO;
	GLuint m_TangentBO;
	GLuint m_BitangentBO;
	GLuint m_UnifMtlBLCInd;
	Material m_SimpleMaterial;
	GLContImporter *m_Importer;
	ACollidableBase* m_InitCollision;
	const StaticMeshShader* m_ShaderStMesh;

};

/**
* \brief this class represents geometry, connected to mesh, has bunch of materials that have all information how to draw themselves.
* have to be used only with smart loader, that will take care of allocations.
*/
class StaticMeshGeom : public SimpleStaticMeshGeom {
public:
	/**
	* \brief constructs complicated mesh geometry with bunch of materials 
	* @param p_FileName mesh file name that might to be found in g_PropsPath
	*/
	StaticMeshGeom(const std::string& p_FileName);
	/**
	* \brief default destructor
	*/
	virtual ~StaticMeshGeom() {}
	StaticMeshGeom(const StaticMeshGeom&) = delete;
	StaticMeshGeom& operator=(const StaticMeshGeom&) = delete;
	StaticMeshGeom& operator=(StaticMeshGeom&&) = delete;
	/**
	* \brief method used for mesh geometry rendering
	* @param p_ModelViewMat view * model transformation mat (model to camera)
	*/
	virtual void render(MatrixStack & p_ModelViewMat)const;
	/**
	* \brief method that is used for polling shader material pointers in order to upload them in GL context
	* @param p_UnifMtlBLCInd uniform material block index - reserved index in material uniform block
	*/
	virtual std::vector<DShaderMaterial*> getMtlPtrs(int32_t p_UnifMtlBLCInd);
	/**
	* \brief method is used for polling material pointer in order to change it with it's methods
	* \returns Material pointer
	* @param p_MtlInd index of material submesh in mesh geometry
	*/
	virtual Material* getConnectedMaterialPtr(int32_t p_MtlInd = 0);
	/**
	* \brief must be called every time MeshActor was unbind from of this mesh geometry
	* \returns true => MUST BE deleted and CANNOT BE ACCESSED, false => there are more MeshActors using this MeshGeometry
	*/
	virtual bool unload();
protected:
	std::vector<Material> m_Materials;
};

/**
* \brief represents geometry of skybox.
* have to be used only with smart loader, that will take care of allocations.
*/
class SkyboxMeshGeom : public AGeometryBase
{
public:
	/**
	* \brief constructs skybox mesh geometry
	* @param p_FileName mesh file name that might to be found in g_PropsPath
	*/
	explicit SkyboxMeshGeom(const std::string & p_FileName);

	SkyboxMeshGeom(const AGeometryBase&) = delete;
	SkyboxMeshGeom& operator=(const AGeometryBase&) = delete;
	SkyboxMeshGeom& operator=(AGeometryBase&&) = delete;

	/**
	* \brief default destructor
	*/
	virtual ~SkyboxMeshGeom() {}
	/**
	* \brief method used for skybox geometry rendering
	* @param p_ModelViewMat view * model transformation mat (model to camera)
	*/
	virtual void render(MatrixStack & p_ModelViewMat) const override;
	/**
	* \brief skybox doesn't need material in uniform buffer for render, so we simply return empty std::vector of appropriate type
	* @param p_UnifMtlBLCInd uniform material block index - reserved index in material uniform block
	*/
	virtual std::vector<DShaderMaterial*> getMtlPtrs(int32_t p_UnifMtlBLCInd) override { return std::vector<DShaderMaterial*>(); }
	/**
	* \brief method is used for polling material pointer in order to change it with it's methods
	* \returns Material pointer 
	* @param p_MtlInd index of material submesh in mesh geometry
	*/
	virtual Material* getConnectedMaterialPtr(int32_t p_MtlInd = 0) override { return &m_SimpleMaterial; }
	/**
	* \brief binds skybox geometry with relevant shader program
	* @param p_Shader shader class instance
	*/
	virtual void loadShaderProgram(const AShaderBase *p_Shader);
	/**
	* \brief must be called every time MeshActor was unbind from of this skybox
	* \returns true => MUST BE deleted and CANNOT BE ACCESSED, false => there are more MeshActors using this MeshGeometry
	*/
	virtual bool unload();
protected:
	Material m_SimpleMaterial;
	GLuint m_BigCloudsTex;
	GLuint m_SmallCloudsTex;
	GLuint m_StarsTex;
	const SkyboxShader* m_Shader;
};


/**
* \brief this class represents geometry of visual effects.
*  have to be used only with smart loader, which will return NEW INSTANCE for each FX in the scene because there can be many different FX with one model name
*/
class FXGeom : public AGeometryBase
{
public:
	explicit FXGeom(const std::string & p_FileName, int32_t p_TexScale);
	FXGeom(const AGeometryBase&) = delete;
	FXGeom& operator=(const AGeometryBase&) = delete;
	FXGeom& operator=(AGeometryBase&&) = delete;
	/**
	* \brief default destructor 
	*/
	virtual ~FXGeom() { unload(); }
	/**
	* \brief method used for effects geometry rendering
	* @param p_ModelViewMat view * model transformation mat (model to camera)
	*/
	virtual void render(MatrixStack & p_ModelViewMat) const override;
	/**
	* \brief FX doesn't need material in uniform buffer for render, so we simply return empty std::vector of appropriate type
	* @param p_UnifMtlBLCInd uniform material block index - reserved index in material uniform block
	*/
	virtual std::vector<DShaderMaterial*> getMtlPtrs(int32_t p_UnifMtlBLCInd) override { return std::vector<DShaderMaterial*>(); }
	/**
	* \brief method is used for polling material pointer in order to change it with it's methods
	* \returns Material pointer
	* @param p_MtlInd index of material submesh in mesh geometry
	*/
	virtual Material* getConnectedMaterialPtr(int32_t p_MtlInd = 0) override { return &m_SimpleMaterial; }
	/**
	* \brief binds FX geometry with relevant shader program
	* @param p_Shader shader class instance
	*/
	virtual void loadShaderProgram(const AShaderBase *p_Shader);
	/**
	* \brief for FX geometry there cannot be more then one user, so we simply call unload on destruction
	* \returns true always
	*/
	virtual bool unload() { return AGeometryBase::unload(); }
protected:
	Material m_SimpleMaterial;
	const FXShader* m_Shader;
	int32_t m_TexScale;
};

/**
* \brief smart flyweight loader, uses map to hold pointers to geomerty objects
*/
class MeshFlyweightLoader {
public:
	MeshFlyweightLoader(const std::vector<AShaderBase*> &p_Shaders) : m_Shaders(p_Shaders) {}
	~MeshFlyweightLoader() { for (auto &geomBase : m_PointerMap) delete geomBase.second; }

	/**
	* \brief creates new instance of geometry, if it's not present in memory or
	* \returns pointer to geometry allready allocated in memory, when p_ExpectedUsage is FXActor returns NEW instance of effect actor
	* @param p_FileName mesh file name that might to be found in g_PropsPath
	* @param p_ExpectedUsage type of geometry to be created
	* @param p_AdditionalInfo is material number when p_ExpectedUsage is SimpleMatertialActor (instancing simple mesh geometry), and texture scale when instancing FXActor
	*/
	AGeometryBase* getMeshGeomPtrOrLoad(const std::string& p_FileName,
		const ActorType& p_ExpectedUsage, int32_t p_AdditionalInfo = -1);

	/**
	* \brief sets material index in uniform block for each mesh
	* \returns vector of material pointers like they are represented in VRAM
	*/
	std::vector<DShaderMaterial*> prepareAndGetAllShaderMaterials() const;


	/**
	* \brief finds and returns pointer to material with mesh name and material index
	* \returns material pointer
	* @param p_MeshName mesh file name that might to be found in g_PropsPath and that was already loaded
	* @param p_MaterialInd material submesh index
	*/
	Material *getConnectedMaterial(const std::string &p_MeshName, int32_t p_MaterialInd) const;

	/**
	* \brief deletes mesh geometry instance from memory
	* @param p_MeshName mesh file name that might to be found in g_PropsPath and that was already loaded
	* @param p_MaterialInd material submesh index
	*/
	void freeMesh(const std::string &p_MeshName, int32_t p_MaterialInd);

	/**
	* \brief finds and returns pointer to NEW ALLOCATED collision connected to mesh with mesh name and material index
	* \returns collision pointer to allocated collision, receiver must manage memory free
	* @param p_MeshName mesh file name that might to be found in g_PropsPath and that was already loaded
	* @param p_MaterialInd material submesh index
	*/
	ACollidableBase *getNewConnectedCollision(const std::string &p_MeshName, int32_t p_MaterialInd) const;

private:
	///first is pair of model file name and integer, representing mesh number is scene, all meshes := -1
	std::map<std::pair<std::string, int32_t>, AGeometryBase*> m_PointerMap;
	const std::vector<AShaderBase*>& m_Shaders;
};


/*
class AnimatedMeshGeom : public HierarchicalMeshGeom {

protected:
	BoneBase *m_Bones;
};*/


#endif //!__MESH_GEOMETRY_H