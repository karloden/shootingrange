#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __COLLISION_H
#define __COLLISION_H

#include <glm/vec3.hpp>
#include <vector>

///predefenition of classes

class CollidableBox;
class CollidableSphere;
class CollidablePoint;

/**
* \brief Base abstract class for collision detection
*/
class ACollidableBase {
public:
	/**
	* \brief enum represents type of collision
	* P.S. SphereCollision is not implemented yet
	*/
	enum CollisionType : uint8_t { BoxCollision = 0, SphereCollision, WorldPoint };

	/**
	* \brief returns type of collision instance for choosing what kind of comparison must be chosen
	* \returns type of collision instance for choosing what kind of comparison must be chosen
	*/
	CollisionType getType() const { return m_Type; }

	/**
	* \brief abstract method to hard copy inhereted classes instances 
	* Example: instancing collision for same mesh geometry with another actor transformation
	* \returns pointer to new collision instance
	*/
	virtual ACollidableBase* clone() const = 0;

	/**
	* \brief abstract method to solve which comparison function to choose for inheritated classes
	* @param p_Other any collidable object
	*/
	virtual bool checkCollision(const ACollidableBase* p_Other) const = 0;

	/**
	* \brief abstract method to decide if (this) instance collides with point
	* @param p_Other collidable point instance ptr
	*/
	virtual bool pointInCollidable(const CollidablePoint* p_Point) const = 0;

	/**
	* \brief abstract method to decide if (this) instance collides with box
	* @param p_Other collidable box instance ptr
	*/
	virtual bool checkBoxCollision(const CollidableBox* p_Other) const = 0;

	/**
	* \brief abstract method scales collision faster then applying transformation
	*/
	virtual void scale(const glm::vec3& p_Scales) = 0;

	/**
	 * \brief abstract method applies transformation matrix on collidable 
	 * @param p_InitTransform transformation matrix
	*/
	virtual void applyTransform(const glm::mat4 &p_InitTransform) = 0;

	/**
	* \brief abstract method resets collision to init untransformed state (normalized model space) 
	*/
	virtual void reset() = 0;


protected:
	/**
	 * \brief constructor of base abstract class, which is protected
	 * @param p_Type enum value, representing type of collidable
	*/
	ACollidableBase(CollisionType p_Type) : m_Type(p_Type) {}
	CollisionType m_Type;
};

/**
 * \brief Implements axis-aligned bounding box for calculating collisions fast with average accuracy
*/
class CollidableBox : public ACollidableBase {
public:
	/**
	 * \brief constructor, that constructs collidable box instance
	 * takes 6 floats, each of them is mapped to imaganary box faces coordinates
	*/
	CollidableBox(float p_InitMaxX, float p_InitMinX
		, float p_InitMaxY, float p_InitMinY
		, float p_InitMaxZ, float p_InitMinZ);

	//TODO add reset here
	CollidableBox & operator=(const CollidableBox&) = default;
	CollidableBox(const CollidableBox&) = default;

	/**
	* \brief method to hard copy CollidableBox class instances
	* Example: instancing CollidableBox for same mesh geometry with another actor transformation
	* \returns pointer to new CollidableBox instance
	*/
	virtual ACollidableBase* clone() const override { return new CollidableBox(*this); }

	/**
	* \brief method solves which comparison function to choose for CollidableBox instance
	* @param p_Other any collidable object
	*/
	virtual bool checkCollision(const ACollidableBase* p_Other) const override;

	/**
	* \brief method decides if (this) CollidableBox instance collides with another box
	* @param p_Other collidable box instance ptr
	*/
	virtual bool checkBoxCollision(const CollidableBox* p_Other) const override;

	/**
	* \brief  method decides if (this) CollidableBox instance collides with point
	* @param p_Other collidable point instance ptr
	*/
	virtual bool pointInCollidable(const CollidablePoint* p_Point) const override;

	/**
	* \brief method scales CollidableBox faster than applying transformation
	* @param p_InitTransform transformation matrix
	*/
	virtual void scale(const glm::vec3& p_Scales) override;

	/**
	* \brief abstract method applies transformation matrix on CollidableBox
	* @param p_InitTransform transformation matrix
	*/
	virtual void applyTransform(const glm::mat4 &p_InitTransform);

	/**
	* \brief hardly translates bounding box to new world space coordinates.
	* must be used only if you are 100% sure about new positions
	* takes 6 floats, each of them is mapped to imaganary box faces coordinates
	*/
	void relocate(float p_MaxX, float p_MinX
		, float p_MaxY, float p_MinY
		, float p_MaxZ, float p_MinZ);

	/**
	* \brief resets collision to init untransformed state (normalized model space)
	*/
	virtual void reset();
private:
	///Init coordinates are bounding box coords in model space with no transformations applied
	const float m_InitMaxX, m_InitMinX;
	///Init coordinates are bounding box coords in model space with no transformations applied
	const float m_InitMaxY, m_InitMinY;
	///Init coordinates are bounding box coords in model space with no transformations applied
	const float m_InitMaxZ, m_InitMinZ;
	///Coordinates are bounding box coords in world space with static mesh transformations applied
	float m_CurrMaxX, m_CurrMinX;
	///Coordinates are bounding box coords in world space with static mesh transformations applied
	float m_CurrMaxY, m_CurrMinY;
	///Coordinates are bounding box coords in world space with static mesh transformations applied
	float m_CurrMaxZ, m_CurrMinZ;
};

class CollidablePoint : public ACollidableBase {
public:
	/**
	 * \brief constructor, that constructs collidable point instance 
	 * used only for solving if that point lies in another collidable bounds
	*/
	CollidablePoint(const glm::vec3& p_Point)
		: ACollidableBase(WorldPoint)
		, m_InitPoint(p_Point)
		, m_Point(p_Point) {}

	CollidablePoint & operator=(const CollidablePoint&) = default;
	CollidablePoint(const CollidablePoint&) = default;

	/**
	* \brief method to hard copy CollidablePoint class instances
	* Example: instancing CollidablePoint for same mesh geometry with another actor transformation
	* \returns pointer to new CollidablePoint instance
	*/
	virtual ACollidableBase* clone() const override { return new CollidablePoint(*this); }


	/**
	* \brief method solves which comparison function to choose for CollidablePoint instance
	* @param p_Other any collidable object
	*/
	virtual bool checkCollision(const ACollidableBase* p_Other) const override;

	/**
	* \brief method decides if (this) CollidablePoint instance collides with CollidableBox
	* @param p_Other CollidableBox instance ptr
	*/
	virtual bool checkBoxCollision(const CollidableBox* p_Other) const override { return p_Other->pointInCollidable(this); }


	/**
	* \brief  method decides if (this) CollidablePoint instance collides with another CollidablePoint
	* @param p_Other CollidablePoint instance ptr
	*/
	virtual bool pointInCollidable(const CollidablePoint* p_Point) const;

	/**
	 * \brief one point cannot be scaled. Does nothing
	*/
	virtual void scale(const glm::vec3& p_Scales) override {}

	/**
	* \brief applying transformation on single point have to only translate it
	*/
	virtual void applyTransform(const glm::mat4 &p_InitTransform) { m_Point += glm::vec3(p_InitTransform[3]); }

	/**
	* \brief resets collision to init untransformed state (normalized model space)
	*/
	virtual void reset() override { m_Point = m_InitPoint; }

	const glm::vec3 m_InitPoint;
	glm::vec3 m_Point;
};

#endif //!__COLLISION_H