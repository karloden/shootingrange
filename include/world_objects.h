#ifdef _MSC_VER
#pragma once
#endif

#ifndef __WORLD_OBJECTS_H
#define __WORLD_OBJECTS_H

#include <string>
#include <gl_core_4_4.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include "mesh_geometry.h"
#include "transformations.h"



/**
* \brief abstract class represents all objects that can be found in the world.
*/
class AActorBase {
public:
	/**
	* \brief constructor inits parent class for inherited
	* @param p_FileName specifies mesh filename
	* @param p_InitWorldPos init world position
	* @param p_InitScale init scale
	* @param p_InitRawDegrees init raw degrees of rotations along x,y,z axis in vector
	* @param p_ChangeableVRAM true if mesh data will be changed in VRAM afterwards
	*/
	explicit AActorBase(const std::string& p_FileName
		, const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, const glm::vec3& p_InitRawDegrees
		, bool p_ChangeableVRAM);
	/**
	* \brief default destructor
	*/
	virtual ~AActorBase() {}
	/**
	* \brief sets static pointer to MeshFlyweightLoader, that every mesh might need. Must be called before any mesh loads
	* @param p_Loader reference to FlyweightLoader
	*/
	static void setLoader(MeshFlyweightLoader &p_Loader) { m_Loader = &p_Loader; }
	/**
	* \brief toggles actor rendering
	*/
	void toggle() { m_RenderDisabled = !m_RenderDisabled; }
	/**
	* \returns true if actor was correctly loaded
	*/
	bool isLoaded() const { return m_Loaded; }
	/**
	* \returns true if actor rendering is enabled
	*/
	bool isEnabled() const { return !m_RenderDisabled; }
	/**
	* \returns const reference to string with mesh filename
	*/
	const std::string& getName() const { return m_Filename; }
	/**
	* \brief abstract method might used to revert actor to initiate state
	*/
	virtual void reset() = 0;
	/**
	* \brief abstract method might used to translate meshes
	*/
	virtual void translate(const glm::vec3& p_MoveFor) = 0;
	/**
	* \brief abstract method might used to translate meshes
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) = 0;
	/**
	* \brief abstract method might used to scale meshes
	*/
	virtual void scale(const glm::vec3& p_NewScale) = 0;
	/**
	* \brief abstract method might used to scale meshes
	*/
	virtual void scaleRelative(const glm::vec3& p_NewScale) = 0;
	/**
	* \brief abstract method might used to orient meshes
	*/
	virtual void orient(const glm::fquat& p_NewOrientation) = 0;
	/**
	* \brief abstract method might used to rotate meshes
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) = 0;
	/**
	* \brief abstract method might used to load and init actor
	*/
	virtual void load() = 0;
	/**
	* \brief abstract method might used to free actor
	*/
	virtual void free() = 0;
	/**
	* \brief renders actor using MatrixStack to perform transformations
	* @param p_ViewMatrix in this state it is just View matrix, render function will add Model transformation aswell if necessary
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const = 0;
	/**
	* \brief checks if (this) actor's collidable collides with other
	* \returns true if these two objects collide
	*/
	virtual bool collides(const ACollidableBase* p_Other) = 0;

	float getDistanceCamera(const glm::vec3 &p_PlayerWorldPos);
protected:

	AGeometryBase * m_Mesh;
	static MeshFlyweightLoader *m_Loader;
	bool m_RenderDisabled;
	bool m_Loaded;
	glm::mat4 m_InitTransform;
	const std::string m_Filename;
	bool m_ChangeableVRAM;
};

/**
* \brief class represents dynamic world skybox with day time, stars, dynamic clouds, fog, etc.
*/
class SkyBox : public AActorBase {
public:
	/**
	* \brief constructs SkyBox actor taking some hard parameters
	* @param p_CloudColor color of clouds
	* @param p_CloudSpeed speed of clouds 
	* @param p_CloudOpacity opacity of clouds
	* @param p_HorizonFalloff exponent value, that represents place on skybox, where horizon changes to zenith
	*/
	explicit SkyBox(const glm::vec3& p_CloudColor, float p_CloudSpeed
		, float p_CloudOpacity, float p_HorizonFalloff);
	/**
	* \brief default destructor
	*/
	virtual ~SkyBox() { glDeleteBuffers(1, &m_SkyBoxDataBuffer); }
	/**
	* \brief SkyBox actor can't be reseted
	*/
	virtual void reset() final {}
	/**
	* \brief SkyBox actor can't be translated, does nothing
	*/
	void translate(const glm::vec3& p_MoveFor) final {}
	/**
	* \brief SkyBox actor can't be translated, does nothing
	*/
	void moveTo(const glm::vec3& p_NewPos) final {}
	/**
	* \brief SkyBox actor can't be scaled, does nothing
	*/
	void scale(const glm::vec3& p_NewScale) final {}
	/**
	* \brief SkyBox actor can't be scaled, does nothing
	*/
	void scaleRelative(const glm::vec3& p_NewScale) final {}
	/**
	* \brief SkyBox actor can't be oriented, does nothing
	*/
	void orient(const glm::fquat& p_NewOrientation) final {}
	/**
	* \brief SkyBox actor can't be rotated, does nothing
	*/
	void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) final {}
	/**
	* \brief SkyBox actor is always loaded, does nothing
	*/
	void load() final {}
	/**
	* \brief SkyBox actor doesn't need advanced free, does nothing
	*/
	void free() final {}
	/**
	* \brief renders skybox using MatrixStack to perform transformations
	* @param p_ViewMatrix in this state it is just View matrix
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const override;
	/**
	* \brief SkyBox cannot collide with anything
	* \returns false always
	*/
	bool collides(const ACollidableBase* p_Other) final { return false; }

	/**
	* \brief accepts new soft params and reloads uniform buffer to GL context
	* @param p_Time time after program start
	* @param p_SunDirection normalized sun direction vector
	* @param p_MoonDirection normalized moon direction vector
	* @param p_ZenithColor color of zenith
	* @param p_HorizonColor color of horizon
	* @param p_StarsBrightness stars brightness
	*/
	void update(float p_Time
		, const glm::vec3& p_SunDirection, const glm::vec3& p_MoonDirection
		, const glm::vec3& p_ZenithColor, const glm::vec3& p_HorizonColor, float p_StarsBrightness);
	/**
	* \brief struct same as GLSL skybox shader has
	*/
	struct SkyBoxData
	{
		/**
		* \brief constructs SkyBoxData struct representing one in GLSL shader program
		* @param p_CloudColor color of clouds
		* @param p_CloudSpeed speed of clouds
		* @param p_CloudOpacity opacity of clouds
		* @param p_HorizonFalloff exponent value, that represents place on skybox, where horizon changes to zenith
		*/
		SkyBoxData(const glm::vec3& p_CloudColor, float p_CloudSpeed
			, float p_CloudOpacity, float p_HorizonFalloff);
		glm::vec3 m_SunDirection;
		float m_Time;
		glm::vec3 m_ZenithColor;
		float m_CloudSpeed;
		glm::vec3 m_HorizonColor;
		float m_CloudOpacity;
		glm::vec3 m_CloudColor;
		float m_HorizonFalloff;
		glm::vec3 m_MoonDirection;
		float m_StarsBrightness;
	};

private:
	SkyBoxData m_Skybox;
	GLuint m_SkyBoxDataBuffer;
};

/**
* \brief class represents animated effect in scene, that behaves like sprite (always faced to camera)
*/
class EffectsActor : public AActorBase {
public:
	/**
	* \brief constructor builds instance of effects actor
	* @param p_InitWorldPos init world position
	* @param p_InitScale init scale
	* @param p_TexScale integer, providing how many tiles does animated effect have
	* @param p_AlphaTimeLoc location of alpha time in shader
	*/
	explicit EffectsActor(const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, int32_t p_TexScale
		, GLuint p_AlphaTimeLoc);
	/**
	* \brief default destructor, frees mesh, does nothing
	*/
	virtual ~EffectsActor() { delete m_Mesh; }
	/**
	* \brief EffectsActor cannot be reset, does nothing
	*/
	virtual void reset() final {}
	/**
	* \brief EffectsActor cannot be translated, does nothing
	*/
	void translate(const glm::vec3& p_MoveFor) final {}
	/**
	* \brief EffectsActor cannot be translated, does nothing
	*/
	void moveTo(const glm::vec3& p_NewPos) final {}
	/**
	* \brief EffectsActor cannot be scaled, does nothing
	*/
	void scale(const glm::vec3& p_NewScale) final {}
	/**
	* \brief EffectsActor cannot be scaled, does nothing
	*/
	void scaleRelative(const glm::vec3& p_NewScale) final {}
	/**
	* \brief EffectsActor cannot be oriented, does nothing
	*/
	void orient(const glm::fquat& p_NewOrientation) final {}
	/**
	* \brief EffectsActor cannot be rotate, does nothing
	*/
	void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) final {}
	/**
	* \brief EffectsActor already loaded, does nothing
	*/
	void load() final {}
	/**
	* \brief EffectsActor doesn't need advanced free, does nothing
	*/
	void free() final {}
	/**
	* \brief first things first inverses View transform rotation and adds only roll component of this after applying init transform matrix
	* @param p_ViewMatrix just View matrix
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const override;
	/**
	* \brief EffectsActor cannot collide with anything
	* \returns false always
	*/
	bool collides(const ACollidableBase* p_Other) final { return false; }
	/**
	* \brief sets alpha time [0, 1] based on this animation can work in shader
	* @param p_AlphaTime alpha time from loop timer
	*/
	void setAlpha(float p_AlphaTime) { m_AlphaTime = p_AlphaTime; }
	/**
	* \returns Material, connected with EffectActor, pointer 
	*/
	Material* getConnectedMaterialPtr() { return m_Mesh ? m_Mesh->getConnectedMaterialPtr(0) : nullptr; }
private:
	float m_AlphaTime;
	GLuint m_AlphaTimeLoc;
	GLuint m_Texture;
};




/*

class Impostor : AActorBase
{
};

*/


#endif // !__WORLD_OBJECTS_H
