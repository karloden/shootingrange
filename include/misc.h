#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __MISC_H
#define __MISC_H

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#endif
#include <GL/freeglut.h>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>



const float PI = 3.14159265358979323f;
const float g_MapSize = 470.f;

/**
* \brief used to correctly find mandatory resources, no matter how and where from application was executed
* @return string, representing full path to binary
*/
std::string getPathToBinary();
const std::string g_PathToBinary = getPathToBinary();



/**
* \brief takes a string to convert '\b' symbols as actual char erasing.
* Used to get rid of odd behaviour of SDL_ShowMessageBox
* @param p_String input string
* @return output string
*/
std::string convertBackspaces(const std::string &p_String);

/**
* \brief tries to find file by provided finename
* \returns found file path
* \throws FileException if file cannot be found
*/
std::string findFileOrThrow(const std::string &strBasename);



/**
* \brief converts degrees to radians
*/
inline float degreesToRadians(float deg) {
	static const float radInDeg = PI / 180;
	return radInDeg * deg;
}
#ifdef FREEGLUT
/**
* \brief warps cursor to screen coords with glut function
*/
inline void cursorToWinPosition(uint32_t x, uint32_t y) {
	glutWarpPointer(x, y);
}
#endif




#endif //!__MISC_H