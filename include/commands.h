#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER


#ifndef __COMMANDS_H
#define __COMMANDS_H

#include "mesh.h"

/**
* \brief Command pattern used for different manipulations of different actors using native C++ VTABLE
*/
class Command {
public:
	Command() {}
	virtual ~Command() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start) = 0;
};

/**
* \brief Command pattern, that starting actor's movement forward on execute
*/
class MoveForward : public Command {
public:
	MoveForward() {}
	virtual ~MoveForward() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting actor's movement forward on execute
*/
class MoveBackward : public Command {
public:
	MoveBackward() {}
	virtual ~MoveBackward() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting actor's movement forward on execute
*/
class MoveStrafeLeft : public Command {
public:
	MoveStrafeLeft() {}
	virtual ~MoveStrafeLeft() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting actor's movement forward on execute
*/
class MoveStrafeRight : public Command {
public:
	MoveStrafeRight() {}
	virtual ~MoveStrafeRight() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting actor's movement forward on execute
*/
class MoveJump : public Command {
public:
	MoveJump() {}
	virtual ~MoveJump() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting PLAYERS's leaning left on execute
* "PLAYERS's" means, that execution can be called on Player class instances only
*/
class PlayerLeanLeft : public Command {
public:
	PlayerLeanLeft() {}
	virtual ~PlayerLeanLeft() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that starting PLAYERS's leaning right on execute
* "PLAYERS's" means, that execution can be called on Player class instances only
*/
class PlayerLeanRight : public Command {
public:
	PlayerLeanRight() {}
	virtual ~PlayerLeanRight() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that toggles PLAYERS's flashlight on execute
* "PLAYERS's" means, that execution can be called on Player class instances only
*/
class PlayerToggleFlashlight : public Command {
public:
	PlayerToggleFlashlight() {}
	virtual ~PlayerToggleFlashlight() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that toggles PLAYERS's fly mode on execute
* "PLAYERS's" means, that execution can be called on Player class instances only
*/
class PlayerToggleFly : public Command {
public:
	PlayerToggleFly() {}
	virtual ~PlayerToggleFly() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
};

/**
* \brief Command pattern, that switches PLAYERS's camera view on execute
* "PLAYERS's" means, that execution can be called on Player class instances only
*/
class MoveToggleView : public Command {
public:
	MoveToggleView() { m_CurrView = 0; }
	virtual ~MoveToggleView() {}
	/**
	* \brief executes command
	* @param p_Actor pointer to movable actor which must execute command
	* @param p_Start true if key was pushed, false if it was realesed
	*/
	virtual void execute(MovableMesh *p_Actor, bool p_Start);
private:
	const std::vector<glm::vec3> m_Positions = 
	{	  glm::vec3(99.63f, 0.f, -8.28f)
		, glm::vec3(88.22f, 18.51f, 9.56f)
		, glm::vec3(-47.66f, 24.97f, 52.8675f)};
	const std::vector<glm::fquat> m_Orientations = 
	{	
		  glm::fquat(-0.68966f, -0.0262239f, 0.723141f, 0.027497f)
		, glm::fquat(0.733728f,0.109396f,-0.663261f,-0.0988906f)
		, glm::fquat(0.915465f, 0.0348098f, 0.400613f, 0.0152333f)};
	int8_t m_CurrView;
};

#endif //!__COMMANDS_H