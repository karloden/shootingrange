#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER


#ifndef __PARAMETERS_H
#define __PARAMETERS_H

#include <fstream>
#include <cstdint>
#include <vector>
#include <iostream>
#include <map>
#include <atomic>
#include "commands.h"
#include "misc.h"


//Compile time parameters


const std::string g_ResourcesPath = g_PathToBinary + "/data/";
const std::string g_PropsPath = g_ResourcesPath + "props/";
const std::string g_TexturesPath = g_PropsPath + "textures/";
const std::string g_ShadersPath = g_ResourcesPath + "shaders/";

const std::string g_ConfigDefault = g_ResourcesPath + "config/base_template.cfg";
const std::string g_Config = g_ResourcesPath + "config/base.cfg";

/**
 * \brief class used to get and set game parameters of all purposes
 */
class Parameters {
public:
	/**
	 * \brief Sets config files paths
	 */
	Parameters();

	/**
	 * \parblock Prohibits instance being copied or moved
	 */
	Parameters(const Parameters &) = delete;

	Parameters &operator=(const Parameters &) = delete;

	Parameters(const Parameters &&) = delete;

	Parameters &operator=(const Parameters &&) = delete;
	/**
	* \endparblock
	*/

	/**
	 * \brief Cleans memory, probably allocated by instance.
	 */
	~Parameters();

	/**
	 * \returns resolution X.
	 */
	int32_t resX() const { return m_ResX; }

	/**
	 * \returns resolution Y.
	 */
	int32_t resY() const { return m_ResY; }

	/**
	 * \returns Fullscreen flag.
	 */
	bool fullscreen() const { return m_Fullscreen; }

	/**
	 * \returns Camera sensitivity.
	 */
	float cameraSens() const { return m_Sens; }


	/**
	 * \returns true if all configuration parameters was initialized correctly.
	 */
	bool allParametersInitialized() const { return m_AllInit; }

	/**
	  * \returns serialized pointer to array, used to insert in a save file
	 */
	void* serialize();

	/**
	* \brief Parses and sets graphics parameters from files (default and then user's)
	* \throws ConfigFileFormatException if one of 3 category (game, buildings , graphics, was not found)
	*/
	void readConfig();

	/**
	 * \brief returns const pointer to key mapping map
	 * first is ASCII code of callback symbol, second is class inherited from Command pattern class
	 * \returns key mapping map
	*/
	const std::map<char, Command*>* getControlMap() const { return &m_ControlsMap;  }

private:


	/**
	 * \brief Parses changeable graphics parameters from vector
	 * \throws ConfigFileFormatException if one of parameters was not found
	 * @param parameters vector with parameters
	 */
	void parseGraphics(const std::vector<std::string> &parameters);

	/**
	* \brief Parses changeable bindings parameters from vector
	* \throws ConfigFileFormatException if one of parameters was not found
	* @param parameters vector with parameters
	*/
	void parseBindings(const std::vector<std::string> &parameters);

	/**
	 * \brief Parses changeable game parameters from vector
	 * \throws ConfigFileFormatException if one of parameters was not found
	 * @param parameters vector with parameters
	 */
	void parseGame(const std::vector<std::string> &parameters);

	/**
	 * \brief Checks if user's file exists and copying it from default config if not.
	 * \throws FileOpenException if nor of them exist
	 * @param is input stream where file might to be opened
	 * @param paramPath path to file
	 */
	void checkAndRecoverConfig(std::ifstream &paramStream, const std::string &paramPath) const;

	/**
	* \brief Parses all parameters from the file, using specified functions
	* For consistence of program work this function is called twice to get default configs from file
	* if configs in user's .cfg file doesn't exist.
	 * \throws ConfigFileFormatException if none of category or line in one of config files was found
	* @param paramPath is a path to config file.
	*/
	void fillMap(const std::string &paramPath, std::map<std::string, std::vector<std::string>> &parameters);

	/**
	* \brief takes map with string names of special keys and assigned chars and returns that char
	* @param p_LookingFor string name of char
	* @param p_SpecialKeyMaps map with special keys
	*/
	char specialKeyStringToASCII(const std::string &p_LookingFor, const std::map <std::string, char> & p_SpecialKeyMaps) const;

	uint32_t m_ResX, m_ResY;
	float m_Sens;
	bool m_AllInit;
	bool m_Fullscreen;
	bool m_LoadedGame;
	std::map<char, Command*> m_ControlsMap;
};


#endif //!__PARAMETERS_H
