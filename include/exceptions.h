#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __EXCEPTIONS_H
#define __EXCEPTIONS_H

#include <assimp/Importer.hpp>
#include <exception>
#include <string>
#include "misc.h"

class EngineException : public std::exception {
public:
	/**
	* \brief Constructs exception from C string.
	*  @param message C-style string error message.
	*/
	explicit EngineException(const char* p_Message) : m_Message(convertBackspaces(p_Message)) {}

	/**
	* \brief Constructs exception from STL string.
	*  @param message The error message.
	*/
	explicit EngineException(const std::string& p_Message) : m_Message(convertBackspaces(p_Message)) {}

	/**
	* \brief Destructor. Virtual to allow for subclassing.
	*/
	virtual ~EngineException() throw () {}

	/**
	* \brief Returns a pointer to the (constant) error description.
	*  @return A pointer to a const char*. The underlying memory
	*          is in posession of the Exception object. Callers must
	*          not attempt to free the memory.
	*/
	const char* what() const throw () { return m_Message.c_str(); }

private:
	/** Error message.
	*/
	const std::string m_Message;
};

class ModuleException : public EngineException {
public:
	/**
	* \brief Constructs module exception from C string.
	*  @param message C-style string error message.
	*/
	explicit ModuleException(const char* p_ModuleName, const char* p_HadToMakeJob) :
		EngineException("Module " + std::string(p_ModuleName) + " failed, making: " + std::string(p_HadToMakeJob)) {}

	/**
	* \brief Constructs module exception from STL string.
	*  @param message The error message.
	*/
	explicit ModuleException(const std::string& p_ModuleName = "\b", const std::string& p_HadToMakeJob = "\b\b it's job") :
		EngineException("Module " + p_ModuleName + " failed, making: " + p_HadToMakeJob) {}

};

class FileException : public EngineException {
public:
	/**
	* \brief Constructs file exception from C string.
	*  @param message C-style string error message.
	*/
	explicit FileException(const char* p_FileName, const char* p_HadToMakeJob) :
		EngineException("File " + std::string(p_FileName) + " loading failed: " + std::string(p_HadToMakeJob)) {}

	/**
	* \brief Constructs file exception from STL string.
	*  @param message The error message.
	*/
	explicit FileException(const std::string& p_FileName = "\b", const std::string& p_HadToMakeJob = "\b\b.") :
		EngineException("File " + p_FileName + " loading failed: " + p_HadToMakeJob) {}
};

class LogicException : public EngineException {
public:
	/**
	* \brief Constructs logic exception from C string.
	*  @param message C-style string error message.
	*/
	explicit LogicException(const char* p_Part, const char* p_HadToMakeJob) :
		EngineException(std::string(p_Part) + std::string(p_HadToMakeJob)) {}

	/**
	* \brief Constructs logic exception from STL string.
	*  @param message The error message.
	*/
	explicit LogicException(const std::string& p_Part, const std::string& p_HadToMakeJob) :
		EngineException(p_Part + p_HadToMakeJob) {}
};

typedef Assimp::Importer OBJImporter;
typedef aiScene MeshesPtr;

class ShaderException : public EngineException {
public:
	/**
	* \brief Constructs shader exception from C string.
	*  @param message C-style string error message.
	*/
	explicit ShaderException(const char* p_ShaderFileName = "Shader", const char* p_HadToMakeJob = "\b") :
		EngineException(std::string(p_ShaderFileName) + " compile failed. " + std::string(p_HadToMakeJob)) {}

	/**
	* \brief Constructs shader exception from STL string.
	*  @param message The error message.
	*/
	explicit ShaderException(const std::string& p_ShaderFileName = "Shader", const std::string& p_HadToMakeJob = "\b") :
		EngineException(p_ShaderFileName + " compile failed. " + p_HadToMakeJob) {}
};

class ObjectConstructionException : public EngineException {
public:
	/**
	* \brief Constructs shader exception from C string.
	*  @param message C-style string error message.
	*/
	explicit ObjectConstructionException(
		const char* p_ObjectName = "Object", const char* p_Problem = "\b") :
		EngineException(std::string(p_ObjectName) + " object construction failed. " + std::string(p_Problem)) {}

	/**
	* \brief Constructs shader exception from STL string.
	*  @param message The error message.
	*/
	explicit ObjectConstructionException(
		const std::string& p_ObjectName = "Object", const std::string& p_Problem = "\b") :
		EngineException(p_ObjectName + " object construction failed. " + p_Problem) {}
};

class ConfigFileFormatException : public EngineException {
public:
	/**
	* \brief Constructs configuration file exception from C string.
	*  @param message C-style string error message.
	*/
	explicit ConfigFileFormatException(const char*p_FileName = "File", const char *p_WrongString = "\b\b.")
		: EngineException(std::string(p_FileName) +
			" has wrong format! Unexpected or invalid entry found: " + std::string(p_WrongString)) {}

	/**
	* \brief Constructs configuration file from STL string.
	*  @param message The error message.
	*/
	explicit ConfigFileFormatException(const std::string &p_FileName = "File", const std::string &p_WrongString = "\b\b.")
		: EngineException(p_FileName +
			" has wrong format! Unexpected or invalid entry found: " + p_WrongString) {}

	virtual ~ConfigFileFormatException() {}
};

class OutOfRangeException : public EngineException {
public:
	/**
	* \brief Constructs array out of range exception from C string.
	*  @param message C-style string error message.
	*/
	explicit OutOfRangeException(const char *p_Object = "Array")
		: EngineException(std::string(p_Object) + " out of range") {}

	/**
	* \brief Constructs array out of range exception from STL string.
	*  @param message The error message.
	*/
	explicit OutOfRangeException(const std::string &p_Object = "Array")
		: EngineException(p_Object + " out of range") {}

	virtual ~OutOfRangeException() {}
};


#endif // !__EXCEPTIONS_H


