#ifdef _MSC_VER
#pragma once
#endif
#ifndef __PLAYER_H
#define __PLAYER_H

#include <glm/glm.hpp>
#include "world_objects.h"
#include "mesh.h"
#include "collision.h"
#include "scene_handler.h"


/**
* \brief class represents player in the world, has all properties, that movable mesh has, also can 'pilot' a camera
*/
class Player : public MovableMesh {
public:
	/**
	 * \brief constructs Player instance
	 * @param p_Scene pointer to scene, where player is located
	 * @param p_StartPos provides position, where Player starts
	*/
	Player(SceneHandler *p_Scene, const glm::vec3& p_StartPos);
	/**
	 * \brief default destructor
	*/
	virtual ~Player() {}

	/**
	* \brief accelerates Player according to m_ActionStateMachine current state, gravity and states like jumping flying etc
	* if Player is flying, no gravity is applied
	*/
	virtual void accelerate() override;
	/**
	* \brief applies acceleration (making movement) and atttenuates it, calculates collisions 
	* if Player is flying, then collisions don't compute
	*/
	virtual void applyAcceleration() override;
	/**
	 * \brief player cannot be scaled, does nothing
	*/
	void scale(const glm::vec3& p_NewScale) final {}
	/**
	* \brief player cannot be scaled, does nothing
	*/
	void scaleRelative(const glm::vec3& p_NewScale) final {}
	/**
	* \brief renders weapon in hands if it is present
	* @param p_ViewMatrix here doesn't matter, because weapon is thing that might be already in camera space
	*/
	virtual void render(MatrixStack & p_ViewMatrix) const final;
	/**
	* \brief enables and disables flashlight
	*/
	void toggleFlashLight();
	/**
	* \brief enables and disables flying mode
	*/
	void toggleFlyMode() { m_Flying = !m_Flying; }
	/**
	* \brief enables and disables movement blocking
	* @param p_Block true if block, false if release
	*/
	void toggleMoveBlock(bool p_Block);
	/**
	* \brief calculates new orientation based on new screen coords
	*/
	void lookAtScreenCoords(int32_t p_NewScreenX, int32_t p_NewScreenY);
	/**
	* \brief find object in the world, which bounds the world-space point
	* @param p_WorldPos point in world space
	* \returns pointer to AActorBase which enclose point in the world
	*/
	AActorBase* resolvePicking(const glm::vec3 &p_WorldPos);
	/**
	* \brief takes to hands pickable object
	* @param p_Pickable pointer to pickable activator
	*/
	void interactWithActiv(const Activator* p_Pickable, const glm::vec3& p_WorldPoint);
	/**
	* \brief starts jump timer coundown
	*/
	void jump();
	/**
	* \brief applies shooter-like roll to left and right, simulating leaning
	* @param p_Roll radians angle of rotation
	* @param p_Start true if key was PRESSED, false if key was released
	*/
	void applyAdditionalRoll(float p_Roll, bool p_Start) { m_Lean += p_Start ? p_Roll : -p_Roll; }
	/**
	* \brief method used to get viewMatrix from Player
	* \returns viewMatrix transformation
	*/
	virtual glm::mat4 viewFromActor() const override { return Camera::calcViewMatrix(m_WorldPos, m_Orientation, m_Lean); }

	/**
	 * \returns player world position
	*/
	const glm::vec3 &getCurrPosition() { return m_WorldPos; }

private:
	///Jump acceleration
	const float m_JumpSpeed = 0.02f;
	///Half of player width for calculating collisions
	const float m_HalfPlayerWidth = 2.f;
	///Half of player height for calculating collisions
	const float m_HalfPlayerHeight = 4.5f;
	///Player collision box
	CollidableBox m_CollBox;
	///Ptr to scene, player is in
	SceneHandler *m_Scene;
	///Object picked to hands
	const Activator* m_Pickable;
	float m_Lean;
	bool m_Flying;
	bool m_Blocked;
	bool m_FlLgtEnabled;
	int32_t m_JumpingTimer;
	uint32_t m_FlashLightInd;

};

#endif //!__PLAYER_H
