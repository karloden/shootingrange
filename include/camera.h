#ifdef _MSC_VER
#pragma once
#endif

#ifndef __CAMERA_H
#define __CAMERA_H

#include <vector>
#include <gl_core_4_4.h>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>


/**
* \brief used to manipulate camera with mouse and keyboard
*/
class Camera {
public:
	/**
	* \brief constructor used to set values from parameters and allocates Projection matrix uniform block buffer in opengl
	*/
	Camera(int32_t p_WindowWidth, int32_t p_WindowHeight, float p_MouseSens);

	/**
	* \brief called on window reshape 
	*/
	void screenReshape(int32_t resX, int32_t resY);

	/**
	* \brief called on changing field of view
	* @param p_Diff difference between old FoV and new FoV
	*/
	void changeFieldOfView(float p_Diff);

	/**
	* \brief called on motion callback, warps cursor to window center and calculates new orientation based on offset of old and new coords
	* \returns new orientation represented by quaternion
	*/
	glm::fquat rotateCamera(const glm::fquat &p_OldOrientation, int32_t p_NewScreenX, int32_t p_NewScreenY) const;

	/**
	* \brief calculates view matrix based on provided params
	* \returns View Matrix (world to camera)
	* @param p_Position world space coordinates
	* @param p_Orientation orientation quaternion
	* @param p_Lean additional camera roll, used to simulate lean
	*/
	static glm::mat4 calcViewMatrix(const glm::vec3& p_Position, const glm::fquat& p_Orientation, float p_Lean);

	/**
	* \brief changes camera sensitivity, that affects only mouse motion
	*/
	void changeSensitivity(float p_MouseSens) { m_MouseSens = p_MouseSens * 0.001f; }

	/**
	* \brief returns const reference to current projection matrix
	* \returns const reference to current projection matrix
	*/
	const glm::mat4 &getCurrentProjMat() { return m_Proj.Pmatrix; }
private:
	/**
	* \brief recalculates projection block using new provided values (window size, fov) and reloads it to opengl
	*/
	void recalculateProjectionBlock();
	struct ProjectionBlock
	{
		glm::mat4 Pmatrix;
	};
	float m_Fov;
	float m_MouseSens;
	int32_t m_WindowWidth, m_WindowHeight;
	GLuint m_ProjectionUniformBuffer;
	ProjectionBlock m_Proj;
	
};

#endif // !__CAMERA_H

