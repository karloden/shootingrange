#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __GAMESTATE_H
#define __GAMESTATE_H

#include "mesh.h"

/**
 * \brief timer class is used to provide time-based changes to dynamic objects and control interpolators
*/
class Timer
{
public:
	/**
	* \brief resets timer (sets it to state when it was constructed)
	*/
	void reset();

	/**
	* \brief updates timer
	* \returns true if timer reached end
	*/
	bool update();

	/**
	* \brief representing progress through the duration. For single and loop timers ([0, 1])
	*/ 
	float getAlpha() const;

	/**
	* \brief representing time through the duration. For single and loop timers ([0, duration])
	*/
	float getProgression() const;

	/**
	* \brief representing global time since timer started ([0, inf])
	*/
	float getTimeSinceStart() const;

	/**
	 * \returns timer duration, that was provided on Timer construction
	*/
	float getDuration() const { return m_SecDuration; }

	/**
	 * \brief toggles timer's pause
	 * @param p_Pause true if timer ought to be paused, false to continue
	*/
	void togglePause(bool p_Pause = true) { m_Paused = p_Pause; }

	/**
	* \brief enum chooses type of timer
	*/
	enum Type : uint8_t
	{
		Loop = 0, Single, Infinite, TimerTypesNum,
	};

	/**
	* \brief creates a timer with the given type.
	**/
	Timer(Type eType = Infinite, float p_Duration = 1.0f);

private:
	Type m_Type;
	float m_SecDuration;

	bool m_HasUpdated;
	bool m_Paused;

	float m_AbsPrevTime;
	float m_SecAccumTime;
};

/**
 * \brief Activator class used to build simple game logic, like object picking or implementing shooting targets logic
*/
class Activator : public StaticMesh {
public:
	/**
	* \brief Represents type of activator and expected usage
	*/
	enum ActivatorType : uint8_t { Pickable=0, Targetable };

	/**
	* \brief Constructor builds instance
	* @param p_Type type of activator
	* @param p_FileName specifies mesh filename
	* @param p_InitWorldPos init world position
	* @param p_InitScale init scale
	* @param p_InitRawDegrees init raw degrees of rotations along x,y,z axis in vector
	* @param p_Center center of target
	* @param p_MeshInd material submesh, that is expected to load from p_FileName, -1 for whole model
	*/
	explicit Activator(ActivatorType p_Type
		, const std::string &p_FileName
		, const glm::vec3& p_InitWorldPos
		, const glm::vec3& p_InitScale
		, const glm::vec3& p_InitRawDegrees
		, const glm::vec3& p_Center = glm::vec3(0.f)
		, int32_t p_MeshInd = -1);

	/**
	 * \brief returns point number based on provided point
	 * @param p_WorldPoint point was shoted
	*/
	int32_t getPoints(const glm::vec3 & p_WorldPoint) const;

	/**
	* \brief returns distance between provided point and (this) object's center
	* @param p_WorldPoint <- from this to this -> center
	*/
	float getDistance(const glm::vec3 & p_WorldPoint) const;

private:
	/**
	* \brief constructs Activator instance from another. Looses init transformations. Have to be used only for picking in hands by Player
	*/
	explicit Activator(const Activator& p_Other);

	/**
	* \brief unusial function used to render weapon in player's hands
	* have to be called only from Player class
	*/
	void renderPickableInHands(MatrixStack& p_ViewMatrix) const;
	
	const glm::vec3 m_Center;
	ActivatorType m_Type;
	friend class Player;
};

#endif //!__GAMESTATE_H