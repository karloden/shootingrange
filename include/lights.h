#ifdef _MSC_VER
#pragma once
#endif

#ifndef __LIGHTS_H
#define __LIGHTS_H
#include <vector>

#include "world_objects.h"



/**
* \brief base abstract class representing light source in scene, has common logic to work with lights in VRAM
*/
class ALightBase : public AActorBase {
public:
	/**
	 * \brief default destructor
	*/
	virtual ~ALightBase() {}

	/**
	* \brief abstract method might used to simple-transform light sources(change directions or positions)
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override = 0;

	/**
	* \brief abstract method might used to simple-transform light sources(change directions or positions)
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override = 0;

	/**
	* \brief abstract method might used to transform light sources(rotate)
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override = 0;

	/**
	* \brief abstract method might used to calculate light positions to camera space and reload lights to GL context
	*/
	virtual void updateCamSpPos(const glm::mat4 & p_ViewMatrix) = 0;

	/**
	* \brief manipulates light intensity by setting average of 3 vector components as new intensity
	* @param p_NewScale vector, that will be transformed at intensity
	*/
	void scale(const glm::vec3& p_NewScale) final;

	/**
	* \brief manipulates light intensity by scaling (multiplying) intensity value by average of 3 vector components
	* @param p_NewScale vector, that will be transformed at intensity
	*/
	void scaleRelative(const glm::vec3& p_NewScale) final;

	/**
	 * \brief sets light values, that was provided on initialization
	*/
	void reset() final;

	/**
	* \brief light cannot be oriented by quaterion, so does nothing
	*/
	void orient(const glm::fquat& p_NewOrientation) final {}

	/**
	* \brief light cannot be rendered, so does nothing
	*/
	void render(MatrixStack & p_ViewMatrix) const final {}

	/**
	* \brief light cannot be collidable object
	* \returns false always
	*/
	bool collides(const ACollidableBase* p_Other) final { return false; }

	/**
	* \brief light is always initialized if it exists, so does nothing
	*/
	void load() final {}

	/**
	* \brief light is always initialized if it exists, and cannot be free, so does nothing
	*/
	void free() final {}

	/**
	 * \brief struct represents light source data layout, like GLSL shader has in order to provide binary compatibility
	 * no need for padding if value of size 4 goes imidiatly after vec3
	*/
	struct ShaderLightSource
	{
		ShaderLightSource(const glm::vec3& p_LightColor
			, float p_InitIntensity
			, float p_InitAttenuation
			, uint32_t p_Type);
		glm::vec3 m_CamSpLightPos;
		float m_LightIntensity;
		glm::vec3 m_CamSpLightDir;
		float m_LightAttenuation;
		glm::vec3 m_LightColor;
		const uint32_t m_Type;
	};

	/**
	 * \brief bit-wise mask, used to provide light type to GLSL shader program
	*/
	enum LightType : uint32_t {
		Directional	= 0x00000001u,
		Point		= 0x00000002u,
		Spot		= 0x00000004u,
		LgtTypeMask = 0x0000000fu,
		Phong		= 0x00000010u,
		Gauss		= 0x00000020u,
		SpecMask	= 0x000000f0u
	};
protected:
	/**
	* \brief protected constructor, used by inherited classes in order to initialize their parent
	* @param p_LightColor represents light's color
	* @param p_InitIntensity intensity, that light source will have. Not bound or clamped, can be any value (supports HDR)
	* @param p_HalfLightDistance distance, where light will have half of intensity, used to set attenuation
	* @param p_Type integer representation of LightType enum, in order to have consistance with glsl shader program
	*/
	ALightBase(const glm::vec3& p_LightColor
		, float p_InitIntensity
		, float p_HalfLightDistance
		, int32_t p_Type);
	ShaderLightSource m_LightData;
	const float m_InitIntensity;
	const float m_HalfLightDistance;
	const glm::vec3 m_InitColor;
	friend class LightManager;
};

typedef ALightBase::ShaderLightSource DShaderLgtSrc;
typedef ALightBase::LightType LightType;

/**
* \brief Directional light source class represents sun-like light sources, doesn't attenuate, has light direction same for whole scene
* have to be handled by light manager
*/
class DirectionalLight : public ALightBase {
public:
	/**
	 * \brief constructs directional light source
	 * aware of having more than 3 of dir lights, having approximatly same directions, 
	 * because they will probably cause undefined specular behavior
	 *
	 * @param p_DirVector vector, providing direction of light, will be implicitly normalized
	 * @param p_LightColor represents light's color
	 * @param p_InitIntensity intensity, that light source will have. Not bound or clamped, can be any value (supports HDR)
	 * @param p_SpecularGauss provide true if you want to use Gauss specular component. 
	 * BEWARE: setting p_SpecularGauss to true will force you to set every Material.m_Shininess to extremely low values
	*/
	DirectionalLight(const glm::vec3& p_DirVector
		, const glm::vec3& p_LightColor
		, float p_InitIntensity
		, bool p_SpecularGauss = false);

	/**
	 * \brief default destructor
	*/
	virtual ~DirectionalLight() {}

	/**
	* \brief sets light direction to average normalized vectors of current and provided
	* @param p_MoveFor provided direction vector
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override { m_LightDirVect = glm::normalize(m_LightDirVect + glm::normalize(p_MoveFor)); }
	
	/**
	 * \brief sets light direction to the provided one
	 * @param p_NewPos provided direction vector
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override { m_LightDirVect = glm::normalize(p_NewPos); }

	/**
	* \brief method used to calculate light direction to camera space and reload lights to GL context
	* @param p_ViewMatrix view matrix (world to camera)
	*/
	virtual void updateCamSpPos(const glm::mat4 & p_ViewMatrix) override;

	/**
	 * \brief rotates light direction vector around specified axis for specified radians counter clock-wise
	 * @param p_AxisOfRotation axis of rotation
	 * @param p_AngRadCCW radians count
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override;
private:
	glm::vec3 m_LightDirVect;
};


/**
* \brief Point light source class like room light bulb, does attenuate, has changeable intensity, world position, light color
* have to be handled by light manager
*/
class PointLight : public ALightBase {
public:
	/**
	* \brief constructs point ligth instance
	* @param p_WorldPos world space position coordinates
	* @param p_LightColor represents light's color
	* @param p_InitIntensity intensity, that light source will have. Not bound or clamped, can be any value (supports HDR)
	* @param p_HalfLightDistance distance, where light will have half of intensity, used to set attenuation
	* @param p_SpecularGauss provide true if you want to use Gauss specular component.
	* BEWARE: setting p_SpecularGauss to true will force you to set every Material.m_Shininess to extremely low values
	*/
	PointLight(const glm::vec3& p_WorldPos
		, const glm::vec3& p_LightColor
		, float p_InitIntensity
		, float p_HalfLightDistance
		, bool p_SpecularGauss = false);
	/**
	* \brief default destructor
	*/
	virtual ~PointLight() {}

	/**
	 * \brief moves light position using provided vector as offset
	 * @param p_MoveFor offset translation
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override { m_WorldSpLightPos += p_MoveFor; }

	/**
	* \brief usign provided vector as new position, moves light position to it
	* @param p_MoveFor new position
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override { m_WorldSpLightPos = p_NewPos; }

	/**
	* \brief method used to calculate light position to camera space and reload lights to GL context
	* @param p_ViewMatrix view matrix (world to camera)
	*/
	virtual void updateCamSpPos(const glm::mat4 & p_ViewMatrix) override;

	/**
	 * \brief rotation of point light does nothing
	 * @param p_AxisOfRotation axis of rotation
	 * @param p_AngRadCCW radians count
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override {}
private:
	glm::vec3 m_WorldSpLightPos;
};


/**
* \brief Spot (projected) lights sources class like projectors, spot lights etc. Supports changeable diameter among other light sources params
*/
class SpotLight : public ALightBase {
public:
	/**
	* \brief constructs point ligth instance
	* @param p_WorldPos world space position coordinates
	* @param p_DirVector vector, providing direction of light, will be implicitly normalized
	* @param p_LightColor represents light's color
	* @param p_InitIntensity intensity, that light source will have. Not bound or clamped, can be any value (supports HDR)
	* @param p_Diameter diameter of light spot, accepts [50, 100], other will be clamped
	* @param p_SpecularGauss provide true if you want to use Gauss specular component.
	* BEWARE: setting p_SpecularGauss to true will force you to set every Material.m_Shininess to extremely low values
	*/
	SpotLight(const glm::vec3& p_WorldPos
		, const glm::vec3& p_DirVector
		, const glm::vec3& p_LightColor
		, float p_InitIntensity
		, float p_Diameter
		, bool p_SpecularGauss = false);

	/**
	* \brief default destructor
	*/
	virtual ~SpotLight() {}

	/**
	* \brief moves light position using provided vector as offset
	* @param p_MoveFor offset translation
	*/
	virtual void translate(const glm::vec3& p_MoveFor) override { m_WorldSpLightPos += p_MoveFor; }

	/**
	* \brief usign provided vector as new position, moves light position to it
	* @param p_MoveFor new position
	*/
	virtual void moveTo(const glm::vec3& p_NewPos) override { m_WorldSpLightPos = p_NewPos; }

	/**
	* \brief method used to calculate light position and direction to camera space and reload lights to GL context
	* @param p_ViewMatrix view matrix (world to camera)
	*/
	virtual void updateCamSpPos(const glm::mat4 & p_ViewMatrix) override;

	/**
	* \brief rotates light direction vector around specified axis for specified radians counter clock-wise
	* @param p_AxisOfRotation axis of rotation
	* @param p_AngRadCCW radians count
	*/
	virtual void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) override;
private:
	glm::vec3 m_LightDirVect;
	glm::vec3 m_WorldSpLightPos;

};

/**
 * \brief Flashlight light source, that player can toggle
 * shares semantics with SpotLight class, despite of fact Flashlight is not inherited from SpotLight
*/
class FlashLight : public ALightBase {
public:
	/**
	* \brief constructs point ligth instance
	* @param p_LightColor represents light's color
	* @param p_InitIntensity intensity, that light source will have. Not bound or clamped, can be any value (supports HDR)
	* @param p_SpecularGauss provide true if you want to use Gauss specular component.
	* BEWARE: setting p_SpecularGauss to true will force you to set every Material.m_Shininess to extremely low values
	*/
	FlashLight(const glm::vec3 & p_LightColor
		, float p_InitIntensity
		, bool p_SpecularGauss = false);

	/**
	* \brief default destructor
	*/
	virtual ~FlashLight() {}

	/**
	 * \brief flashlight is always in same camera-space position, so no need to deal with world coordinates and view transformations, does nothing
	*/
	void translate(const glm::vec3& p_MoveFor) final { }

	/**
	 * \brief flashlight is always in same camera-space position, so no need to deal with world coordinates and view transformations, does nothing
	*/
	void moveTo(const glm::vec3& p_NewPos) final { }

	/**
	* \brief flashlight will have always same coordinates in camera space, does nothing
	* @param p_ViewMatrix view matrix (world to camera)
	*/
	void updateCamSpPos(const glm::mat4 & p_ViewMatrix) final {}

	/**
	* \brief flashlight is always oriented same as player does, so rotation cannot be done, does nothing
	*/
	void rotate(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW) final {}
};



/**
* \brief Class takes care of every light source in scene and uploads them to GL context
*/
class LightManager {
public:
	const uint32_t m_MaxLightSorces = 20;
	const float m_MaxFogIntensity = 0.02f;

	/**
	 * \brief struct represents uniform buffer block layout, like appropriate uniform in GLSL shader
	*/
	struct ShaderLightCorrectionBlock
	{
		/**
		 * \brief constructs struct instance
		 * @param p_Gamma accepts gamma value, to be used legacy-style gamma correction pow(color, gamma)
		*/
		ShaderLightCorrectionBlock(float p_Gamma);
		glm::vec3 m_AmbientIntensity;
		float m_MaxIntensity;
		glm::vec3 m_FogColor;
		float m_FogIntensity;
		float m_Gamma;
		float _padding[3];
	};
	/**
	 * \brief operator[] allows to access light sources directly 
	 * \returns reference to pointer of light source
	 * @param p_LgtInd light index
	*/
	ALightBase *&operator[](uint32_t p_LgtInd);

	/**
	* \brief operator[] allows to access light sources directly
	* \returns const reference to const pointer of light source, that absolutely secures read-only operations
	* @param p_LgtInd light index
	*/
	const ALightBase* const &operator[](uint32_t p_LgtInd) const;

	/**
	 * \brief constructor used to initialize light manager. 
	 * Light manager ought to have only one instance through whole program
	 * @param p_Shaders reference to read-only shader data
	*/
	explicit LightManager(const std::vector<AShaderBase*>& p_Shaders);
	
	/**
	 * \brief prepares and loads lights to GL context, updates light correction block based on light sources values.
	 * to be precise: compute camera space positions for every light in scene, finds the brightest light source and 
	 * sets LightCorrection uniform block's maxIntensity = (brightestLight + ambientLight) * 1.2, 
	 * ambientIntensity = p_AmbientIntAndColor * (m_LgtCorrData.m_MaxIntensity * 0.05f)
	 * @param p_ViewMatrix view matrix transformation (from world to camera)
	 * @param p_AmbientIntAndColor ambient lightning intensity and color vector
	*/
	void updateLights(const glm::mat4 & p_ViewMatrix = glm::mat4(1.f), const glm::vec3& p_AmbientIntAndColor = glm::vec3(0.2f));
	/**
	 * \brief sets fog values, that might to be sent in program shader.
	 * @param p_FogColor vector represents color of fog, not clamped nor divided
	 * @param p_FogIntensity other words - fog density, must be [0, 1]
	*/
	void setFog(const glm::vec3& p_FogColor, float p_FogIntensity);

	/**
	 * \brief takes new light to manage. 
	 * Must be NEW ALLOCATED, in order to avoid heap corruption
	 * @param p_Light pointer to allocated light source class
	*/
	uint32_t addLight(ALightBase* p_Light);

	/**
	 * \brief deletes light source from managed lights list and frees it's memory
	 * @param p_Ind index of light to be deleted
	*/
	void deleteLight(uint32_t p_Ind);

	/**
	 * \brief allows to replace light by index with another light source
	 * Must be NEW ALLOCATED, in order to avoid heap corruption
	 * @param p_Ind index of light to be updated
	 * @param p_Light pointer to allocated light source class
	*/
	void updateLight(uint32_t p_Ind, ALightBase* p_Light);
	/**
	 * \brief default destructor. Frees all light sources
	 */
	~LightManager();
private:
	/**
	* \brief allocates memory for light sources in GL context. 
	* Doesn't upload light sources by it's own automatically
	*/
	void initLgtSrcBlc();
	/**
	* \brief reloads LightCorrectionBlock instance to GL context
	*/
	void updateLgtCorrBlc() const;
	const std::vector<AShaderBase*>& m_Shaders;
	GLuint m_LgtCorrBuffer;
	GLuint m_LgtSrcBuffer;
	ShaderLightCorrectionBlock m_LgtCorrData;
	std::vector<ALightBase*> m_LightSources;
};

typedef LightManager::ShaderLightCorrectionBlock DShaderLgtCtrlBlc;

#endif // !__LIGHTS_H
