#ifdef _MSC_VER
#pragma once
#endif //!_MSC_VER

#ifndef __MATERIALS_H
#define __MATERIALS_H
#define GL_TEXTURE_MAX_ANISOTROPY_EXT 0x84FE

#include <string>
#include <gl_core_4_4.h>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>


/**
* \brief Class that's instance manipulates material of one model or one submesh, that can have separate material
*/
class Material {
public:
	/**
	 * \brief default constructor
	 * sets diffuse and normal textures to black 1x1pix cap in order to consider if mesh has texture set in shader
	*/
	explicit Material();
	///Explicitly allows to copy and move Material instances
	Material(const Material& p_Other) = default;
	Material& operator=(const Material& p_Other) = default;
	Material& operator=(Material&& p_Other) = default;

	/**
	 * \brief default destructor
	 * deletes static samplers once and diffuse and normal textures for every instance if they was set
	*/
	virtual ~Material();

	/**
	 * \brief loads diffuse texture
	 * @param p_FileName name of texture file, that might be found in g_TexturesPath (can be found in parameters.h)
	*/
	void loadDiffuseTexture(const std::string& p_FileName);

	/**
	 * \brief loads normal map texture
	 * @param p_FileName name of texture file, that might be found in g_TexturesPath (can be found in parameters.h)
	*/
	void loadNormalMapTexture(const std::string& p_FileName = "");

	/**
	* \brief sets diffuse color to material, and if diffuse texture is set, this action will unbind and delete this texture
	* @param p_Color diffuse color
	*/
	void setDiffuseColor(const glm::vec3& p_Color);

	/**
	* \brief sets ambient color
	* @param p_Color ambient color
	*/
	void setAmbientColor(const glm::vec3& p_Color);

	/**
	* \brief sets specular color
	* @param p_Color specular color
	*/
	void setSpecularColor(const glm::vec3& p_Color);

	/**
	* \brief sets shininess
	* @param p_Shininess shininess
	*/
	void setShininess(float p_Shininess);
	
	/**
	 * \brief static texture samplers settings
	 * @param p_Create true if this is first applied texture
	*/
	static void samplerRepeatEdge(bool p_Create = false);
	static void samplerClampEdge(bool p_Create = false);
	static void samplerMipMapLin(bool p_Create = false);
	static void samplerAnisotropy(bool p_Create = false);


	/**
	 * \brief sets size of material block to static variable and initializes black texture cap
	 * must be called before any other material methods call
	 * @param p_SizeMaterialBlock size of one material block
	*/
	static void initialize(GLuint p_SizeMaterialBlock);

	/**
	* \brief sets material uniform buffer to static variable
	* must be called before any other material methods call
	* @param p_MtlUniformBuffer provides material uniform buffer
	*/
	static void setMaterialBuffer(GLuint p_MtlUniformBuffer);

	/**
	 * \brief represents material block like it is in GLSL shader program
	*/
	struct ShaderMaterialBlock {
		/**
		* \brief default constructor, inits all variables of the struct to average values
		*/
		explicit ShaderMaterialBlock();
		glm::vec3 m_DiffuseColor;
		float _padding1;
		glm::vec3 m_AmbientColor;
		float _padding2;
		glm::vec3 m_SpecularColor;
		float m_Shininess;
	};
private:
	ShaderMaterialBlock m_ShaderMtlData;
	GLuint m_DiffuseTexture;
	GLuint m_NormalMapTexture;


	GLuint m_IndexBegin;
	GLuint m_IndArrayVertexBase;
	GLuint m_IndNum;
	static GLuint m_NoTexCap;
	static GLuint m_DiffTexSampler;
	static GLuint m_MtlUniformBuffer;
	static GLuint m_SizeMaterialBlock;
	friend class GLContImporter;
	friend class SimpleStaticMeshGeom;
	friend class StaticMeshGeom;
	friend class SkyboxMeshGeom;
	friend class FXGeom;
};

typedef Material::ShaderMaterialBlock DShaderMaterial;

#endif // !__MATERIALS_H

