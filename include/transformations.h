#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __TRANSFORMATIONS_H
#define __TRANSFORMATIONS_H

#include <stack>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include "misc.h"

/**
 * \brief implements maxtrix pattern, used to make hierarchicaly dependent tranformations.
 * interface of class and idea was partially taken from glutil library header. Own implementation
*/
class MatrixStack
{
public:
	/**
	* \brief Initializes the matrix stack with the identity matrix
	*/
	MatrixStack()
		: m_CurrMatrix(1)
	{}
	/**
	* \brief Initializes the matrix stack with the given matrix
	*/
	explicit MatrixStack(const glm::mat4 &initialMatrix)
		: m_CurrMatrix(initialMatrix)
	{}

	MatrixStack& operator=(const MatrixStack& other) = default;

	void applyMatrix(const glm::mat4 &theMatrix) { m_CurrMatrix *= theMatrix; }

	MatrixStack &operator*=(const glm::mat4 &theMatrix) { applyMatrix(theMatrix); return *this; }
	/**
	* \brief Preserves the current matrix on the stack
	*/
	void push() { m_Stack.push(m_CurrMatrix); }
	/**
	* \brief The given matrix becomes the current matrix
	*/
	void setMatrix(const glm::mat4 &theMatrix) { m_CurrMatrix = theMatrix; }
	/**
	* \brief Sets the current matrix to the identity matrix
	*/
	void setIdentity() { m_CurrMatrix = glm::mat4(1.0f); }
	/**
	* \brief Restores the most recently preserved matrix
	*/
	void pop()
	{
		m_CurrMatrix = m_Stack.top();
		m_Stack.pop();
	}
	/**
	* \brief Retrieve the current matrix.
	*/
	const glm::mat4 &top() const { return m_CurrMatrix; }
	/**
	* \brief Restores the current matrix to the value of the most recently preserved matrix.
	*/
	void reset() { m_CurrMatrix = m_Stack.top(); }
	/**
	* \brief Applies a rotation matrix about the given axis, with the given angle in degrees.
	*/
	void rotate(const glm::vec3 p_Axis, float p_AngRadCCW) { m_CurrMatrix = glm::rotate(m_CurrMatrix, p_AngRadCCW, glm::normalize(p_Axis)); }
	/**
	* \brief Applies a rotation matrix, made of quaternion (Gimbal lock safe)
	*/
	void rotate(const glm::fquat& p_Quat) { m_CurrMatrix *= glm::mat4_cast(p_Quat); }
	/**
	* \brief Applies a rotation matrix about the given axis, with the given angle in radians.
	*/
	void rotateRadians(const glm::vec3 &axis, float angRadCCW);
	/**
	* \brief Applies a rotation matrix about the given axis, with the given angle in Degrees.
	*/
	void rotateDegrees(const glm::vec3 &axis, float p_AngRadCCW) { rotateRadians(axis, degreesToRadians(p_AngRadCCW)); }
	/**
	* \brief Applies 3 rotations, about
	*/
	void rotateRawDegrees(const glm::vec3 &p_DegList);
	/**
	* \brief Applies a rotation matrix about the +X axis, with the given angle in degrees.
	*/
	void rotateX(float angDegCCW) { rotate(glm::vec3(1.0f, 0.0f, 0.0f), angDegCCW); }
	/**
	* \brief Applies a rotation matrix about the +Y axis, with the given angle in degrees.
	*/
	void rotateY(float angDegCCW) { rotate(glm::vec3(0.0f, 1.0f, 0.0f), angDegCCW); }
	/**
	* \brief Applies a rotation matrix about the +Z axis, with the given angle in degrees.
	*/
	void rotateZ(float angDegCCW) { rotate(glm::vec3(0.0f, 0.0f, 1.0f), angDegCCW); }
	/**
	* \brief Applies a scale matrix, with the given glm::vec3 as the axis scales.
	*/
	void scale(const glm::vec3 &scaleVec) { m_CurrMatrix = glm::scale(m_CurrMatrix, scaleVec); }
	/**
	* \brief Applies a scale matrix, with the given values as the axis scales.
	*/
	void scale(float scaleX, float scaleY, float scaleZ) { scale(glm::vec3(scaleX, scaleY, scaleZ)); }
	/**
	* \brief Applies a uniform scale matrix.
	*/
	void scale(float uniformScale) { scale(glm::vec3(uniformScale)); }
	/**
	* \brief Applies a translation matrix, with the given glm::vec3 as the offset.
	*/
	void translate(const glm::vec3 &offsetVec) { m_CurrMatrix = glm::translate(m_CurrMatrix, offsetVec); }
	/**
	* \brief Applies a translation matrix, with the given X, Y and Z values as the offset.
	*/
	void translate(float transX, float transY, float transZ) { translate(glm::vec3(transX, transY, transZ)); }
	/**
	* \brief  Applies a matrix that transforms to a camera-space using GLM's params.
	*/
	void lookAt(const glm::vec3 &cameraPos, const glm::vec3 &lookatPos, const glm::vec3 &upDir)
	{
		m_CurrMatrix *= glm::lookAt(cameraPos, lookatPos, upDir);
	}
	/**
	* \brief  Applies a standard, GLM perspective projection matrix.
	*/
	void perspective(float degFOV, float aspectRatio, float zNear, float zFar)
	{
		m_CurrMatrix *= glm::perspective(degFOV, aspectRatio, zNear, zFar);
	}
	/**
	* \brief Applies a standard, GLM orthographic projection matrix.
	*/
	void orthographic(float left, float right, float bottom, float top, float zNear = -1.0f, float zFar = 1.0f)
	{
		m_CurrMatrix *= glm::ortho(left, right, bottom, top, zNear, zFar);
	}
	class PushAndDestroyOnBlock;
private:
	std::stack<glm::mat4, std::vector<glm::mat4> > m_Stack;
	glm::mat4 m_CurrMatrix;
};



/**
 * \brief Class is used to auto-pop and control garbage collect MatrixStack using blocking with brackets
*/
class MatrixStack::PushAndDestroyOnBlock
{
public:
	/**
	* \brief Pushes the given MatrixStack.
	*/
	PushAndDestroyOnBlock(MatrixStack &stack)
		: m_Stack(stack)
	{
		m_Stack.push();
	}
	/**
	* \brief Pops the MatrixStack that the constructor was given
	*/
	~PushAndDestroyOnBlock()
	{
		m_Stack.pop();
	}
	/**
	* \brief Resets the current matrix of the MatrixStack to the value that was pushed in the constructor
	*/
	void ResetStack()
	{
		m_Stack.reset();
	}

private:
	MatrixStack & m_Stack;

	PushAndDestroyOnBlock(const PushAndDestroyOnBlock &) = delete;
	PushAndDestroyOnBlock &operator=(const PushAndDestroyOnBlock&) = delete;
};

glm::fquat rawDegreesToQuat(const glm::vec3 &p_DegList);

#endif //!__TRANSFORMATIONS_H