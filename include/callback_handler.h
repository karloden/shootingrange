#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#ifndef __CALLBACK_HANDLER_H
#define __CALLBACK_HANDLER_H

#include "game_state.h"
#include "scene_handler.h"
#include "mesh_geometry.h"
#include "camera.h"
#include "player.h"
#include "parameters.h"

/**
 * \brief Main controller of application, you have to call it's methods in order to do anything
*/
class CallbackHandler {
public:
	/**
	 * \brief Constructs instance with nullptrs
	*/
	explicit CallbackHandler();

	/**
	* \brief destructor
	*/
	~CallbackHandler();

	/**
	* \brief initializes all necessary classes instances
	* \returns true if everything initialized correctly, false otherwise
	*/
	bool initialize();

	/**
	 * \brief have to be called when render routine has to be done
	 * 
	*/
	void handleDisplay();

	/**
	* \brief have to be called when window is reshaped by common window manager routine (border drag or full-screen mode)
	* @param p_ResX new resolution of X axis
	* @param p_ResY new resolution of Y axis
	*/
	void handleReshape(int32_t p_ResX, int32_t p_ResY);

	/**
	 * \brief have to be called every frame to update world (scene, player position etc)
	*/
	void handleWorldUpdate();

	/**
	 * \brief destructs all objects, that program used 
	*/
	void handleClose();

	/**
	* \brief handles keyboard key PUSH
	* some keys, that cannot be set in config file are harcoded (reload config file, esc for exit)
	* when command pattern keys are used, uses player instance to execute commands on him, but in place of player there can be any object
	* @param p_Key ASCII code of key
	* @param p_X mouse pointer coordinate X in moment of callback start
	* @param p_Y mouse pointer coordinate Y in moment of callback start
	*/
	void handleInputKeyboardDown(unsigned char p_Key, int32_t p_X, int32_t p_Y);

	/**
	* \brief handles keyboard key RELEASE
	* @param p_Key ASCII code of key
	* @param p_X mouse pointer coordinate X in moment of callback start
	* @param p_Y mouse pointer coordinate Y in moment of callback start
	*/
	void handleInputKeyboardUp(unsigned char p_Key, int32_t p_X, int32_t p_Y);

	/**
	* \brief handles mouse click
	* @param p_Button one of GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, or GLUT_RIGHT_BUTTON
	* @param p_State GLUT_UP or GLUT_DOWN callback was due to a release or press
	* @param p_X mouse pointer coordinate X in moment of callback start 
	* @param p_Y mouse pointer coordinate Y in moment of callback start 
	*/
	void handleMouseClick(int32_t p_Button, int32_t p_State, int32_t p_X, int32_t p_Y);

	/**
	* uses player instance to execute orientation change on him, but in place of player there can be any object
	* @param p_X mouse pointer coordinate X in moment of callback start
	* @param p_Y mouse pointer coordinate Y in moment of callback start
	*/
	void handleMouseMotion(int32_t p_X, int32_t p_Y);

	/**
	* \brief zooms camera by changing it's FOV
	* @param p_Direction direction where mouse wheel goes +/-1
	* @param p_X mouse pointer coordinate X in moment of callback start
	* @param p_Y mouse pointer coordinate Y in moment of callback start
	*/
	void handleMouseWheel(int32_t p_Direction, int32_t p_X, int32_t p_Y);
private:
	/**
	 * \brief translates window space coordinates to world coordinates
	 * @param coordinate X of window
	 * @param coordinate Y of window
	*/
	glm::vec3 windowCoordToWorld(int32_t p_X, int32_t p_Y);

	SceneHandler * m_SceneHandler;
	Camera *m_Camera;
	Player *m_Player;
	Parameters *m_Parameters;
	std::vector<AShaderBase*> m_Shaders;
	const std::map<char, Command*>* m_Keybindings;
};

void callbackTimer(int fps);

#endif //!__CALLBACK_HANDLER_H