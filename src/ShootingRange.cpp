// ShootingRange.cpp : Defines the entry point for the console application.
//

#include "pgr.h"
#include <iostream>
#include "misc.h"
#include "callback_handler.h"
#include "mesh_geometry.h"


const std::string g_WindowName = "ShootingRange v0.3";


CallbackHandler g_Handler;

void callbackTimer(int fps)
{
	glutTimerFunc(16, callbackTimer, 0);
	g_Handler.handleWorldUpdate();
}

void callbackDisplay() { g_Handler.handleDisplay(); }

void callbackClose() { g_Handler.handleClose(); }

void callbackReshape(int32_t resX, int32_t resY) { g_Handler.handleReshape(resX, resY); }

void callbackKeyboardDown(unsigned char key, int x, int y) { g_Handler.handleInputKeyboardDown(key, x, y); }

void callbackKeyboardUp(unsigned char key, int x, int y) { g_Handler.handleInputKeyboardUp(key, x, y); }

void callbackMouseClick(int button, int state, int x, int y) { g_Handler.handleMouseClick(button, state, x, y); }

void callbackMouseMotion(int x, int y) { g_Handler.handleMouseMotion(x, y); }

void callbackMouseWheel(int wheel, int direction, int x, int y) { g_Handler.handleMouseWheel(direction, x, y); }



int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitDisplayMode(GLUT_SRGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);

	glutInitWindowSize(800, 600);
	glutCreateWindow(g_WindowName.c_str());

	glutIgnoreKeyRepeat(1);
	glutDisplayFunc(callbackDisplay);
	glutReshapeFunc(callbackReshape);
	glutTimerFunc(16, callbackTimer, 0);
	glutCloseFunc(callbackClose);
	glutKeyboardFunc(callbackKeyboardDown);
	glutKeyboardUpFunc(callbackKeyboardUp);
	glutMouseFunc(callbackMouseClick);

	//Shooter-like camera must move even if mouse button is down
	glutPassiveMotionFunc(callbackMouseMotion);
	glutMotionFunc(callbackMouseMotion);

	glutMouseWheelFunc(callbackMouseWheel);
	if (!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR, pgr::DEBUG_LOW))
		pgr::dieWithError("Framework init failed. Make sure required OpenGL is supported.");

	if (g_Handler.initialize())
		glutMainLoop();
	else pgr::dieWithError("ShootingRange initialize failed.");


	return 0;
}

