#include "callback_handler.h"
#include "misc.h"
#include "transformations.h"
#include "world_objects.h"
#include "mesh.h"
//todo res from singleton params

CallbackHandler::CallbackHandler()
	: m_SceneHandler(nullptr)
	, m_Camera(nullptr)
	, m_Parameters(nullptr)
	, m_Player(nullptr) {}

CallbackHandler::~CallbackHandler()
{
	if (m_SceneHandler) delete m_SceneHandler;
	if (m_Camera) delete m_Camera;
	if (m_Player) delete m_Player;
	if (m_Parameters) delete m_Parameters;
	for (auto &a : m_Shaders) delete a;
}

bool CallbackHandler::initialize()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f);

	glEnable(GL_DEPTH_CLAMP);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#ifdef _DEBUG
	//glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif //!_DEBUG

	try {
		m_Parameters = new Parameters();
	}
	catch (const ConfigFileFormatException &ex) {
		std::cerr << ex.what()<< std::endl;
		return false;
	}
	m_Keybindings = m_Parameters->getControlMap();
	if (m_Parameters->fullscreen()) glutFullScreen();
	m_Camera = new Camera(m_Parameters->resX(), m_Parameters->resY(), m_Parameters->cameraSens());
	m_Shaders.resize(ShadersNum);
	try {
		m_Shaders[ComplMaterialActor]=new StaticMeshShader();
		m_Shaders[SkyboxActor] = new SkyboxShader();
		m_Shaders[FXActor] = new FXShader();
	}
	catch (const ShaderException &e) {
		std::cerr << e.what() << std::endl;
		return false;
	}

	m_SceneHandler = new SceneHandler(m_Shaders);
	m_Player = new Player(m_SceneHandler, glm::vec3(0.0f, -0.1f, 0.f));
	m_Player->pilot(m_Camera);
	//Scene now take care of deleting player instance
	m_SceneHandler->setPlayerPtr(m_Player);
	CHECK_GL_ERROR();
	if (m_Parameters && m_Camera && m_Player && m_Shaders.size() && m_Player->cameraIsSet())
		return true;
	return false;

}

void CallbackHandler::handleDisplay()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	MatrixStack a;
	a.setMatrix(m_Player->viewFromActor());
	m_SceneHandler->render(a);
	glutSwapBuffers();


}

void CallbackHandler::handleReshape(int32_t p_ResX, int32_t p_ResY)
{
	m_Camera->screenReshape(p_ResX, p_ResY);
}

void CallbackHandler::handleWorldUpdate()
{
	m_Player->accelerate();
	m_Player->applyAcceleration();
	m_SceneHandler->update();
	glutPostRedisplay();

}

void CallbackHandler::handleClose()
{
	delete m_SceneHandler;
	delete m_Camera;
	delete m_Parameters;
	for (auto &a : m_Shaders)
		delete a;
}

void CallbackHandler::handleInputKeyboardDown(unsigned char p_Key, int32_t p_X, int32_t p_Y)
{
	switch (p_Key) {
	case 'p':
		try {
			m_Parameters->readConfig();
			std::cout << "Config file has been reloaded" << std::endl;
			m_Camera->screenReshape(m_Parameters->resX(), m_Parameters->resY());
			m_Camera->changeSensitivity(m_Parameters->cameraSens());
			if (m_Parameters->fullscreen()) glutFullScreen();
			else glutReshapeWindow(m_Parameters->resX(), m_Parameters->resY());
		}
		catch (const EngineException &a) {
			std::cerr << a.what() << std::endl;
		}
		return;
	case '.':
		m_SceneHandler->toggleFog();
		return;
	case 27:
		glutLeaveMainLoop();
		return;
	default:
		auto iter = m_Keybindings->find(p_Key);
		if (iter == m_Keybindings->cend()) return;
		iter->second->execute(m_Player, true);
	}
}

void CallbackHandler::handleInputKeyboardUp(unsigned char p_Key, int32_t p_X, int32_t p_Y)
{
	switch (p_Key) {
	case 27:
		glutLeaveMainLoop();
		return;
	default:
		auto iter = m_Keybindings->find(p_Key);
		if (iter == m_Keybindings->cend()) return;
		iter->second->execute(m_Player, false);
	}
}

void CallbackHandler::handleMouseClick(int32_t p_Button, int32_t p_State, int32_t p_X, int32_t p_Y)
{
	if (p_State == GLUT_UP) return;
	if (p_Button == GLUT_LEFT_BUTTON) {

		glm::vec3 worldPosClick = windowCoordToWorld(p_X, p_Y);

		AActorBase *obj = m_Player->resolvePicking(worldPosClick);
//		if(obj) std::cout << obj->getName() << std::endl;

		if (obj) m_Player->interactWithActiv(static_cast<const Activator*> (obj), worldPosClick);

		std::cout <<worldPosClick.x << " " << worldPosClick.y << " " << worldPosClick.z << std::endl;
	}
}

void CallbackHandler::handleMouseMotion(int32_t p_X, int32_t p_Y)
{

	m_Player->lookAtScreenCoords(p_X, p_Y);

}

void CallbackHandler::handleMouseWheel(int32_t p_Direction, int32_t p_X, int32_t p_Y)
{
	if (p_Direction > 0) m_Camera->changeFieldOfView(-0.1f);
	else if(p_Direction < 0) m_Camera->changeFieldOfView(0.1f);

}

glm::vec3 CallbackHandler::windowCoordToWorld(int32_t p_X, int32_t p_Y)
{
	int32_t windowHeight = glutGet(GLUT_WINDOW_HEIGHT);
	int32_t clickYInverted = windowHeight - p_Y - 1;
	int32_t windowWigth = glutGet(GLUT_WINDOW_WIDTH);
	float windowDepth;
	glReadPixels(p_X, clickYInverted, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &windowDepth);

	glm::vec3 winCoordVec = glm::vec3((float)p_X, (float)clickYInverted, windowDepth);

	return glm::unProject(winCoordVec, m_Player->viewFromActor()
		, m_Camera->getCurrentProjMat(), glm::vec4(0.f, 0.f, (float)windowWigth, (float)windowHeight));

}
