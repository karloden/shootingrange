
#include "camera.h"
#include "transformations.h"
#include "shaders.h"
#include "misc.h"

Camera::Camera(int32_t p_WindowWidth, int32_t p_WindowHeight, float p_MouseSens)
	: m_MouseSens(p_MouseSens * 0.001f)
	, m_Fov(45.0f)
	, m_WindowWidth(p_WindowWidth)
	, m_WindowHeight(p_WindowHeight)

{
	glGenBuffers(1, &m_ProjectionUniformBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_ProjectionUniformBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(ProjectionBlock), NULL, GL_DYNAMIC_DRAW);
	glBindBufferRange(GL_UNIFORM_BUFFER, StaticMeshShader::m_ProjUnifBLCBndIdx, m_ProjectionUniformBuffer,
		0, sizeof(ProjectionBlock));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Camera::screenReshape(int32_t resX, int32_t resY)
{

	m_WindowWidth = resX;
	m_WindowHeight = resY;
	recalculateProjectionBlock();
}

void Camera::changeFieldOfView(float p_Diff)
{
	float initMouseSens = m_MouseSens;
	m_Fov += p_Diff;
	m_MouseSens += p_Diff * 0.0045f;
	if (m_Fov > 45.5f) {
		m_Fov = 45.5f;
		m_MouseSens = initMouseSens;
	}
	if (m_Fov < 44.5f) {
		m_Fov = 44.5f;
		m_MouseSens = initMouseSens;
	}
	recalculateProjectionBlock();
}

glm::fquat Camera::rotateCamera(const glm::fquat &p_OldOrientation, int32_t p_NewScreenX, int32_t p_NewScreenY) const
{
	int32_t initPosX = m_WindowWidth / 2, initPosY = m_WindowHeight / 2;
	if (p_NewScreenX == initPosX && p_NewScreenY == initPosY) return p_OldOrientation;
	cursorToWinPosition(initPosX, initPosY);
	int32_t diffX = p_NewScreenX - initPosX, diffY = p_NewScreenY - initPosY;
	float degXDiff = diffX * m_MouseSens, degYDiff = diffY * m_MouseSens;
	glm::fquat tempOrientation = p_OldOrientation * glm::angleAxis(degXDiff, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::fquat pitchTempOrientation = glm::angleAxis(degYDiff, glm::vec3(1.0f, 0.0f, 0.0f)) * tempOrientation;
	float pitch = glm::roll(glm::fquat(pitchTempOrientation.w, pitchTempOrientation.y, pitchTempOrientation.z, pitchTempOrientation.x));
	if ((pitch > 1.52f && diffY > 0) || (pitch < -1.52f && diffY < 0))
	{
		return tempOrientation;
	}
	else {
		return pitchTempOrientation;
	}
}


glm::mat4 Camera::calcViewMatrix(const glm::vec3 & p_Position, const glm::fquat & p_Orientation, float p_Lean)
{
	MatrixStack stack;
	stack.translate(glm::vec3(0.0f, 0.0f, 1.f));
	stack.rotateRadians(glm::vec3(0.0f, 0.0f, 1.0f), p_Lean);
	stack.rotate(p_Orientation);
	stack.translate(-p_Position);

	return stack.top();
}

void Camera::recalculateProjectionBlock()
{
	MatrixStack persMatrix;

	persMatrix.perspective(m_Fov, (float)m_WindowWidth / (float)m_WindowHeight, 0.01f, 1000.0f);

	m_Proj.Pmatrix = persMatrix.top();
	glBindBuffer(GL_UNIFORM_BUFFER, m_ProjectionUniformBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(ProjectionBlock), &m_Proj);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glViewport(0, 0, (GLsizei)m_WindowWidth, (GLsizei)m_WindowHeight);

}
