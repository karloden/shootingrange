#include "game_state.h"
#include "exceptions.h"

Timer::Timer(Type p_Type, float p_Duration)
	: m_Type(p_Type)
	, m_SecDuration(p_Duration)
	, m_HasUpdated(false)
	, m_Paused(false)
	, m_AbsPrevTime(0.0f)
	, m_SecAccumTime(0.0f)
{
	if (m_Type == Infinite && m_SecDuration != 1.0f)
		throw ObjectConstructionException("Timer type if infinite.", "So duration cannot be set");

}

void Timer::reset()
{
	m_HasUpdated = false;
	m_SecAccumTime = 0.0f;
}



bool Timer::update()
{
	float absCurrTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	if (!m_HasUpdated)
	{
		m_AbsPrevTime = absCurrTime;
		m_HasUpdated = true;
	}

	if (m_Paused)
	{
		m_AbsPrevTime = absCurrTime;
		return false;
	}

	float fDeltaTime = absCurrTime - m_AbsPrevTime;
	m_SecAccumTime += fDeltaTime;

	m_AbsPrevTime = absCurrTime;
	if (m_Type == Single)
		return m_SecAccumTime > m_SecDuration;

	return false;
}

float Timer::getAlpha() const
{
	switch (m_Type)
	{
	case Loop:
		return fmodf(m_SecAccumTime, m_SecDuration) / m_SecDuration;
	case Single:
		return glm::clamp(m_SecAccumTime / m_SecDuration, 0.0f, 1.0f);
	default:
		return 1.f;
	}
}

float Timer::getProgression() const
{
	switch (m_Type)
	{
	case Loop:
		return fmodf(m_SecAccumTime, m_SecDuration);
	case Single:
		return glm::clamp(m_SecAccumTime, 0.0f, m_SecDuration);
	default:
		return m_SecDuration;
	}
}

float Timer::getTimeSinceStart() const
{
	return m_SecAccumTime;
}

Activator::Activator(ActivatorType p_Type
	, const std::string & p_FileName
	, const glm::vec3 & p_InitWorldPos
	, const glm::vec3 & p_InitScale
	, const glm::vec3 & p_InitRawDegrees
	, const glm::vec3& p_Center
	, int32_t p_MeshInd)
	: StaticMesh(p_FileName, p_InitWorldPos, p_InitScale, p_InitRawDegrees, p_MeshInd)
	, m_Type(p_Type)
	, m_Center(p_Center)
{
}

int32_t Activator::getPoints(const glm::vec3 & p_WorldPoint) const
{
	float radius = getDistance(p_WorldPoint);
	if (radius < 0.6f) return 10;
	if (radius < 1.2f) return 5;
	if (radius < 1.8f) return 2;
	return 0;
}

float Activator::getDistance(const glm::vec3 & p_WorldPoint) const
{
	return glm::distance(m_Center, p_WorldPoint);
}

Activator::Activator(const Activator & p_Other)
	:StaticMesh(p_Other.m_Filename, glm::vec3(0.f), glm::vec3(1.f), glm::vec3(0.f), p_Other.m_MaterialInd)
{
}

void Activator::renderPickableInHands(MatrixStack & p_ViewMatrix) const
{
	if (!isEnabled() || !isLoaded()) return;
	m_Mesh->render(p_ViewMatrix);
}
