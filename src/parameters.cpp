#include <utility>

#include "parameters.h"
#include "exceptions.h"


void Parameters::fillMap(const std::string &paramPath, std::map<std::string, std::vector<std::string>> &parameters)
{
	std::ifstream paramStream;
	checkAndRecoverConfig(paramStream, paramPath);
	std::string line, kind = "";
	bool gotAnything = false;
	bool strict = parameters.empty();
	while (getline(paramStream, line)) {
		if (line[0] == '#') continue;
		if (line[0] == '[' && line.find(']') != std::string::npos) {
			kind = line;
			continue;
		}
		if (kind == "")
			throw ConfigFileFormatException(paramPath, "Each category of config file must start as ['Category']");
		parameters[kind].push_back(line);
		gotAnything = true;
	}
	if (!gotAnything && strict) throw ConfigFileFormatException(paramPath);
	paramStream.close();
}

Parameters::Parameters() : m_AllInit(false), m_LoadedGame(false)
{
	readConfig();
}

Parameters::~Parameters()
{
	for (auto &a : m_ControlsMap) delete a.second;
}

void Parameters::readConfig()
{
	std::map<std::string, std::vector<std::string>> parameters;
	fillMap(g_ConfigDefault, parameters);
	fillMap(g_Config, parameters);
	const std::string graphics = "[Graphics]";
	const std::string game = "[GameLogic]";
	const std::string bindings = "[Bindings]";

	auto iterGr = parameters.find(graphics);
	if (iterGr != parameters.end()) {
		parseGraphics(iterGr->second);
		parameters.erase(iterGr);
	}
	else throw ConfigFileFormatException("Config", graphics + " category not found");

	iterGr = parameters.find(bindings);
	if (iterGr != parameters.end()) {
		parseBindings(iterGr->second);
		parameters.erase(iterGr);
	}
	else throw ConfigFileFormatException("Config", bindings + " category not found");

	/*auto iterGame = parameters.find(game);
	if (iterGame != parameters.end()) {
		parseGame(iterGame->second);
		parameters.erase(iterGame);
	} else throw ConfigFileFormatException("Config", game + " category not found");*/

	m_AllInit = true;
}


void Parameters::checkAndRecoverConfig(std::ifstream &paramStream, const std::string &paramPath) const
{
	paramStream.open(paramPath, std::ios::in);
	if (!paramStream.is_open() || !paramStream.good() || paramStream.fail()) {
		paramStream.close();
		if (g_ConfigDefault == paramPath) throw FileException("with default config");
		std::ifstream src(g_ConfigDefault, std::ios::binary);
		std::ofstream dst(paramPath, std::ios::binary);
		dst << src.rdbuf();
		src.close();
		dst.close();
		paramStream.open(paramPath, std::ios::in);
		if (!paramStream.is_open() || !paramStream.good() || paramStream.fail())
			throw FileException(paramPath);
	}
}


void Parameters::parseGame(const std::vector<std::string> &parameters)
{

}


void Parameters::parseGraphics(const std::vector<std::string> &parameters)
{
	const std::string resX = "ResX";
	const std::string resY = "ResY";
	const std::string screen = "FullScreen";
	const std::string cameraSens = "CameraSensitivity";
	for (const auto &consideredStr : parameters) {
		try {
			if (!consideredStr.find(resX)) {
				int32_t resXtmp = std::stoi(consideredStr.substr(resX.size() + 1, consideredStr.size()));
				if (resXtmp < 800)
					throw ConfigFileFormatException(
						"Config", consideredStr +
						"\nResolution recommended 800x600 or 1024x768, cannot be less than 800x600");
				m_ResX = (uint32_t)resXtmp;
				continue;
			}
			else if (!consideredStr.find(resY)) {
				int32_t resYtmp = std::stoi(consideredStr.substr(resY.size() + 1, consideredStr.size()));
				if (resYtmp < 600)
					throw ConfigFileFormatException(
						"Config", consideredStr +
						"\nResolution recommended 800x600 or 1024x768, cannot be less than 800x600");
				m_ResY = (uint32_t)resYtmp;
				continue;
			}
			else if (!consideredStr.find(screen)) {
				m_Fullscreen = (bool)std::stoi(consideredStr.substr(screen.size() + 1, consideredStr.size()));
				if (m_Fullscreen != 0 && m_Fullscreen != 1)
					throw ConfigFileFormatException(
						"Config", consideredStr + "\nFullscreen must be 0 or 1 (off or on)");
				continue;
			}
			else if (!consideredStr.find(cameraSens)) {
				float sensTmp = std::stof(consideredStr.substr(cameraSens.size() + 1, consideredStr.size()));
				if (sensTmp <= 0 || sensTmp > 100) throw ConfigFileFormatException("Config", consideredStr);
				m_Sens = sensTmp;
				continue;
			}
			throw ConfigFileFormatException("Config", consideredStr);
		}
		catch (...) {
			throw ConfigFileFormatException("Config", consideredStr);
		}
	}
}

void Parameters::parseBindings(const std::vector<std::string>& parameters)
{

	const std::map <std::string, char>  specialKeyMaps
	{
		std::make_pair("space", 32),
		std::make_pair("tab", 9),
		std::make_pair("delete", 127),
		std::make_pair("backspace", 8)
	};

	const std::vector<std::string> commandNames =
	{
		"Forward", "Backward", "StrafeLeft", "StrafeRight", "Jump", "LeanLeft", "LeanRight", "Flashlight", "Fly", "ToggleView"
	};
	std::vector<Command*> commandList =
	{
		new MoveForward(), new MoveBackward(), new MoveStrafeLeft(), new MoveStrafeRight(),
		new MoveJump() , new PlayerLeanLeft(), new PlayerLeanRight(), new PlayerToggleFlashlight(), new PlayerToggleFly(), new MoveToggleView()

	};
	std::map<char, Command*> controlsMap;


	for (const auto &consideredStr : parameters) {
		bool found = false;
		for (uint32_t i = 0; i < commandList.size(); i++) {
			if (!consideredStr.find(commandNames[i])) {
				std::string symbol = consideredStr.substr(commandNames[i].size() + 1, consideredStr.size());
				if (symbol.length() != 1) {
					symbol = specialKeyStringToASCII(symbol, specialKeyMaps);
					if (!symbol[0])
						throw ConfigFileFormatException("Config", consideredStr);
				}

				for (auto &alreadyExists : controlsMap) {
					if (typeid(*alreadyExists.second) == typeid(*commandList[i]))
					{
						auto iter = controlsMap.find(alreadyExists.first);
						controlsMap.erase(iter);
						break;
					}
				}

				auto iter = controlsMap.find(symbol[0]);
				if (iter != controlsMap.end()) controlsMap.erase(iter);
				controlsMap.insert(std::pair<char, Command*>(symbol[0], commandList[i]));
				found = true;
				break;
			}
			else continue;
		}
		if (found) continue;
		throw ConfigFileFormatException("Config", consideredStr);
	}
	for (auto &newlyAdded : controlsMap)
	{
		for (auto &alreadyExists : m_ControlsMap)
		{
			if (typeid(*alreadyExists.second) == typeid(*newlyAdded.second) || alreadyExists.first == newlyAdded.first)
			{
				delete alreadyExists.second;
				auto found = m_ControlsMap.find(alreadyExists.first);
				m_ControlsMap.erase(found);
				break;
			}
		}
		m_ControlsMap.insert(newlyAdded);
	}
}


char Parameters::specialKeyStringToASCII(const std::string &p_LookingFor, const std::map<std::string, char>& p_SpecialKeyMaps) const
{
	auto found = p_SpecialKeyMaps.find(p_LookingFor);
	if (found != p_SpecialKeyMaps.end()) return found->second;
	else return 0;
}