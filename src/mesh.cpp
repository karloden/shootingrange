#include "mesh.h"

extern const std::string g_PathToBinary;


MeshFlyweightLoader *AActorBase::m_Loader;

StaticMesh::StaticMesh(const std::string &p_FileName
	, const glm::vec3& p_InitWorldPos
	, const glm::vec3& p_InitScale
	, const glm::vec3& p_InitRawDegrees
	, int32_t p_MeshInd)
	: AActorBase(p_FileName, p_InitWorldPos, p_InitScale, p_InitRawDegrees, false)
	, m_MaterialInd(p_MeshInd)
	, m_Collision(nullptr) 
{
	load();
}

void StaticMesh::reset()
{
	free();
	load();
}

void StaticMesh::load()
{

	if (m_MaterialInd == -1)
		m_Mesh = m_Loader->getMeshGeomPtrOrLoad(m_Filename, ComplMaterialActor, -1);
	else
		m_Mesh = m_Loader->getMeshGeomPtrOrLoad(m_Filename, SimpleMatertialActor, m_MaterialInd);

	if (m_Mesh) {
		m_Loaded = true;
		if (m_Collision) delete m_Collision;
		m_Collision = nullptr;
		m_Collision = m_Loader->getNewConnectedCollision(m_Filename, m_MaterialInd);
		m_Collision->applyTransform(m_InitTransform);
	}

}

void StaticMesh::free()
{
	if (m_Loaded && m_Mesh->unload()) {
		m_Loader->freeMesh(m_Filename, m_MaterialInd);
		m_Mesh = nullptr;
	}
	if (m_Collision) delete m_Collision;
	m_Collision = nullptr;
	m_Loaded = false;
}

void StaticMesh::render(MatrixStack & p_ViewMatrix) const
{
	if (!isEnabled() || !isLoaded()) return;
	MatrixStack::PushAndDestroyOnBlock p(p_ViewMatrix);

	p_ViewMatrix *= m_InitTransform;

	m_Mesh->render(p_ViewMatrix);
}

void StaticMesh::addCollTrans(const glm::mat4 & p_Trans)
{
	m_Collision->reset();
	m_Collision->applyTransform(m_InitTransform * p_Trans);
}

void MovableMesh::render(MatrixStack & p_ViewMatrix) const
{
	if (!isEnabled() || !isLoaded()) return;
	MatrixStack::PushAndDestroyOnBlock p(p_ViewMatrix);
	p_ViewMatrix *= m_InitTransform;
	p_ViewMatrix.translate(m_WorldPos);
	p_ViewMatrix.scale(m_Scale);
	p_ViewMatrix.rotate(m_Orientation);

	m_Mesh->render(p_ViewMatrix);
}




MovableMesh::MovableMesh(const std::string & p_FileName
	, const glm::vec3& p_InitWorldPos
	, const glm::vec3& p_InitScale
	, const glm::vec3& p_InitRawDegrees
	, int32_t p_MeshInd)
	: StaticMesh(p_FileName, p_InitWorldPos, p_InitScale, p_InitRawDegrees, p_MeshInd)
	, m_Acceleration(0.0f)
	, m_MaxSpeed(0.6f)
	, m_AccelAttenuation(g_BaseFrictForceCoef)
	, m_ActionStateMachine(Idle)
	, m_Scale(1.f)
	, m_WorldPos(0.f)
	, m_Orientation(1.f, 0.f, 0.f, 0.f)
	, m_CameraPiloting(nullptr)
{}


void MovableMesh::rotate(const glm::vec3 & p_AxisOfRotation, float p_AngRadCCW)
{
	float angRadCCW = degreesToRadians(p_AngRadCCW);
	glm::vec3 axis = glm::normalize(p_AxisOfRotation);

	axis = axis * sinf(angRadCCW / 2.0f);
	float scalar = cosf(angRadCCW / 2.0f);

	m_Orientation *= glm::fquat(scalar, axis.x, axis.y, axis.z);
}


void MovableMesh::accelerate()
{
	glm::vec3 directionAndForce(0.0f);
	if (m_ActionStateMachine & MovingForward)		directionAndForce += glm::vec3(0.0f, 0.0f, -0.5f);
	if (m_ActionStateMachine & MovingBackward)		directionAndForce += glm::vec3(0.0f, 0.0f, 0.5f);
	if (m_ActionStateMachine & MovingStrafeLeft)	directionAndForce += glm::vec3(-0.5f, 0.0f, 0.0f);
	if (m_ActionStateMachine & MovingStrafeRight)	directionAndForce += glm::vec3(0.5f, 0.0f, 0.0f);

	glm::mat4 currMat = Camera::calcViewMatrix(m_WorldPos, m_Orientation, 0.0f);
	glm::fquat orientation = glm::quat_cast(currMat);

	glm::fquat invOrient;
	invOrient = glm::conjugate(orientation);

	glm::vec3 worldOffset = invOrient * directionAndForce;

	if (m_Acceleration.x + worldOffset.x > m_MaxSpeed)
	{
		m_Acceleration.x = m_MaxSpeed;
		m_Acceleration.y = worldOffset.y;
		m_Acceleration.z = worldOffset.z;
		return;
	}
	if (m_Acceleration.y + worldOffset.y > m_MaxSpeed)
	{
		m_Acceleration.x = worldOffset.x;
		m_Acceleration.y = m_MaxSpeed;
		m_Acceleration.z = worldOffset.z;
		return;
	}
	if (m_Acceleration.z + worldOffset.z > m_MaxSpeed)
	{
		m_Acceleration.x = worldOffset.x;
		m_Acceleration.y = worldOffset.y;
		m_Acceleration.z = m_MaxSpeed;
		return;
	}
	if (m_Acceleration.x + worldOffset.x < -m_MaxSpeed)
	{
		m_Acceleration.x = -m_MaxSpeed;
		m_Acceleration.y = worldOffset.y;
		m_Acceleration.z = worldOffset.z;
		return;
	}
	if (m_Acceleration.y + worldOffset.y < -m_MaxSpeed)
	{
		m_Acceleration.x = worldOffset.x;
		m_Acceleration.y = -m_MaxSpeed;
		m_Acceleration.z = worldOffset.z;
		return;
	}
	if (m_Acceleration.z + worldOffset.z < -m_MaxSpeed)
	{
		m_Acceleration.x = worldOffset.x;
		m_Acceleration.y = worldOffset.y;
		m_Acceleration.z = -m_MaxSpeed;
		return;
	}
	m_Acceleration += worldOffset;
}

void MovableMesh::applyAcceleration()
{

	m_WorldPos += m_Acceleration;


	if (m_Acceleration.x < 0.10f && m_Acceleration.x > -0.10f) m_Acceleration.x = 0.f;
	else
		if (m_Acceleration.x > 0.f)
			m_Acceleration.x -= m_AccelAttenuation; else m_Acceleration.x += m_AccelAttenuation;

	if (m_Acceleration.y < 0.10f && m_Acceleration.y > -0.10f)  m_Acceleration.y = 0.f;
	else
		if (m_Acceleration.y > 0.f)
			m_Acceleration.y -= m_AccelAttenuation; else m_Acceleration.y += m_AccelAttenuation;


	if (m_Acceleration.z < 0.10f && m_Acceleration.z > -0.10f) m_Acceleration.z = 0.f;
	else
		if (m_Acceleration.z > 0.f)
			m_Acceleration.z -= m_AccelAttenuation; else m_Acceleration.z += m_AccelAttenuation;
}

void MovableMesh::newAction(Action p_Action, bool p_Start)
{
	switch (m_ActionStateMachine) {
	case Idle:
		if (p_Start) m_ActionStateMachine = p_Action;
		break;
	case MovingForward:
		if (p_Start && (p_Action == MovingStrafeLeft || p_Action == MovingStrafeRight))
			m_ActionStateMachine = (Action)(m_ActionStateMachine | p_Action);
		if (!p_Start && p_Action == MovingForward) m_ActionStateMachine = Idle;
		break;
	case MovingBackward:
		if (p_Start && (p_Action == MovingStrafeLeft || p_Action == MovingStrafeRight))
			m_ActionStateMachine = (Action)(m_ActionStateMachine | p_Action);
		if (!p_Start && p_Action == MovingBackward) m_ActionStateMachine = Idle;
		break;
	case MovingStrafeLeft:
		if (p_Start && (p_Action == MovingForward || p_Action == MovingBackward))
			m_ActionStateMachine = (Action)(m_ActionStateMachine | p_Action);
		if (!p_Start && p_Action == MovingStrafeLeft) m_ActionStateMachine = Idle;
		break;
	case MovingStrafeRight:
		if (p_Start && (p_Action == MovingForward || p_Action == MovingBackward))
			m_ActionStateMachine = (Action)(m_ActionStateMachine | p_Action);
		if (p_Action == MovingStrafeRight) m_ActionStateMachine = Idle;
		break;
	case MovingForward | MovingStrafeLeft:
		if (!p_Start && (p_Action == MovingStrafeLeft)) m_ActionStateMachine = MovingForward;
		if (!p_Start && (p_Action == MovingForward)) m_ActionStateMachine = MovingStrafeLeft;
		break;
	case MovingForward | MovingStrafeRight:
		if (!p_Start && (p_Action == MovingStrafeRight)) m_ActionStateMachine = MovingForward;
		if (!p_Start && (p_Action == MovingForward)) m_ActionStateMachine = MovingStrafeRight;
		break;
	case MovingBackward | MovingStrafeLeft:
		if (!p_Start && (p_Action == MovingStrafeLeft)) m_ActionStateMachine = MovingBackward;
		if (!p_Start && (p_Action == MovingBackward)) m_ActionStateMachine = MovingStrafeLeft;
		break;
	case MovingBackward | MovingStrafeRight:
		if (!p_Start && (p_Action == MovingStrafeRight)) m_ActionStateMachine = MovingBackward;
		if (!p_Start && (p_Action == MovingBackward)) m_ActionStateMachine = MovingStrafeRight;
		break;
	default:
		break;
	}
}


HierarchicalActor::HierarchicalActor(const std::string & p_FileName
	, const glm::vec3& p_InitWorldPos
	, const glm::vec3& p_InitScale
	, const glm::vec3& p_InitRawDegrees
	, int32_t p_MeshInd)
	: StaticMesh(p_FileName, p_InitWorldPos, p_InitScale, p_InitRawDegrees, p_MeshInd)
{

}