#include "transformations.h"
#include <glm/gtc/matrix_transform.hpp>


void MatrixStack::rotateRadians(const glm::vec3& p_AxisOfRotation, float p_AngRadCCW)
{
	glm::vec3 axis = glm::normalize(p_AxisOfRotation);

	axis = axis * sinf(p_AngRadCCW / 2.0f);
	float scalar = cosf(p_AngRadCCW / 2.0f);


	rotate(glm::fquat(scalar, axis.x, axis.y, axis.z));

}

void MatrixStack::rotateRawDegrees(const glm::vec3 & p_DegList)
{
	rotateDegrees(glm::vec3(1.0f, 0.0f, 0.0f), p_DegList.x);
	rotateDegrees(glm::vec3(0.0f, 1.0f, 0.0f), p_DegList.y);
	rotateDegrees(glm::vec3(0.0f, 0.0f, 1.0f), p_DegList.z);
}

glm::fquat rawDegreesToQuat(const glm::vec3 & p_DegList)
{
	glm::vec3 axis(1.0f, 0.0f, 0.0f);
	float radAngle = degreesToRadians(p_DegList.x);
	axis = axis * sinf(radAngle / 2.0f);
	float scalar = cosf(radAngle / 2.0f);
	glm::fquat retVal(scalar, axis.x, axis.y, axis.z);
	retVal = glm::normalize(retVal);
	
	axis = glm::vec3(0.0f, 1.0f, 0.0f);
	radAngle = degreesToRadians(p_DegList.y);
	axis = axis * sinf(radAngle / 2.0f);
	scalar = cosf(radAngle / 2.0f);
	retVal *= glm::fquat(scalar, axis.x, axis.y, axis.z);
	retVal = glm::normalize(retVal);

	axis = glm::vec3(0.0f, 0.0f, 1.0f);
	radAngle = degreesToRadians(p_DegList.z);
	axis = axis * sinf(radAngle / 2.0f);
	scalar = cosf(radAngle / 2.0f);
	retVal *= glm::fquat(scalar, axis.x, axis.y, axis.z);

	retVal = glm::normalize(retVal);
	return retVal;
}
