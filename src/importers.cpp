
#include <vector>
#include <limits>
#include "pgr.h"
#include "importers.h"

#include "misc.h"
#include "parameters.h"

#undef max
#undef min


void GLContImporter::setMeshesMetadata(int32_t p_MeshIdx)
{
	if (m_MeshIdx == p_MeshIdx) return;
	uint32_t from = p_MeshIdx == -1 ? 0 : p_MeshIdx;
	uint32_t to = p_MeshIdx == -1 ? m_Meshes->mNumMeshes : p_MeshIdx + 1;
	uint32_t indexBegin = 0;
	uint32_t indArrayVertexBase = 0;
	for (uint32_t m = from; m < to; ++m) {
		Material *material = &m_Materials[m];
		const auto *mesh = m_Meshes->mMeshes[m];
		material->m_IndexBegin = indexBegin;
		material->m_IndNum = mesh->mNumFaces * 3;
		material->m_IndArrayVertexBase = indArrayVertexBase;

		indexBegin += mesh->mNumFaces * 3;
		indArrayVertexBase += mesh->mNumVertices;
	}
	m_IndecesNum = indexBegin;
	m_VerticesNum = indArrayVertexBase;
	if (p_MeshIdx == -1) {
		m_HasBitangents = m_HasTangents = m_HasColors = m_HasTexCoords = m_HasNormals = false;
		for (uint32_t m = 0; m < m_Meshes->mNumMeshes; ++m) {
			if (!m_HasNormals && m_Meshes->mMeshes[m]->mNormals) { m_HasNormals = true; }
			if (!m_HasTexCoords && m_Meshes->mMeshes[m]->mTextureCoords) { m_HasTexCoords = true; }
			if (!m_HasColors && m_Meshes->mMeshes[m]->mColors) { m_HasColors = true; }
			if (!m_HasTangents && m_Meshes->mMeshes[m]->mTangents) { m_HasTangents = true; }
			if (!m_HasBitangents && m_Meshes->mMeshes[m]->mBitangents) { m_HasBitangents = true; }

			if (m_HasTexCoords && m_HasNormals && m_HasColors && m_HasTangents && m_HasBitangents) break;
		}
	}
	else {
		m_HasNormals = m_Meshes->mMeshes[p_MeshIdx]->mNormals ? true : false;
		m_HasTexCoords = m_Meshes->mMeshes[p_MeshIdx]->mTextureCoords ? true : false;
		m_HasColors = m_Meshes->mMeshes[p_MeshIdx]->mColors ? true : false;
		m_HasTangents = m_Meshes->mMeshes[p_MeshIdx]->mTangents ? true : false;
		m_HasBitangents = m_Meshes->mMeshes[p_MeshIdx]->mBitangents ? true : false;
	}
	m_MeshIdx = p_MeshIdx;
	if (!m_VerticesNum || !m_IndecesNum) throw LogicException("Mesh " + m_ModelPath + " doesnt have any vertices or faces.", " Load failed");

}



GLuint GLContImporter::getBufferCommonTopology(const BufferTypeCommonTopology & p_Type)
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, 3 * m_VerticesNum * sizeof(float), 0,
		m_Flags & ChangeableVRAM ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	uint32_t from = m_MeshIdx == -1 ? 0 : m_MeshIdx;
	uint32_t to = m_MeshIdx == -1 ? m_Meshes->mNumMeshes : m_MeshIdx + 1;
	for (uint32_t m = from, offset = 0; m < to; ++m) {
		uint32_t size = m_Meshes->mMeshes[m]->mNumVertices * 3 * sizeof(float);
		switch (p_Type) {
		case VertexBuffer:
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, m_Meshes->mMeshes[m]->mVertices);
			break;
		case NormalBuffer:
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, m_Meshes->mMeshes[m]->mNormals);
			break;
		case ColorBuffer:
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, m_Meshes->mMeshes[m]->mColors);
			break;
		case TangentBuffer:
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, m_Meshes->mMeshes[m]->mTangents);
			break;
		case BitangentBuffer:
			glBufferSubData(GL_ARRAY_BUFFER, offset, size, m_Meshes->mMeshes[m]->mBitangents);
			break;
		}
		offset += size;
	}
	return vbo;
}


GLContImporter::GLContImporter(const std::string & p_FileName, const MeshFlags & p_Flags)
	: GLContImporterBase(p_FileName, p_Flags)
{
	m_Importer = new OBJImporter();
	m_Importer->SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);
	m_Meshes = m_Importer->ReadFile(m_ModelPath.c_str(), 0x108014b);
	if (!m_Meshes) throw ModuleException(std::string("Importer"), std::string("loading scene from ") + m_ModelPath +
		"\n" + std::string(m_Importer->GetErrorString()));
	m_Materials.resize(m_Meshes->mNumMeshes);
	setMeshesMetadata(-1);
}


GLContImporterBase::GLContImporterBase(const std::string & p_FileName, const MeshFlags & p_Flags)
	: MeshImporterBase(findFileOrThrow(g_PropsPath + p_FileName), p_Flags)
{}




bool MeshImporterBase::setMeshIdxInScene(int32_t p_MeshIdx)
{
	if ((p_MeshIdx < -1) || (p_MeshIdx >= 0 && (uint32_t)p_MeshIdx > this->getMeshNum())) return false;
	setMeshesMetadata(p_MeshIdx);
	return true;
}

std::vector<Material> MeshImporterBase::getMaterials()
{
	if (m_ReturnedAllMtls) throw LogicException("Materials retrieving", "Materials already been moved. Consider load materials again.");
	m_ReturnedAllMtls = true;
	return std::move(m_Materials);
}

GLContObjImporter::GLContObjImporter(const std::string & p_FileName, const MeshFlags & p_Flags)
	: GLContImporterBase(p_FileName, p_Flags)
	, m_Meshes(nullptr)
{
	const std::string filename = findFileOrThrow(p_FileName);
	m_File.open(p_FileName.c_str(), std::ios::ate | std::ios::binary);
	m_Filesize = (size_t)m_File.tellg();
	m_File.seekg(0);
}

GLContObjImporter::~GLContObjImporter()
{
	if (m_Meshes) delete[] m_Meshes;
	m_File.close();
}

GLuint GLContObjImporter::getTexCoordBuffer()
{
	return 0;
}

GLuint GLContObjImporter::getElementBuffer()
{
	return 0;
}

ACollidableBase * GLContObjImporter::getCollision(ACollidableBase::CollisionType p_Type)
{
	return nullptr;
}

void GLContObjImporter::setMeshesMetadata(int32_t m_MeshIdx)
{
}

GLuint GLContObjImporter::getBufferCommonTopology(const BufferTypeCommonTopology & p_Type)
{
	return 0;
}
GLuint GLContImporter::getTexCoordBuffer()
{
	GLuint textureCoordBO;
	std::vector<float> textureCoords(2 * m_VerticesNum);
	float *currTexCoordPointer = &textureCoords[0];

	uint32_t indexBegin = 0;
	uint32_t indArrayVertexBase = 0;

	uint32_t from = m_MeshIdx == -1 ? 0 : m_MeshIdx;
	uint32_t to = m_MeshIdx == -1 ? m_Meshes->mNumMeshes : m_MeshIdx + 1;

	for (uint32_t m = from; m < to; ++m) {
		const auto *mesh = m_Meshes->mMeshes[m];
		if (mesh->HasTextureCoords(0)) {
			for (unsigned idx = 0; idx < mesh->mNumVertices; idx++) {
				auto vect = (mesh->mTextureCoords[0])[idx];
				*currTexCoordPointer++ = vect.x;
				*currTexCoordPointer++ = vect.y;
			}
		}
		indexBegin += mesh->mNumFaces * 3;
		indArrayVertexBase += mesh->mNumVertices;
	}
	glGenBuffers(1, &textureCoordBO);
	glBindBuffer(GL_ARRAY_BUFFER, textureCoordBO);
	// Texture coords might not been changed in VRAM
	glBufferData(GL_ARRAY_BUFFER, m_VerticesNum * 2 * sizeof(float), &textureCoords[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return textureCoordBO;
}

GLuint GLContImporter::getElementBuffer()
{
	GLuint elementBuffer;
	glGenBuffers(1, &elementBuffer);
	std::vector<uint32_t> indices(m_IndecesNum);
	uint32_t from = m_MeshIdx == -1 ? 0 : m_MeshIdx;
	uint32_t to = m_MeshIdx == -1 ? m_Meshes->mNumMeshes : m_MeshIdx + 1;
	uint32_t startIndex = 0;
	for (uint32_t m = from; m < to; ++m) {
		const auto *mesh = m_Meshes->mMeshes[m];
		for (unsigned int f = 0; f < mesh->mNumFaces; ++f) {
			memcpy(&indices[startIndex + f * 3], mesh->mFaces[f].mIndices, 3 * sizeof(uint32_t));
		}
		startIndex += mesh->mNumFaces * 3;
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * m_IndecesNum, &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	return elementBuffer;
}

ACollidableBase* GLContImporter::getCollision(ACollidableBase::CollisionType p_Type)
{
	switch (p_Type) {
	case ACollidableBase::BoxCollision:
	{
		float maxZ, minZ, maxY, minY, maxX, minX;
		maxZ = maxY = maxX = -std::numeric_limits<float>::max();
		minZ = minY = minX = std::numeric_limits<float>::max();
		uint32_t from = m_MeshIdx == -1 ? 0 : m_MeshIdx;
		uint32_t to = m_MeshIdx == -1 ? m_Meshes->mNumMeshes : m_MeshIdx + 1;
		for (uint32_t m = from, offset = 0; m < to; ++m) {
			const auto *mesh = m_Meshes->mMeshes[m];
			for (uint32_t vert = 0; vert < m_Meshes->mMeshes[m]->mNumVertices; ++vert) {
				if (mesh->mVertices[vert].x > maxX) maxX = mesh->mVertices[vert].x;
				if (mesh->mVertices[vert].x < minX) minX = mesh->mVertices[vert].x;
				if (mesh->mVertices[vert].y > maxY) maxY = mesh->mVertices[vert].y;
				if (mesh->mVertices[vert].y < minY) minY = mesh->mVertices[vert].y;
				if (mesh->mVertices[vert].z > maxZ) maxZ = mesh->mVertices[vert].z;
				if (mesh->mVertices[vert].z < minZ) minZ = mesh->mVertices[vert].z;
			}
		}
		return new CollidableBox(maxX, minX, maxY, minY, maxZ, minZ);
	}
	case ACollidableBase::SphereCollision:
		break;
	}
	return nullptr;
}
