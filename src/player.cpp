#include "player.h"

Player::Player(SceneHandler *p_Scene, const glm::vec3 & p_StartPos)
	: MovableMesh("", p_StartPos, glm::vec3(1.0f), glm::vec3(0.0f))
	, m_Flying(false)
	, m_Blocked(false)
	, m_FlashLightInd(0)
	, m_FlLgtEnabled(false)
	, m_Pickable(nullptr)
	, m_Scene(p_Scene)
	, m_CollBox(m_HalfPlayerWidth, -m_HalfPlayerWidth, m_HalfPlayerHeight, -m_HalfPlayerHeight, m_HalfPlayerWidth, -m_HalfPlayerWidth)
{
	m_WorldPos = p_StartPos;
	MovableMesh::orient(glm::normalize(glm::fquat(5.f, 2.f, 0.0f, 0.0f)));
	m_RenderDisabled = true;
}


void Player::lookAtScreenCoords(int32_t p_NewScreenX, int32_t p_NewScreenY)
{
	if (m_Blocked) m_CameraPiloting->rotateCamera(m_Orientation, p_NewScreenX, p_NewScreenY);
	else m_Orientation = m_CameraPiloting->rotateCamera(m_Orientation, p_NewScreenX, p_NewScreenY);
}

AActorBase* Player::resolvePicking(const glm::vec3 & p_WorldPos)
{
	return m_Scene->collidesWithStatic(&CollidablePoint(p_WorldPos), true);
}

void Player::interactWithActiv(const Activator * p_Activator, const glm::vec3& p_WorldPoint)
{
	if (!p_Activator)
	{
		return;
	}
	switch (p_Activator->m_Type)
	{
	case Activator::Pickable:
		if (p_Activator->getDistance(m_WorldPos) > 6.f)
		{
			std::cout << "Come closer to get weapon!" << std::endl;
			return;
		}
		if (m_Pickable) delete m_Pickable;
		m_Pickable = new Activator(*p_Activator);
		m_RenderDisabled = false;
		m_Loaded = true;
		break;
	case Activator::Targetable:
	{
		if (!m_Pickable)
		{
			std::cout << "Take a gun first!" << std::endl;
			return;
		}
		if (p_Activator->getDistance(m_WorldPos) < 90.f)
		{
			std::cout << "Don't cheat! Go farther!" << std::endl;
			return;
		}
		int32_t score = p_Activator->getPoints(p_WorldPoint);
		switch (score)
		{
		case 10:
			std::cout << "Bull's eye! Scored: " << score << std::endl;
			return;
		case 5:
			std::cout << "Average result: " << score << std::endl;
			return;
		case 2:
			std::cout << "Worst case: " << score << "..." << std::endl;
			return;
		default:
			std::cout << "Miss" << std::endl;
			return;
		}
	}
	}


}

void Player::jump()
{
	if (m_JumpingTimer || m_Blocked) return;
	m_JumpingTimer = 32;
}

void Player::accelerate()
{
	//if i stand on the ground, NO vert acceleration MUST be applied
	if (m_Blocked) return;
	glm::vec3 directionAndForce(0.0f);
	if (m_ActionStateMachine & MovingForward)		directionAndForce += glm::vec3(0.0f, 0.0f, -0.5f);
	if (m_ActionStateMachine & MovingBackward)		directionAndForce += glm::vec3(0.0f, 0.0f, 0.5f);
	if (m_ActionStateMachine & MovingStrafeLeft)	directionAndForce += glm::vec3(-0.5f, 0.0f, 0.0f);
	if (m_ActionStateMachine & MovingStrafeRight)	directionAndForce += glm::vec3(0.5f, 0.0f, 0.0f);


	glm::mat4 currMat = Camera::calcViewMatrix(m_WorldPos, m_Orientation, 0.0f);
	glm::fquat orientation = glm::quat_cast(currMat);

	glm::fquat invOrient;
	if (m_Flying) {
		invOrient = glm::conjugate(orientation);
	}
	else {
		float pitch = glm::pitch(glm::fquat(orientation.w, orientation.y, orientation.z, orientation.x));
		glm::fquat walkOnly = glm::fquat(cosf(pitch / 2.0f), 0.f, sinf(pitch / 2.0f), 0.f);
		invOrient = glm::conjugate(walkOnly);
	}

	glm::vec3 additiveAccel = invOrient * directionAndForce;

	if (!m_Flying)
	{
		if (m_JumpingTimer)
		{
			if (m_JumpingTimer > 10) additiveAccel.y += m_JumpSpeed * m_JumpingTimer;
			else additiveAccel.y -= m_JumpSpeed * m_JumpingTimer * 2.f;
			--m_JumpingTimer;
		}
		else
			additiveAccel.y -= 0.5f;
	}

	if (m_Acceleration.x + additiveAccel.x > m_MaxSpeed)
	{
		m_Acceleration.x = m_MaxSpeed;
		m_Acceleration.y = additiveAccel.y;
		m_Acceleration.z = additiveAccel.z;
		return;
	}
	if (m_Acceleration.y + additiveAccel.y > m_MaxSpeed)
	{
		m_Acceleration.x = additiveAccel.x;
		m_Acceleration.y = m_MaxSpeed;
		m_Acceleration.z = additiveAccel.z;
		return;
	}
	if (m_Acceleration.z + additiveAccel.z > m_MaxSpeed)
	{
		m_Acceleration.x = additiveAccel.x;
		m_Acceleration.y = additiveAccel.y;
		m_Acceleration.z = m_MaxSpeed;
		return;
	}
	if (m_Acceleration.x + additiveAccel.x < -m_MaxSpeed)
	{
		m_Acceleration.x = -m_MaxSpeed;
		m_Acceleration.y = additiveAccel.y;
		m_Acceleration.z = additiveAccel.z;
		return;
	}
	if (m_Acceleration.y + additiveAccel.y < -m_MaxSpeed)
	{
		m_Acceleration.x = additiveAccel.x;
		m_Acceleration.y = -m_MaxSpeed;
		m_Acceleration.z = additiveAccel.z;
		return;
	}
	if (m_Acceleration.z + additiveAccel.z < -m_MaxSpeed)
	{
		m_Acceleration.x = additiveAccel.x;
		m_Acceleration.y = additiveAccel.y;
		m_Acceleration.z = -m_MaxSpeed;
		return;
	}
	m_Acceleration += additiveAccel;
}

void Player::applyAcceleration()
{
	glm::vec3 prevWorldPos = m_WorldPos;
	m_WorldPos += m_Acceleration;

	if (m_Acceleration.x < 0.10f && m_Acceleration.x > -0.10f) m_Acceleration.x = 0.f;
	else
		if (m_Acceleration.x > 0.f)
			m_Acceleration.x -= m_AccelAttenuation; else m_Acceleration.x += m_AccelAttenuation;

	if (m_Acceleration.y < 0.10f && m_Acceleration.y > -0.10f)  m_Acceleration.y = 0.f;
	else
		if (m_Acceleration.y > 0.f)
			m_Acceleration.y -= m_AccelAttenuation; else m_Acceleration.y += m_AccelAttenuation;


	if (m_Acceleration.z < 0.10f && m_Acceleration.z > -0.10f) m_Acceleration.z = 0.f;
	else
		if (m_Acceleration.z > 0.f)
			m_Acceleration.z -= m_AccelAttenuation; else m_Acceleration.z += m_AccelAttenuation;
	if (m_Flying) return;


	m_CollBox.relocate(m_WorldPos.x + m_HalfPlayerWidth, m_WorldPos.x - m_HalfPlayerWidth
		, m_WorldPos.y + m_HalfPlayerHeight, m_WorldPos.y - m_HalfPlayerHeight + 1.0f
		, m_WorldPos.z + m_HalfPlayerWidth, m_WorldPos.z - m_HalfPlayerWidth);
	bool highY = m_Scene->collidesWithStatic(&m_CollBox);

	m_CollBox.relocate(m_WorldPos.x + m_HalfPlayerWidth, m_WorldPos.x - m_HalfPlayerWidth
		, m_WorldPos.y + m_HalfPlayerHeight, m_WorldPos.y - m_HalfPlayerHeight
		, m_WorldPos.z + m_HalfPlayerWidth, m_WorldPos.z - m_HalfPlayerWidth);
	bool origY = m_Scene->collidesWithStatic(&m_CollBox);

	if (highY && origY || highY && !origY)
	{
		m_WorldPos = prevWorldPos;
	}
	if (!highY && origY)
	{
		m_WorldPos.y = prevWorldPos.y;
	}


	if (m_WorldPos.x > g_MapSize) m_WorldPos.x = g_MapSize;
	if (m_WorldPos.x < -g_MapSize) m_WorldPos.x = -g_MapSize;
	if (m_WorldPos.z > g_MapSize) m_WorldPos.z = g_MapSize;
	if (m_WorldPos.z < -g_MapSize) m_WorldPos.z = -g_MapSize;

}

void Player::render(MatrixStack & p_ViewMatrix) const
{
	MatrixStack inHandsMatStack;
	inHandsMatStack.translate(glm::vec3(0.2f, -0.25f, -1.1f));
	inHandsMatStack.rotate(glm::vec3(0.5f, 1.f, -0.0f), 0.05f);
	if (!m_RenderDisabled && m_Pickable)
		m_Pickable->renderPickableInHands(inHandsMatStack);
}

void Player::toggleFlashLight()
{
	if (m_Blocked) return;
	if (!m_FlLgtEnabled)
		m_FlashLightInd = m_Scene->getLightManager().addLight(new FlashLight(glm::vec3(0.7f, 0.5f, 0.4f), 75.f));
	else
		m_Scene->getLightManager().deleteLight(m_FlashLightInd);
	m_FlLgtEnabled = !m_FlLgtEnabled;
}


void Player::toggleMoveBlock(bool p_Block)
{
	m_Blocked = p_Block;
	if (p_Block) {
		if (m_FlLgtEnabled) toggleFlashLight();
		if (m_Pickable)
		{
			if (m_Pickable) delete m_Pickable;
			m_Pickable = nullptr;
		}
		m_Acceleration = glm::vec3(0.f);
	}
}