#include "shaders.h"
#include "misc.h"
#include "lights.h"


ALightBase::ALightBase(const glm::vec3& p_LightColor
	, float p_InitIntensity
	, float p_HalfLightDistance
	, int32_t p_Type)
	: AActorBase("", glm::vec3(0.f), glm::vec3(0.1f), glm::vec3(0.f), false)
	, m_LightData(p_LightColor, p_InitIntensity, 1.f / (p_HalfLightDistance * p_HalfLightDistance), p_Type)
	, m_HalfLightDistance(p_HalfLightDistance)
	, m_InitColor(p_LightColor)
	, m_InitIntensity(p_InitIntensity)
{
}

void ALightBase::reset()
{
	m_LightData.m_LightIntensity = m_InitIntensity;
	m_LightData.m_LightColor = m_InitColor;
	m_LightData.m_LightAttenuation = 1.f / (m_HalfLightDistance * m_HalfLightDistance);
}

void ALightBase::scale(const glm::vec3 & p_NewScale)
{
	m_LightData.m_LightIntensity = m_InitIntensity * ((p_NewScale.x + p_NewScale.y + p_NewScale.z) / 3.0f);
}

void ALightBase::scaleRelative(const glm::vec3 & p_NewScale)
{
	m_LightData.m_LightIntensity *= (p_NewScale.x + p_NewScale.y + p_NewScale.z) / 3.0f;
}


ALightBase::ShaderLightSource::ShaderLightSource(const glm::vec3 & p_LightColor
	, float p_InitIntensity
	, float p_InitAttenuation
	, uint32_t p_Type)
	: m_CamSpLightPos(glm::vec3(0.f))
	, m_LightIntensity(p_InitIntensity)
	, m_LightAttenuation(p_InitAttenuation)
	, m_LightColor(glm::clamp(p_LightColor, 0.f, 1.f))
	, m_Type(p_Type)
{
}


DirectionalLight::DirectionalLight(const glm::vec3& p_DirVector
	, const glm::vec3& p_LightColor
	, float p_InitIntensity
	, bool p_SpecularGauss)
	: ALightBase(p_LightColor, p_InitIntensity, 1.f, Directional | (p_SpecularGauss ? Gauss : Phong))
	, m_LightDirVect(glm::normalize(p_DirVector))
{
}

PointLight::PointLight(const glm::vec3 & p_WorldPos
	, const glm::vec3 & p_LightColor
	, float p_InitIntensity
	, float p_HalfLightDistance
	, bool p_SpecularGauss)
	: ALightBase(p_LightColor, p_InitIntensity, p_HalfLightDistance, Point | (p_SpecularGauss ? Gauss : Phong))
	, m_WorldSpLightPos(p_WorldPos)
{
}


SpotLight::SpotLight(const glm::vec3 & p_WorldPos
	, const glm::vec3 & p_DirVector
	, const glm::vec3 & p_LightColor
	, float p_InitIntensity
	, float p_Diameter
	, bool p_SpecularGauss)
	: ALightBase(p_LightColor, p_InitIntensity, glm::clamp(p_Diameter / 50.f, 1.1f, 2.f), Spot | (p_SpecularGauss ? Gauss : Phong))
	, m_LightDirVect(glm::normalize(p_DirVector))
	, m_WorldSpLightPos(p_WorldPos)
{
}


FlashLight::FlashLight(const glm::vec3 & p_LightColor
	, float p_InitIntensity
	, bool p_SpecularGauss)
	: ALightBase(p_LightColor, p_InitIntensity, 1.f, Spot | (p_SpecularGauss ? Gauss : Phong))
{
	m_LightData.m_LightAttenuation = 0.99f;
	m_LightData.m_CamSpLightPos = glm::vec3(0.f, 0.44f, 0.f);
	m_LightData.m_CamSpLightDir = glm::normalize(glm::vec4(0.f, 0.f, -1.f, 0.f));
}

void DirectionalLight::updateCamSpPos(const glm::mat4 & p_ViewMatrix)
{
	m_LightData.m_CamSpLightDir = glm::normalize(p_ViewMatrix * glm::vec4(m_LightDirVect, 0.f));
}

void PointLight::updateCamSpPos(const glm::mat4 & p_ViewMatrix)
{
	m_LightData.m_CamSpLightPos = p_ViewMatrix * glm::vec4(m_WorldSpLightPos, 1.f);
}


void SpotLight::updateCamSpPos(const glm::mat4 & p_ViewMatrix)
{
	m_LightData.m_CamSpLightPos = p_ViewMatrix * glm::vec4(m_WorldSpLightPos, 1.f);
	m_LightData.m_CamSpLightDir = glm::normalize(p_ViewMatrix * glm::vec4(m_LightDirVect, 0.f));

}

void SpotLight::rotate(const glm::vec3 & p_AxisOfRotation, float p_AngRadCCW)
{
	glm::mat3 rotationMatrix = glm::rotate(degreesToRadians(p_AngRadCCW), p_AxisOfRotation);
	m_LightDirVect = (m_LightDirVect * rotationMatrix);
	m_LightDirVect = glm::normalize(m_LightDirVect);
}




void DirectionalLight::rotate(const glm::vec3 & p_AxisOfRotation, float p_AngRadCCW)
{
	glm::mat3 rotationMatrix = glm::rotate(degreesToRadians(p_AngRadCCW), p_AxisOfRotation);
	m_LightDirVect = (m_LightDirVect * rotationMatrix);
	m_LightDirVect = glm::normalize(m_LightDirVect);
}



LightManager::~LightManager()
{
	for (auto &a : m_LightSources)
		delete a;
}

void LightManager::updateLgtCorrBlc() const
{
	glBindBuffer(GL_UNIFORM_BUFFER, m_LgtCorrBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(DShaderLgtCtrlBlc), &m_LgtCorrData);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindBufferRange(GL_UNIFORM_BUFFER, StaticMeshShader::m_LightCorrUnifBLCBndIdx, m_LgtCorrBuffer,
		0, sizeof(DShaderLgtCtrlBlc));
}

void LightManager::initLgtSrcBlc()
{
	uint32_t sizeLgtSrcStruct = sizeof(DShaderLgtSrc);


	glGenBuffers(1, &m_LgtSrcBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_LgtSrcBuffer);
	glBufferData(GL_UNIFORM_BUFFER, m_MaxLightSorces * sizeLgtSrcStruct + sizeof(int32_t), NULL, GL_DYNAMIC_DRAW);
	glBindBufferRange(GL_UNIFORM_BUFFER, StaticMeshShader::m_LgtSrcUnifBLCBndIdx, m_LgtSrcBuffer,
		0, m_MaxLightSorces * sizeLgtSrcStruct + sizeof(int32_t));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void LightManager::updateLights(const glm::mat4 & p_ViewMatrix, const glm::vec3& p_AmbientIntAndColor)
{
	float maxIntensity = 0.f;
	int32_t lgtSrcNum = m_LightSources.size();
	uint32_t sizeLgtSrcStruct = sizeof(DShaderLgtSrc);

	glBindBuffer(GL_UNIFORM_BUFFER, m_LgtSrcBuffer);
	for (int32_t lgt = 0; lgt < lgtSrcNum; ++lgt)
	{
		m_LightSources[lgt]->updateCamSpPos(p_ViewMatrix);
		glBufferSubData(GL_UNIFORM_BUFFER,
			(lgt * sizeLgtSrcStruct), sizeLgtSrcStruct, &m_LightSources[lgt]->m_LightData);

		float accumLight = m_LightSources[lgt]->m_LightData.m_LightIntensity *
			((m_LightSources[lgt]->m_LightData.m_LightColor.x
				+ m_LightSources[lgt]->m_LightData.m_LightColor.y
				+ m_LightSources[lgt]->m_LightData.m_LightColor.z) / 3.f);

		if (maxIntensity < accumLight)
			maxIntensity = accumLight;
	}
	glBufferSubData(GL_UNIFORM_BUFFER, m_MaxLightSorces * sizeLgtSrcStruct, sizeof(int32_t), &lgtSrcNum);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	m_LgtCorrData.m_MaxIntensity = maxIntensity +
		(p_AmbientIntAndColor.x + p_AmbientIntAndColor.y + p_AmbientIntAndColor.z) / 3.f;

	m_LgtCorrData.m_AmbientIntensity = p_AmbientIntAndColor * (m_LgtCorrData.m_MaxIntensity * 0.05f);
	m_LgtCorrData.m_MaxIntensity *= 1.2f;
	updateLgtCorrBlc();
}

void LightManager::setFog(const glm::vec3 & p_FogColor, float p_FogIntensity)
{
	m_LgtCorrData.m_FogIntensity = glm::clamp(p_FogIntensity, 0.f, 1.f) * m_MaxFogIntensity;
	m_LgtCorrData.m_FogColor = p_FogColor;
}

uint32_t LightManager::addLight(ALightBase * p_Light)
{
	if (m_LightSources.size() == m_MaxLightSorces) return 0;

	uint32_t sizeLgtSrcStruct = sizeof(DShaderLgtSrc);


	glBindBuffer(GL_UNIFORM_BUFFER, m_LgtSrcBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, m_LightSources.size() * sizeLgtSrcStruct, sizeLgtSrcStruct, &(p_Light->m_LightData));
	m_LightSources.push_back(p_Light);
	int32_t lgtSrcNum = m_LightSources.size();
	glBufferSubData(GL_UNIFORM_BUFFER, m_MaxLightSorces * sizeLgtSrcStruct, sizeof(int32_t), &lgtSrcNum);

	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	return m_LightSources.size() - 1;
}

void LightManager::deleteLight(uint32_t p_Ind)
{
	if (p_Ind > m_LightSources.size()) return;
	delete m_LightSources[p_Ind];
	std::vector<ALightBase*>::iterator iter = m_LightSources.begin();
	size_t ind = 0;
	for (auto &a : m_LightSources) {
		if (ind++ == p_Ind) break;
		iter++;
	}
	if (iter != m_LightSources.end())
		m_LightSources.erase(iter);
}

void LightManager::updateLight(uint32_t p_Ind, ALightBase* p_Light)
{
	if (p_Ind > m_LightSources.size()) return;
	delete m_LightSources[p_Ind];
	m_LightSources[p_Ind] = p_Light;
}

LightManager::ShaderLightCorrectionBlock::ShaderLightCorrectionBlock(float p_Gamma)
	: m_MaxIntensity(1.f)
	, m_Gamma(1.f / p_Gamma)
	, m_AmbientIntensity(glm::vec3(0.f)) {}




ALightBase *& LightManager::operator[](uint32_t p_LgtInd)
{
	if (p_LgtInd > m_LightSources.size()) throw OutOfRangeException("Material array");
	return m_LightSources[p_LgtInd];
}

const ALightBase* const& LightManager::operator[](uint32_t p_LgtInd) const
{
	if (p_LgtInd > m_LightSources.size()) throw OutOfRangeException("Material array");
	return m_LightSources[p_LgtInd];
}

LightManager::LightManager(const std::vector<AShaderBase*>& p_Shaders)
	: m_LgtCorrData(1.8f)
	, m_Shaders(p_Shaders)
	, m_LgtCorrBuffer(0)
	, m_LgtSrcBuffer(0)
{
	glGenBuffers(1, &m_LgtCorrBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_LgtCorrBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(DShaderLgtCtrlBlc), NULL, GL_DYNAMIC_DRAW);
	glBindBufferRange(GL_UNIFORM_BUFFER, StaticMeshShader::m_LightCorrUnifBLCBndIdx, m_LgtCorrBuffer,
		0, sizeof(DShaderLgtCtrlBlc));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	m_LightSources.reserve(m_MaxLightSorces);
	updateLgtCorrBlc();
	initLgtSrcBlc();
}
