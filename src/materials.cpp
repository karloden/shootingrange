#include "pgr.h"
#include "misc.h"
#include "parameters.h"
#include "materials.h"


GLuint Material::m_MtlUniformBuffer;
GLuint Material::m_SizeMaterialBlock;
GLuint Material::m_NoTexCap;
GLuint Material::m_DiffTexSampler;

Material::ShaderMaterialBlock::ShaderMaterialBlock()
	: m_DiffuseColor(1.f)
	, m_AmbientColor(1.f)
	, m_SpecularColor(0.2f)
	, m_Shininess(10.f)
{
}



Material::Material()
	: m_DiffuseTexture(m_NoTexCap)
	, m_NormalMapTexture(m_NoTexCap)
	, m_IndexBegin(0)
	, m_IndArrayVertexBase(0)
	, m_IndNum(0)

{
}

Material::~Material()
{
	if (m_DiffTexSampler) {
		glDeleteSamplers(1, &m_DiffTexSampler);
		m_DiffTexSampler = 0;
	}
	if (m_DiffuseTexture != m_NoTexCap) glDeleteTextures(1, &m_DiffuseTexture);
	if (m_NormalMapTexture != m_NoTexCap) glDeleteTextures(1, &m_DiffuseTexture);
}


void Material::loadDiffuseTexture(const std::string & p_FileName)
{
	try {
		std::string textureName = findFileOrThrow(g_TexturesPath + p_FileName);
		m_DiffuseTexture = pgr::createTexture(textureName);
	}
	catch (const FileException &a) {
		std::cerr << a.what() << std::endl;
	}
	m_ShaderMtlData.m_DiffuseColor.x = m_ShaderMtlData.m_DiffuseColor.y = m_ShaderMtlData.m_DiffuseColor.z = 0.0f;
}

void Material::loadNormalMapTexture(const std::string & p_FileName)
{
	if (!p_FileName.length()) {
		if(m_NormalMapTexture != m_NoTexCap) glDeleteTextures(1, &m_NormalMapTexture);
		m_NormalMapTexture = m_NoTexCap;
	}
	try {
		std::string textureName = findFileOrThrow(g_TexturesPath + p_FileName);
		m_NormalMapTexture = pgr::createTexture(textureName);
	}
	catch (const FileException &a) {
		std::cerr << a.what() << std::endl;
	}
}

void Material::setDiffuseColor(const glm::vec3& p_Color)
{
	m_ShaderMtlData.m_DiffuseColor = p_Color;
	if (m_DiffuseTexture != m_NoTexCap)
	{
		glDeleteTextures(1, &m_DiffuseTexture);
		m_DiffuseTexture = m_NoTexCap;
	}
}

void Material::setAmbientColor(const glm::vec3 & p_Color)
{
	m_ShaderMtlData.m_AmbientColor = p_Color;
}

void Material::setSpecularColor(const glm::vec3 & p_Color)
{
	m_ShaderMtlData.m_SpecularColor = p_Color;
}

void Material::setShininess(float p_Shininess)
{
	m_ShaderMtlData.m_Shininess = p_Shininess;
}

void Material::initialize(GLuint p_SizeMaterialBlock)
{
	m_SizeMaterialBlock = p_SizeMaterialBlock;
	uint32_t zeroVar = 0xFF000000;
	glGenTextures(1, &m_NoTexCap);
	glBindTexture(GL_TEXTURE_2D, m_NoTexCap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &zeroVar);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}


void Material::setMaterialBuffer(GLuint p_MtlUniformBuffer)
{
	m_MtlUniformBuffer = p_MtlUniformBuffer;
}



void Material::samplerRepeatEdge(bool p_Create)
{
	GLuint sampleRet;
	if (p_Create)
	{
		glGenSamplers(1, &sampleRet);
		glBindSampler(StaticMeshShader::m_DiffuseTexUnit, sampleRet);
		glBindSampler(StaticMeshShader::m_NormalMapTexUnit, sampleRet);
	}
	else sampleRet = m_DiffTexSampler;	
	glSamplerParameteri(sampleRet, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glSamplerParameteri(sampleRet, GL_TEXTURE_WRAP_T, GL_REPEAT);
	m_DiffTexSampler = sampleRet;
}

void Material::samplerClampEdge(bool p_Create)
{
	GLuint sampleRet;
	if (p_Create)
	{
		glGenSamplers(1, &sampleRet);
		glBindSampler(StaticMeshShader::m_DiffuseTexUnit, sampleRet);
		glBindSampler(StaticMeshShader::m_NormalMapTexUnit, sampleRet);
	}
	else sampleRet = m_DiffTexSampler;	
	glSamplerParameteri(sampleRet, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glSamplerParameteri(sampleRet, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	m_DiffTexSampler = sampleRet;
}

void Material::samplerMipMapLin(bool p_Create)
{
	GLuint sampleRet;
	if (p_Create)
	{
		glGenSamplers(1, &sampleRet);
		glBindSampler(StaticMeshShader::m_DiffuseTexUnit, sampleRet);
		glBindSampler(StaticMeshShader::m_NormalMapTexUnit, sampleRet);
	}
	else sampleRet = m_DiffTexSampler;
	glSamplerParameteri(sampleRet, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glSamplerParameteri(sampleRet, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	m_DiffTexSampler = sampleRet;

}

void Material::samplerAnisotropy(bool p_Create)
{
	GLuint sampleRet;
	if (p_Create)
	{
		glGenSamplers(1, &sampleRet);
		glBindSampler(StaticMeshShader::m_DiffuseTexUnit, sampleRet);
		glBindSampler(StaticMeshShader::m_NormalMapTexUnit, sampleRet);
	}
	else sampleRet = m_DiffTexSampler;
	glSamplerParameterf(sampleRet, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);
	m_DiffTexSampler = sampleRet;
}

