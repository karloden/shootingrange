#include "commands.h"
#include "player.h"

void MoveForward::execute(MovableMesh *p_Actor, bool p_Start)
{
	p_Actor->newAction(MovableMesh::MovingForward, p_Start);
}

void MoveBackward::execute(MovableMesh *p_Actor, bool p_Start)
{
	p_Actor->newAction(MovableMesh::MovingBackward, p_Start);
}

void MoveStrafeLeft::execute(MovableMesh *p_Actor, bool p_Start)
{
	p_Actor->newAction(MovableMesh::MovingStrafeLeft, p_Start);
}

void MoveStrafeRight::execute(MovableMesh *p_Actor, bool p_Start)
{
	p_Actor->newAction(MovableMesh::MovingStrafeRight, p_Start);
}

void MoveJump::execute(MovableMesh *p_Actor, bool p_Start)
{
	if (p_Start) static_cast<Player*>(p_Actor)->jump();
}

void PlayerToggleFlashlight::execute(MovableMesh * p_Actor, bool p_Start)
{
	if (!p_Start) static_cast<Player*>(p_Actor)->toggleFlashLight();
}


void PlayerLeanLeft::execute(MovableMesh * p_Actor, bool p_Start)
{
	//p_Actor->rotate(glm::vec3(0.0, 0.0f, 1.0f), 3.0f);
	static_cast<Player*>(p_Actor)->applyAdditionalRoll(-0.25f, p_Start);
}

void PlayerLeanRight::execute(MovableMesh * p_Actor, bool p_Start)
{
	//p_Actor->rotate(glm::vec3(0.0, 0.0f, 1.0f), -3.0f);
	static_cast<Player*>(p_Actor)->applyAdditionalRoll(0.25f, p_Start);
}

void PlayerToggleFly::execute(MovableMesh * p_Actor, bool p_Start)
{
	if (p_Start) static_cast<Player*>(p_Actor)->toggleFlyMode();
}

void MoveToggleView::execute(MovableMesh * p_Actor, bool p_Start)
{
	if (!p_Start) return;
	m_CurrView = ++m_CurrView % 3;
	if (m_CurrView == 0) static_cast<Player*>(p_Actor)->toggleMoveBlock(false);
	else static_cast<Player*>(p_Actor)->toggleMoveBlock(true);
	p_Actor->moveTo(m_Positions[m_CurrView]);
	p_Actor->orient(m_Orientations[m_CurrView]);
}
