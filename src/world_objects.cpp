#include "world_objects.h"
#include "misc.h"
#include "parameters.h"

AActorBase::AActorBase(const std::string& p_FileName
	, const glm::vec3& p_InitWorldPos
	, const glm::vec3& p_InitScale
	, const glm::vec3& p_InitRawDegrees
	, bool p_ChangeableVRAM)
	: m_Filename(p_FileName)
	, m_ChangeableVRAM(p_ChangeableVRAM)
	, m_Loaded(false)
	, m_RenderDisabled(false)
	, m_Mesh(nullptr)
{
	MatrixStack a;
	a.translate(p_InitWorldPos);
	a.scale(p_InitScale);
	a.rotate(rawDegreesToQuat(p_InitRawDegrees));
	m_InitTransform = a.top();
}


SkyBox::SkyBox(const glm::vec3& p_CloudColor, float p_CloudSpeed
	, float p_CloudOpacity, float p_HorizonFalloff)
	: AActorBase("skybox.obj", glm::vec3(0.f), glm::vec3(1.f), glm::vec3(0.f), false)
	, m_Skybox(p_CloudColor, p_CloudSpeed, p_CloudOpacity, p_HorizonFalloff)
{
	m_Mesh = m_Loader->getMeshGeomPtrOrLoad(m_Filename, SkyboxActor, 0);
	if (m_Mesh)
	{
		m_Loaded = true;
		m_RenderDisabled = false;
	}
	glGenBuffers(1, &m_SkyBoxDataBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_SkyBoxDataBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeof(SkyBoxData), NULL, GL_DYNAMIC_DRAW);
	glBindBufferRange(GL_UNIFORM_BUFFER, SkyboxShader::m_SkyBoxUnifBlcBndInd, m_SkyBoxDataBuffer,
		0, sizeof(SkyBoxData));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void SkyBox::render(MatrixStack & p_ViewMatrix) const
{
	if (!isEnabled() || !isLoaded()) return;
	MatrixStack::PushAndDestroyOnBlock push(p_ViewMatrix);
	p_ViewMatrix.scale(1000.f);
	m_Mesh->render(p_ViewMatrix);
}


EffectsActor::EffectsActor(const glm::vec3& p_InitWorldPos
	, const glm::vec3& p_InitScale
	, int32_t p_TexScale
	, GLuint p_AlphaTimeLoc)
	: AActorBase("fxPlane.obj", p_InitWorldPos, p_InitScale, glm::vec3(0.f), false)
	, m_AlphaTimeLoc(p_AlphaTimeLoc)

{
	m_Mesh = m_Loader->getMeshGeomPtrOrLoad(m_Filename, FXActor, p_TexScale);
	if (m_Mesh)
	{
		m_Loaded = true;
		m_RenderDisabled = false;
	}
}

void EffectsActor::render(MatrixStack & p_ViewMatrix) const
{
	if (!isEnabled() || !isLoaded()) return;
	glm::fquat viewMatRot = glm::quat_cast(p_ViewMatrix.top());
	glm::fquat viewMatInvRot = glm::inverse(viewMatRot);
	MatrixStack::PushAndDestroyOnBlock push(p_ViewMatrix);

	p_ViewMatrix *= m_InitTransform;

	float roll = glm::roll(glm::fquat(viewMatRot.w, viewMatRot.y, viewMatRot.z, viewMatRot.x));

	p_ViewMatrix *= glm::mat4_cast(viewMatInvRot);
	p_ViewMatrix.rotate(glm::vec3(-1.f, 0.f, 0.f), -roll);


	glUniform1f(m_AlphaTimeLoc, m_AlphaTime);
	m_Mesh->render(p_ViewMatrix);
}

SkyBox::SkyBoxData::SkyBoxData(const glm::vec3& p_CloudColor, float p_CloudSpeed
	, float p_CloudOpacity, float p_HorizonFalloff)
	: m_CloudColor(p_CloudColor), m_CloudSpeed(p_CloudSpeed)
	, m_CloudOpacity(p_CloudOpacity), m_HorizonFalloff(p_HorizonFalloff)
{}

void SkyBox::update(float p_Time
	, const glm::vec3 & p_SunDirection, const glm::vec3 & p_MoonDirection
	, const glm::vec3 & p_ZenithColor, const glm::vec3 & p_HorizonColor, float p_StarsBrightness)
{
	m_Skybox.m_Time = p_Time;
	m_Skybox.m_SunDirection = p_SunDirection;
	m_Skybox.m_MoonDirection = p_MoonDirection;
	m_Skybox.m_ZenithColor = p_ZenithColor;
	m_Skybox.m_HorizonColor = p_HorizonColor;
	m_Skybox.m_StarsBrightness = p_StarsBrightness;
	glBindBuffer(GL_UNIFORM_BUFFER, m_SkyBoxDataBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(SkyBoxData), &m_Skybox);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindBufferRange(GL_UNIFORM_BUFFER, SkyboxShader::m_SkyBoxUnifBlcBndInd, m_SkyBoxDataBuffer,
		0, sizeof(SkyBoxData));
}

float AActorBase::getDistanceCamera(const glm::vec3 & p_PlayerWorldPos)
{
	return glm::distance(glm::vec3(m_InitTransform[3]), p_PlayerWorldPos);
}
