#include <random>
#include "scene_handler.h"
#include "player.h"


SceneHandler::SceneHandler(const std::vector<AShaderBase*>& p_Shaders)
	: m_Shaders(p_Shaders)
	, m_MeshLoader(p_Shaders)
	, m_LightManager(p_Shaders)
	, m_MtlUnifBuffer(0)
	, m_FogDisabled(false)
	, m_CopterTimer(Timer::Single, 15.f)
	, m_SunTimer(Timer::Loop, 40.f)
	, m_FireTimer(Timer::Loop, 2.f)
	, m_CommonEndlessTimer(Timer::Infinite)
{
	//Counting UBO minimal block size for material
	int32_t uboAlignSize = 0;
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &uboAlignSize);
	m_SizeMatBLC = sizeof(DShaderMaterial);
	//If material doesnt fit in UBO blocks alignment, then add difference, else add nothing 
	m_SizeMatBLC += (m_SizeMatBLC % uboAlignSize) ? uboAlignSize - (m_SizeMatBLC % uboAlignSize) : 0;
	Material::initialize(m_SizeMatBLC);
	AActorBase::setLoader(m_MeshLoader);
	create();
	//Buffering all materials to one UBO
	bufferMaterials(m_MeshLoader.prepareAndGetAllShaderMaterials(), m_SizeMatBLC);
	m_Skybox = new SkyBox(glm::vec3(1.f), 0.001f, 1.0f, 150.f);
	setInterpolators();
}

void SceneHandler::render(MatrixStack & p_ViewMatrix)
{
	m_SunTimer.update();
	m_LightManager.updateLights(p_ViewMatrix.top(), m_SuncolorInterpolator.interpolate(m_SunTimer.getAlpha()));
	glUseProgram(m_Shaders[ComplMaterialActor]->m_Program);

	for (const auto&a : m_Meshes) {
		a->render(p_ViewMatrix);
	}

	glUseProgram(m_Shaders[SkyboxActor]->m_Program);

	m_Skybox->render(p_ViewMatrix);




	glUseProgram(m_Shaders[ComplMaterialActor]->m_Program);
	std::sort(m_TransparentObjects.begin(), m_TransparentObjects.end(), [&](AActorBase* a, AActorBase* b)
	{
		const glm::vec3 &pos = static_cast<Player*>(m_Meshes[m_PlayerInd])->getCurrPosition();
		return a->getDistanceCamera(pos) > b->getDistanceCamera(pos);
	});
	for (const auto&a : m_TransparentObjects) {
		a->render(p_ViewMatrix);
	}
	glUseProgram(m_Shaders[FXActor]->m_Program);
	m_FireEffect->render(p_ViewMatrix);


	glUseProgram(0);

}




AActorBase* SceneHandler::collidesWithStatic(const ACollidableBase * p_Movable, bool p_ActivatorsOnly) const
{
	if (p_ActivatorsOnly)
		for (const auto&actor : m_Activators) {
			if (actor->collides(p_Movable)) return actor;
		}
	else
	{
		for (const auto&actor : m_Meshes) {
			if (!actor) continue;
			if (actor->collides(p_Movable)) return actor;
		}
		for (const auto&actor : m_TransparentObjects) {
			if (actor->collides(p_Movable)) return actor;
		}
	}
	return nullptr;
}


void SceneHandler::addDynamics()
{

	m_Meshes[m_CopterInd] = new MovableMesh("copter.obj", glm::vec3(50.f, 0.5f, 20.f), glm::vec3(10.0f), glm::vec3(0.f, -45.f, 0.f));

	Material* material = m_MeshLoader.getConnectedMaterial("copter.obj", 0);
	material->loadDiffuseTexture("copter_main.dds");
	material->samplerMipMapLin(true);
	material->samplerAnisotropy();
	material->samplerClampEdge();

	material = m_MeshLoader.getConnectedMaterial("copter.obj", 4);
	material->setDiffuseColor(glm::vec3(0.f));

	material = m_MeshLoader.getConnectedMaterial("copter.obj", 2);
	material->setDiffuseColor(glm::vec3(1.f));
	material->loadDiffuseTexture("full_transp.dds");

	material = m_MeshLoader.getConnectedMaterial("copter.obj", 3);
	material->loadDiffuseTexture("copter_seat.dds");

	m_Meshes[m_BoarInd] = new MovableMesh("boar.obj", glm::vec3(2.4f, -2.4f, 12.6f), glm::vec3(4.5f), glm::vec3(0.f, 50.f, 0.f), 3);
	m_Meshes[m_BoarHandleInd] = new MovableMesh("boar.obj", glm::vec3(2.4f, -2.4f, 12.6f), glm::vec3(4.5f), glm::vec3(0.f, 50.f, 0.f), 1);
	material = m_MeshLoader.getConnectedMaterial("boar.obj", 3);
	material->loadDiffuseTexture("boar_grill.dds");
	material->setSpecularColor(glm::vec3(0.1f, 0.09f, 0.07f));

	material = m_MeshLoader.getConnectedMaterial("boar.obj", 1);
	material->loadDiffuseTexture("grill_handle.dds");
}

void SceneHandler::addStaticProps()
{

	m_Meshes.push_back(new StaticMesh("btr.obj", glm::vec3(10.5f, -1.f, -20.f), glm::vec3(10.0f), glm::vec3(0.f, 90.f, 0.f)));

	Material* material = m_MeshLoader.getConnectedMaterial("btr.obj", 1);
	material->loadDiffuseTexture("btr.dds");
	material->loadNormalMapTexture("btr_norm.dds");
	material->setSpecularColor(glm::vec3(0.1f));
	material->setShininess(4.f);

	m_Meshes.push_back(new StaticMesh("receiver.obj", glm::vec3(17.5f, 0.5f, 10.f), glm::vec3(5.0f), glm::vec3(0.f, -120.f, 0.f)));
	material = m_MeshLoader.getConnectedMaterial("receiver.obj", 1);
	material->loadDiffuseTexture("receiver.dds");

	m_Meshes.push_back(new StaticMesh("box.obj", glm::vec3(25.5f, -3.5f, 0.f), glm::vec3(2.0f), glm::vec3(0.f, -20.f, 0.f)));
	material = m_MeshLoader.getConnectedMaterial("box.obj", 0);
	material->loadDiffuseTexture("wood.dds");
	material->loadNormalMapTexture("wood_norm.dds");
	material->setSpecularColor(glm::vec3(0.1f, 0.09f, 0.07f));


	m_Meshes.push_back(new StaticMesh("boar.obj", glm::vec3(2.4f, -2.4f, 12.6f), glm::vec3(4.5f), glm::vec3(0.f, 50.f, 0.f), 2));
	m_Meshes.push_back(new StaticMesh("boar.obj", glm::vec3(2.4f, -2.4f, 12.6f), glm::vec3(4.5f), glm::vec3(0.f, 50.f, 0.f), 0));

	material = m_MeshLoader.getConnectedMaterial("boar.obj", 2);
	material->loadDiffuseTexture("bark.dds");
	material->loadNormalMapTexture("bark_norm.dds");
	material->setSpecularColor(glm::vec3(0.1f, 0.09f, 0.07f));

	material = m_MeshLoader.getConnectedMaterial("boar.obj", 0);
	material->loadDiffuseTexture("bark.dds");
	material->loadNormalMapTexture("bark_norm.dds");
	material->setSpecularColor(glm::vec3(0.1f, 0.09f, 0.07f));


	for (uint32_t i = 0; i < 3; i++)
		for (uint32_t j = 0; j < 3; j++) {
			m_Meshes.push_back(new StaticMesh(
				"Plane.obj", glm::vec3(-160.f + (i * 160.f), -5.0f, -160.f + (j * 160.f)), glm::vec3(80.0f), glm::vec3(0.f, 0.f, 0.f), 0));
		}
	material = m_MeshLoader.getConnectedMaterial("Plane.obj", 0);
	material->loadDiffuseTexture("dirt.dds");
	material->loadNormalMapTexture("dirt_norm.dds");

	material->setShininess(5.f);
	material->setSpecularColor(glm::vec3(0.1f, 0.088f, 0.075f));



	for (uint32_t i = 0; i < 4; i++) {
		m_Meshes.push_back(new StaticMesh("target.obj", glm::vec3(120.5f, -0.5f, 0.f), glm::vec3(5.0f), glm::vec3(0.f, -80.f, 0.f), i));
		m_Meshes.push_back(new StaticMesh("target.obj", glm::vec3(120.5f, -0.5f, 10.f), glm::vec3(5.0f), glm::vec3(0.f, -80.f, 0.f), i));
		material = m_MeshLoader.getConnectedMaterial("target.obj", i);
		material->loadDiffuseTexture("wood.dds");
		material->loadNormalMapTexture("wood_norm.dds");
		material->setSpecularColor(glm::vec3(0.1f, 0.09f, 0.07f));
	}


	/*	m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(159.37f, 7.f, -38.6556f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, 30.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(162.944f, 7.f, 53.2774f), glm::vec3(15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(82.1997f, 7.f, -108.672f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, 60.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(67.9034f, 7.f, 203.71f), glm::vec3(15.f), glm::vec3(0.f, -70.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(-19.2281f, 7.f, 218.213f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(126.2f, 7.f, 65.8687f), glm::vec3(15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(114.611f, 7.f, -60.43f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(175.583f, 7.f, -130.142f), glm::vec3(15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(209.033f, 7.f, -5.41401f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -120.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(203.977f, 7.f, 101.022f), glm::vec3(15.f), glm::vec3(0.f, -240.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(196.026f, 7.f, 211.416f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -110.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(102.668f, 7.f, 130.264f), glm::vec3(15.f), glm::vec3(0.f, -90.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(18.7457f, 7.f, 168.15f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, 250.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(-54.2805f, 7.f, 179.269f), glm::vec3(15.f), glm::vec3(0.f, -12.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(11.8142f, 7.f, -188.148f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -10.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(-50.0126f, 7.f, -93.1738f), glm::vec3(15.f), glm::vec3(0.f, -60.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(72.9983f, 7.f, -65.8321f), glm::vec3(15.f, 12.f, 15.f), glm::vec3(0.f, -30.f, 0.f)));
		m_TransparentObjects.push_back(new StaticMesh("tree.obj", glm::vec3(153.577f, 7.f, -178.552f), glm::vec3(15.f), glm::vec3(0.f, -20.f, 0.f)));*/

		//"Minimum standard", recommended by Park, Miller, and Stockmeyer in 1993 from cppreference.com
	std::linear_congruential_engine<std::uint32_t, 48271, 0, 2147483647> pseudoRandomGenerator;
	AActorBase *dummyCollInitInst = new StaticMesh("tree.obj", glm::vec3(0.f), glm::vec3(0.f), glm::vec3(0.f));
	ACollidableBase * testCollideInstance = m_MeshLoader.getNewConnectedCollision("tree.obj", -1);
	for (uint32_t i = 0; i < 100; ++i) 
	{
		int x = (pseudoRandomGenerator() % 360) - 180;
		int z = (pseudoRandomGenerator() % 360) - 180;
		float scaleY = (pseudoRandomGenerator() % 70) * 0.1f + 10.f;
		float rotationY = (pseudoRandomGenerator() % 360);


		glm::vec3 position((x / 2.f), 0.f, (z / 2.f));
		float cosTreePosX = glm::dot(glm::normalize(position), glm::vec3(1.f, 0.f, 0.f));
		float cosTreePosZ = glm::dot(glm::normalize(position), glm::vec3(0.f, 0.f, 1.f));
		float xOffset = cosTreePosX * 140.f;
		float zOffset = cosTreePosZ * 140.f;
		position.x += xOffset;
		position.z += zOffset;
		position.y = ((scaleY - 10.f) / 7.f) * (12.f - 5.f) + 5.f;
		glm::mat4 collisionTransform = glm::translate(position);
		//prevent floor collision
		collisionTransform = glm::translate(collisionTransform, glm::vec3(0.f, 0.2f, 0.f));
		collisionTransform = glm::scale(collisionTransform, glm::vec3(8.f, scaleY, 8.f));
		testCollideInstance->reset();
		testCollideInstance->applyTransform(collisionTransform);

		if (collidesWithStatic(testCollideInstance)) { --i; continue; };
		StaticMesh *treePtr = new StaticMesh("tree.obj", position, glm::vec3(15.f, scaleY, 15.f), glm::vec3(0.f, rotationY, 0.f));
		treePtr->addCollTrans((glm::scale(glm::vec3(0.2f, 1.f, 0.2f))));
		m_TransparentObjects.push_back(treePtr);
	}

	material = m_MeshLoader.getConnectedMaterial("tree.obj", 0);
	material->loadDiffuseTexture("bark.dds");
	material->loadNormalMapTexture("bark_norm.dds");
	material = m_MeshLoader.getConnectedMaterial("tree.obj", 1);
	material->loadDiffuseTexture("leaves.dds");
	material->loadNormalMapTexture("leaves_normal.dds");


	m_FireEffect = new EffectsActor(glm::vec3(3.16f, -2.6f, 13.f), glm::vec3(3.f, 2.f, 3.f)
		, 4, static_cast<const FXShader*>(m_Shaders[FXActor])->m_AlphaTimeUnifLoc);
	m_FireEffect->getConnectedMaterialPtr()->loadDiffuseTexture("animated_fire.dds");
}

void SceneHandler::addActivators()
{
	m_Activators.resize(5);
	m_Meshes[m_VintorezInd] = new Activator(Activator::Pickable, "vintorez.obj", glm::vec3(26.f, -1.5f, 0.f), glm::vec3(2.0f), glm::vec3(0.f, 180.f, 90.f), glm::vec3(26.08f, -1.41f, -0.148f));
	m_Activators[m_VintorezInd] = m_Meshes[m_VintorezInd];
	Material* material = m_MeshLoader.getConnectedMaterial("vintorez.obj", 0);
	material->loadDiffuseTexture("vintorez.dds");
	material->loadNormalMapTexture("vintorez_norm.dds");

	m_Meshes[m_SVUInd] = new Activator(Activator::Pickable, "svu.obj", glm::vec3(24.5f, -1.5f, 0.f), glm::vec3(2.0f), glm::vec3(0.f, 180.f, 90.f), glm::vec3(24.52f, -1.40f, -0.13));
	m_Activators[m_SVUInd] = m_Meshes[m_SVUInd];
	material = m_MeshLoader.getConnectedMaterial("svu.obj", 0);
	material->loadDiffuseTexture("svu.dds");
	material->loadNormalMapTexture("svu_norm.dds");

	m_Meshes[m_AKInd] = new Activator(Activator::Pickable, "ak74.obj", glm::vec3(25.3f, -3.1f, 2.5f), glm::vec3(2.0f), glm::vec3(70.f, 0.f, 90.f), glm::vec3(24.94f, -3.10f, 2.60f));
	m_Activators[m_AKInd] = m_Meshes[m_AKInd];
	material = m_MeshLoader.getConnectedMaterial("ak74.obj", 0);
	material->loadDiffuseTexture("ak74.dds");
	material->loadNormalMapTexture("ak74_norm.dds");

	m_Meshes[m_Target1Ind] = new Activator(Activator::Targetable, "target.obj", glm::vec3(120.5f, -0.5f, 0.f)
		, glm::vec3(5.0f), glm::vec3(0.f, -80.f, 0.f), glm::vec3(120.17f, 1.87f, 0.015f), 4);
	m_Meshes[m_Target2Ind] = new Activator(Activator::Targetable, "target.obj", glm::vec3(120.5f, -0.5f, 10.f)
		, glm::vec3(5.0f), glm::vec3(0.f, -80.f, 0.f), glm::vec3(120.17f, 1.87f, 10.01f), 4);
	m_Activators[m_Target1Ind] = m_Meshes[m_Target1Ind];
	m_Activators[m_Target2Ind] = m_Meshes[m_Target2Ind];


	material = m_MeshLoader.getConnectedMaterial("target.obj", 4);
	material->loadDiffuseTexture("target.dds");
	material->loadNormalMapTexture("target_norm.dds");
	material->setSpecularColor(glm::vec3(0.1f));
}

void SceneHandler::create()
{
	m_Meshes.resize(9);

	addDynamics();
	addActivators();
	addStaticProps();

	m_LightManager.addLight(new DirectionalLight(glm::vec3(0.866f, 0.5f, 0.0f), glm::vec3(1.0f, 0.5f, 0.5f), 70.f));

	m_LightManager.addLight(new PointLight(glm::vec3(3.16f, -0.824f, -16.87f), glm::vec3(0.9f, 0.6f, 0.4f), 50.f, 10.f));
	m_LightManager.addLight(new PointLight(glm::vec3(3.14f, -0.831f, -23.16f), glm::vec3(0.9f, 0.6f, 0.4f), 50.f, 10.f));

	m_LightManager.addLight(new PointLight(glm::vec3(3.16f, -2.6f, 13.f), glm::vec3(1.0f, 0.2f, 0.1f), 70.f, 30.f));
}

void SceneHandler::setInterpolators()
{
	const std::vector<glm::vec3> positions =
	{
		//		glm::vec3(150.0f, 50.5f, 0.f),
				glm::vec3(50.0f, 15.5f, 0.f),
				glm::vec3(5.f, 15.5f, 0.f),
				glm::vec3(0.f, 0.f, 0.f),
	};
	m_CopterInterpolator.setValues(positions, false);

	const std::vector<glm::vec3> skyColors =
	{
		glm::vec3(0.2f, 0.4f, 0.8f),
		glm::vec3(0.6f, 0.2f, 0.1f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.6f, 0.2f, 0.1f),
		glm::vec3(0.2f, 0.4f, 0.8f),
	};

	m_SkycolorInterpolator.setValues(skyColors);

	const std::vector<float> sunIntensity =
	{
		90.f, 70.f, 10.f, 10.f, 10.f, 70.f, 90.f
	};

	m_SunIntensityInterpolator.setValues(sunIntensity);

	const std::vector<glm::vec3> sunColors =
	{
		glm::vec3(0.75f, 0.6f, 0.4f),
		glm::vec3(0.7f, 0.3f, 0.15f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.f, 0.f, 0.f),
		glm::vec3(0.7f, 0.3f, 0.15f),
		glm::vec3(0.75f, 0.6f, 0.4f),
	};



	m_SuncolorInterpolator.setValues(sunColors);
}



void SceneHandler::bufferMaterials(const std::vector<DShaderMaterial*>& p_Materials, GLuint p_SizeMtlBLC)
{
	if (!p_Materials.size()) return;
	size_t sizeMaterialUniformBuffer = p_SizeMtlBLC * p_Materials.size();


	std::vector<GLubyte> mtlBuffer;
	mtlBuffer.resize(sizeMaterialUniformBuffer, 0);

	GLubyte *bufferPtr = &mtlBuffer[0];

	for (size_t mtl = 0; mtl < p_Materials.size(); ++mtl)
		memcpy(bufferPtr + (mtl * p_SizeMtlBLC), p_Materials[mtl], sizeof(DShaderMaterial));

	if (m_MtlUnifBuffer) glDeleteBuffers(1, &m_MtlUnifBuffer);
	glGenBuffers(1, &m_MtlUnifBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, m_MtlUnifBuffer);
	glBufferData(GL_UNIFORM_BUFFER, sizeMaterialUniformBuffer, bufferPtr, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	Material::setMaterialBuffer(m_MtlUnifBuffer);
}

void SceneHandler::update()
{
	m_CopterTimer.update();
	m_SunTimer.update();
	m_FireTimer.update();
	m_CommonEndlessTimer.update();

	glm::vec3 pos = m_CopterInterpolator.interpolate(m_CopterTimer.getAlpha());
	m_Meshes[m_CopterInd]->moveTo(pos);

	float angle = 2.0f *  M_PI * m_SunTimer.getAlpha() + 0.5f;
	glm::vec4 sunDirection(0.0f);
	sunDirection.x = sinf(angle);
	sunDirection.y = cosf(angle);
	sunDirection = glm::rotate(glm::mat4(1.0f), 5.0f, glm::vec3(0.0f, 1.0f, 0.0f)) * sunDirection;

	glm::vec3 skyColor = m_SkycolorInterpolator.interpolate(m_SunTimer.getAlpha());
	float intensity = m_SunIntensityInterpolator.interpolate(m_SunTimer.getAlpha());

	glm::vec3 sunColor = m_SuncolorInterpolator.interpolate(m_SunTimer.getAlpha());



	m_LightManager.updateLight(0, new DirectionalLight(sunDirection, sunColor, intensity));

	//Strong fog in night and slightly in the day
	if (m_FogDisabled)
		m_LightManager.setFog(glm::vec3(0.f), 0.f);
	else
		m_LightManager.setFog(glm::vec3(0.4f - sunColor.z), 10.f / (intensity));

	m_Skybox->update(m_CommonEndlessTimer.getTimeSinceStart(), sunDirection, -sunDirection, skyColor, glm::vec3(0.8f),
		0.75f - sunColor.x);



	m_FireEffect->setAlpha(m_FireTimer.getAlpha());

}
