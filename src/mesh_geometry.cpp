#include <glm/gtc/type_ptr.hpp>
#include "mesh_geometry.h"
#include "parameters.h"



SimpleStaticMeshGeom::SimpleStaticMeshGeom(const std::string & p_FileName, int32_t p_MeshNum)
	: AGeometryBase(SimpleMatertialActor), m_UnifMtlBLCInd(0), m_InitCollision(nullptr)
{
	//TODO if filename extension is fbx -> import fbx routine
	try {
		m_Importer = new GLContImporter(p_FileName,
			p_MeshNum == -1 ? MeshImporterBase::HasDifferentMaterials : MeshImporterBase::NoFlags);
		m_Importer->setMeshIdxInScene(p_MeshNum);

	}
	catch (const EngineException& e) {
		std::cerr << e.what() << std::endl;
		throw ObjectConstructionException("Static mesh geometry");
	}

	m_VertexBO = m_Importer->getVertexBuffer();
	if (m_Importer->hasNormals()) m_NormalBO = m_Importer->getNormalBuffer();
	if (m_Importer->hasTangents()) m_TangentBO = m_Importer->getTangentBuffer();
	if (m_Importer->hasBitangents()) m_BitangentBO = m_Importer->getBitangentBuffer();
	if (m_Importer->hasTexCoords())m_TexCoordBO = m_Importer->getTexCoordBuffer();

	m_ElementBO = m_Importer->getElementBuffer();
	m_SimpleMaterial = m_Importer->getMaterial();
	m_IndNum = m_Importer->getIndecesNum();
	m_VertNum = m_Importer->getVerticesNum();
	m_InitCollision = m_Importer->getCollision();
}

SkyboxMeshGeom::SkyboxMeshGeom(const std::string & p_FileName)
	: AGeometryBase(SkyboxActor)
{
	GLContImporter *m_Importer;
	try {
		m_Importer = new GLContImporter(p_FileName, MeshImporterBase::NoFlags);
		m_Importer->setMeshIdxInScene(0);
	}
	catch (const EngineException& e) {
		std::cerr << e.what() << std::endl;
		throw ObjectConstructionException("Static mesh geometry");
	}

	m_VertexBO = m_Importer->getVertexBuffer();
	if (m_Importer->hasTexCoords())m_TexCoordBO = m_Importer->getTexCoordBuffer();

	m_ElementBO = m_Importer->getElementBuffer();
	m_SimpleMaterial = m_Importer->getMaterial();
	m_IndNum = m_Importer->getIndecesNum();
	m_VertNum = m_Importer->getVerticesNum();

}


FXGeom::FXGeom(const std::string & p_FileName, int32_t p_TexScale)
	: AGeometryBase(FXActor), m_TexScale(p_TexScale)
{
	GLContImporter *m_Importer;
	try {
		m_Importer = new GLContImporter(p_FileName, MeshImporterBase::NoFlags);
		m_Importer->setMeshIdxInScene(0);
	}
	catch (const EngineException& e) {
		std::cerr << e.what() << std::endl;
		throw ObjectConstructionException("Static mesh geometry");
	}

	m_VertexBO = m_Importer->getVertexBuffer();
	if (m_Importer->hasTexCoords())m_TexCoordBO = m_Importer->getTexCoordBuffer();

	m_ElementBO = m_Importer->getElementBuffer();
	m_SimpleMaterial = m_Importer->getMaterial();
	m_IndNum = m_Importer->getIndecesNum();
	m_VertNum = m_Importer->getVerticesNum();

}

AGeometryBase::~AGeometryBase()
{
	if (m_VAO) glDeleteVertexArrays(1, &m_VAO);
	if (m_ElementBO) glDeleteBuffers(1, &m_ElementBO);
	if (m_VertexBO) glDeleteBuffers(1, &m_VertexBO);
}

SimpleStaticMeshGeom::~SimpleStaticMeshGeom()
{
	if (m_NormalBO) glDeleteBuffers(1, &m_NormalBO);
	if (m_TexCoordBO) glDeleteBuffers(1, &m_TexCoordBO);
}


bool AGeometryBase::unload()
{
	if (--m_UsersNum) return false;
	else
	{
		if (m_VAO) glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
		if (m_ElementBO) glDeleteBuffers(1, &m_ElementBO);
		m_ElementBO = 0;
		if (m_VertexBO) glDeleteBuffers(1, &m_VertexBO);
		m_VertexBO = 0;
		if (m_TexCoordBO) glDeleteBuffers(1, &m_TexCoordBO);
		m_TexCoordBO = 0;
		m_FromOtherDestructor = false;
		return true;
	}
}

bool SimpleStaticMeshGeom::unload()
{
	if (AGeometryBase::unload()) {
		if (m_NormalBO) glDeleteBuffers(1, &m_NormalBO);
		m_NormalBO = 0;
		if (m_TangentBO) glDeleteBuffers(1, &m_TangentBO);
		m_TangentBO = 0;
		if (m_BitangentBO) glDeleteBuffers(1, &m_BitangentBO);
		m_BitangentBO = 0;
		delete m_InitCollision;
		return true;
	} else return false;
}

bool StaticMeshGeom::unload()
{
	if (SimpleStaticMeshGeom::unload()) 
	{
		return true;
	}
	else return false;
}



std::vector<DShaderMaterial*> SimpleStaticMeshGeom::getMtlPtrs(int32_t p_UnifMtlBLCInd)
{
	m_UnifMtlBLCInd = p_UnifMtlBLCInd;
	return std::vector<DShaderMaterial*>(1, &m_SimpleMaterial.m_ShaderMtlData);
}


void AGeometryBase::loadShaderProgram(const AShaderBase * p_Shader)
{
	m_CommonShader = p_Shader;
	if (!m_VertexBO) return;

	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBO);
	glEnableVertexAttribArray(m_CommonShader->m_PosLoc);
	glVertexAttribPointer(m_CommonShader->m_PosLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindBuffer(GL_ARRAY_BUFFER, m_TexCoordBO);
	glEnableVertexAttribArray(m_CommonShader->m_TexCoordLoc);
	glVertexAttribPointer(m_CommonShader->m_TexCoordLoc, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ElementBO);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void SimpleStaticMeshGeom::loadShaderProgram(const AShaderBase * p_Shader)
{
	if (m_Importer) delete m_Importer;
	m_Importer = nullptr;
	m_ShaderStMesh = static_cast<const StaticMeshShader*>(p_Shader);
	AGeometryBase::loadShaderProgram(p_Shader);
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_NormalBO);
	glEnableVertexAttribArray(m_ShaderStMesh->m_NormalLoc);
	glVertexAttribPointer(m_ShaderStMesh->m_NormalLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_TangentBO);
	glEnableVertexAttribArray(m_ShaderStMesh->m_TangentLoc);
	glVertexAttribPointer(m_ShaderStMesh->m_TangentLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_BitangentBO);
	glEnableVertexAttribArray(m_ShaderStMesh->m_BitangentLoc);
	glVertexAttribPointer(m_ShaderStMesh->m_BitangentLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


StaticMeshGeom::StaticMeshGeom(const std::string & p_FileName)
	: SimpleStaticMeshGeom(p_FileName, -1)
{
	m_Materials = m_Importer->getMaterials();
	if (m_Importer) delete m_Importer;
	m_Importer = nullptr;
}


void SimpleStaticMeshGeom::render(MatrixStack & p_ModelViewMat) const
{
	glBindVertexArray(m_VAO);
	glUniformMatrix4fv(m_ShaderStMesh->m_MVmatrixUnifLoc, 1, GL_FALSE, 
		glm::value_ptr(p_ModelViewMat.top()));
	glm::mat3 normMatrix(p_ModelViewMat.top());
	glUniformMatrix3fv(m_ShaderStMesh->m_MVmatrixNormalUnifLoc, 1, GL_FALSE,
		glm::value_ptr(glm::transpose(glm::inverse(normMatrix))));
	glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_DiffuseTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_SimpleMaterial.m_DiffuseTexture);
	glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_NormalMapTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_SimpleMaterial.m_NormalMapTexture);
	glBindBufferRange(GL_UNIFORM_BUFFER
		, m_ShaderStMesh->m_MtlUnifBLCBndIdx, Material::m_MtlUniformBuffer
		, m_UnifMtlBLCInd * Material::m_SizeMaterialBlock
		, sizeof(DShaderMaterial));
	glDrawElements(GL_TRIANGLES, m_IndNum, GL_UNSIGNED_INT, (void*)0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_DiffuseTexUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glBindBufferBase(GL_UNIFORM_BUFFER, m_ShaderStMesh->m_MtlUnifBLCBndIdx, 0);
}


void StaticMeshGeom::render(MatrixStack & p_ModelViewMat) const
{
	glBindVertexArray(m_VAO);
	glUniformMatrix4fv(m_ShaderStMesh->m_MVmatrixUnifLoc, 1, GL_FALSE, 
		glm::value_ptr(p_ModelViewMat.top()));
	glm::mat3 normMatrix(p_ModelViewMat.top());
	glUniformMatrix3fv(m_ShaderStMesh->m_MVmatrixNormalUnifLoc, 1, GL_FALSE,
		glm::value_ptr(glm::transpose(glm::inverse(normMatrix))));
	uint32_t mtlOffset = 0;
	for (const auto &material : m_Materials) {
		glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_DiffuseTexUnit);
		glBindTexture(GL_TEXTURE_2D, material.m_DiffuseTexture);
		glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_NormalMapTexUnit);
		glBindTexture(GL_TEXTURE_2D, material.m_NormalMapTexture);
		glBindBufferRange(GL_UNIFORM_BUFFER
			, m_ShaderStMesh->m_MtlUnifBLCBndIdx, Material::m_MtlUniformBuffer
			, (m_UnifMtlBLCInd + mtlOffset) * Material::m_SizeMaterialBlock
			, sizeof(DShaderMaterial));
		glDrawElementsBaseVertex(GL_TRIANGLES, material.m_IndNum, GL_UNSIGNED_INT
			, (void*)(material.m_IndexBegin * sizeof(uint32_t)), material.m_IndArrayVertexBase);
		mtlOffset++;

	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_DiffuseTexUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glBindBufferBase(GL_UNIFORM_BUFFER, m_ShaderStMesh->m_MtlUnifBLCBndIdx, 0);
}

std::vector<DShaderMaterial*> StaticMeshGeom::getMtlPtrs(int32_t p_UnifMtlBLCInd)
{
	m_UnifMtlBLCInd = p_UnifMtlBLCInd;
	std::vector<DShaderMaterial*> ret;
	for (auto &mtl : m_Materials) {
		ret.push_back(&(mtl.m_ShaderMtlData));
	}
	return ret;
}

Material * StaticMeshGeom::getConnectedMaterialPtr(int32_t p_MtlInd)
{
	return &m_Materials[p_MtlInd];
}


AGeometryBase::AGeometryBase(ActorType p_Type)
	: m_UsersNum(1), m_VertexBO(0), m_TexCoordBO(0), m_ElementBO(0)
	, m_VAO(0), m_VertNum(0), m_IndNum(0), m_CommonShader(nullptr), m_FromOtherDestructor(false), m_Type(p_Type)
{
	glGenVertexArrays(1, &m_VAO);
}




AGeometryBase * MeshFlyweightLoader::getMeshGeomPtrOrLoad(const std::string & p_FileName,
	const ActorType& p_ExpectedUsage, int32_t p_AdditionalInfo)
{
	if (!p_FileName.size()) return nullptr;
	std::pair<std::string, int32_t> lookupPair = std::pair<std::string, int32_t>(p_FileName, p_AdditionalInfo);
	auto found = m_PointerMap.find(lookupPair);
	if (found != m_PointerMap.end()) {
		found->second->newUser();
		return found->second;
	}
	AGeometryBase* newMesh = nullptr;
	try {
		if (p_AdditionalInfo < -1) throw ObjectConstructionException("Static mesh geometry");
		switch (p_ExpectedUsage)
		{
		case ComplMaterialActor:
			newMesh = new StaticMeshGeom(p_FileName);
			newMesh->loadShaderProgram(m_Shaders[ComplMaterialActor]);
			break;
		case SimpleMatertialActor:
			if (p_AdditionalInfo == -1) throw ObjectConstructionException("Static mesh geometry");
			newMesh = new SimpleStaticMeshGeom(p_FileName, p_AdditionalInfo);
			//SimpleMatertialActor uses same shader, that ComplicatedMaterialActor does, and it has ComplicatedMaterialActor - 1 in shader programms array
			newMesh->loadShaderProgram(m_Shaders[SimpleMatertialActor + 1]);
			break;
		case SkyboxActor:
			if (p_AdditionalInfo == -1) throw ObjectConstructionException("Skybox can have only one material");
			newMesh = new SkyboxMeshGeom(p_FileName);
			newMesh->loadShaderProgram(m_Shaders[SkyboxActor]);
			break;
		case FXActor:
			if (p_AdditionalInfo < 1) throw ObjectConstructionException("Animated FX texture scale cannot be less than zero");
			newMesh = new FXGeom(p_FileName, p_AdditionalInfo);
			newMesh->loadShaderProgram(m_Shaders[FXActor]);
			return newMesh;
		default:
			//TODO
			break;
		}
	}
	catch (const ObjectConstructionException& a) {
		std::cerr << a.what() << std::endl;
		newMesh = nullptr;
	}
	if (!newMesh) return nullptr;
	auto ret = m_PointerMap.insert(std::pair<std::pair<std::string, int32_t>, AGeometryBase*>(lookupPair, newMesh));
	if (!ret.second) return nullptr; else return ret.first->second;
}

std::vector<DShaderMaterial*> MeshFlyweightLoader::prepareAndGetAllShaderMaterials() const
{
	size_t mtlIndex = 0;
	std::vector<DShaderMaterial*> mtlList;
	for (auto it = m_PointerMap.cbegin(); it != m_PointerMap.cend(); ++it)
	{
		std::vector<DShaderMaterial*> retMtl = it->second->getMtlPtrs(mtlIndex);
		mtlIndex += retMtl.size();
		mtlList.insert(mtlList.end(), retMtl.begin(), retMtl.end());
	}
	return mtlList;
}


Material * MeshFlyweightLoader::getConnectedMaterial(const std::string & p_MeshName, int32_t p_MaterialInd) const
{
	if (!p_MeshName.size() || p_MaterialInd < 0) return nullptr;

	std::pair<std::string, int32_t> lookupPair = std::pair<std::string, int32_t>(p_MeshName, p_MaterialInd);
	bool wholeModel = false;
	auto found = m_PointerMap.find(lookupPair);
	if (found == m_PointerMap.end()) 
	{
		wholeModel = true;
		lookupPair = std::pair<std::string, int32_t>(p_MeshName, -1);
		found = m_PointerMap.find(lookupPair);
		if (found == m_PointerMap.end()) return nullptr;
	}

	return wholeModel ? found->second->getConnectedMaterialPtr(p_MaterialInd) : found->second->getConnectedMaterialPtr(0);

}

void MeshFlyweightLoader::freeMesh(const std::string & p_MeshName, int32_t p_MaterialInd)
{
	std::pair<std::string, int32_t> lookupPair = std::pair<std::string, int32_t>(p_MeshName, p_MaterialInd);
	auto found = m_PointerMap.find(lookupPair);
	if (found == m_PointerMap.end()) 
	{
		std::cerr << "Mesh geometry, that had to be freed, cannot be found. Possible RAM and VRAM memory leakage." << std::endl;
		return;
	}
	delete found->second;
	m_PointerMap.erase(found);
}

ACollidableBase * MeshFlyweightLoader::getNewConnectedCollision(const std::string & p_MeshName, int32_t p_MaterialInd) const
{
	if (!p_MeshName.size() || p_MaterialInd < -1) return nullptr;

	std::pair<std::string, int32_t> lookupPair = std::pair<std::string, int32_t>(p_MeshName, p_MaterialInd);
	bool wholeModel = false;
	auto found = m_PointerMap.find(lookupPair);
	if (found == m_PointerMap.end()) return nullptr;
	return static_cast<SimpleStaticMeshGeom*>(found->second)->getInitCollision()->clone();
}

void SkyboxMeshGeom::render(MatrixStack & p_ViewMat) const
{
	glBindVertexArray(m_VAO);
	glm::mat3 rotView = p_ViewMat.top();

	glUniformMatrix3fv(m_Shader->m_VmatrixUnifLoc, 1, GL_FALSE, glm::value_ptr(rotView));

	glActiveTexture(GL_TEXTURE0 + SkyboxShader::m_SkyboxBigCloudsTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_BigCloudsTex);
	glActiveTexture(GL_TEXTURE0 + SkyboxShader::m_SkyboxSmallCloudsTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_SmallCloudsTex);
	glActiveTexture(GL_TEXTURE0 + SkyboxShader::m_SkyboxStarsTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_StarsTex);
	glDrawElements(GL_TRIANGLES, m_IndNum, GL_UNSIGNED_INT, (void*)0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0 + SkyboxShader::m_SkyboxSmallCloudsTexUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0 + SkyboxShader::m_SkyboxBigCloudsTexUnit);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
}
void SkyboxMeshGeom::loadShaderProgram(const AShaderBase * p_Shader)
{
	AGeometryBase::loadShaderProgram(p_Shader);
	m_Shader = static_cast<const SkyboxShader*>(p_Shader);
	try {
		std::string textureName = findFileOrThrow(g_TexturesPath + "cloudes_big.dds");
		m_BigCloudsTex = pgr::createTexture(textureName);
		textureName = findFileOrThrow(g_TexturesPath + "cloudes_small.dds");
		m_SmallCloudsTex = pgr::createTexture(textureName);
		textureName = findFileOrThrow(g_TexturesPath + "stars.dds");
		m_StarsTex = pgr::createTexture(textureName);
	}
	catch (const FileException &a) {
		std::cerr << a.what() << std::endl;
	}
}


void FXGeom::render(MatrixStack & p_ModelViewMat) const
{
	glBindVertexArray(m_VAO);
	glUniformMatrix4fv(m_Shader->m_MVmatrixUnifLoc, 1, GL_FALSE, glm::value_ptr(p_ModelViewMat.top()));
	glActiveTexture(GL_TEXTURE0 + StaticMeshShader::m_DiffuseTexUnit);
	glBindTexture(GL_TEXTURE_2D, m_SimpleMaterial.m_DiffuseTexture);
	glDrawElements(GL_TRIANGLES, m_IndNum, GL_UNSIGNED_INT, (void*)0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
}

void FXGeom::loadShaderProgram(const AShaderBase * p_Shader)
{
	AGeometryBase::loadShaderProgram(p_Shader);
	m_Shader = static_cast<const FXShader*>(p_Shader);
	glUseProgram(m_Shader->m_Program);
	glUniform1i(m_Shader->m_TexScaleUnifLoc, m_TexScale);
	glUseProgram(0);
}

bool SkyboxMeshGeom::unload()
{
	if (AGeometryBase::unload()) {
		return true;
	}
	else return false;
}
