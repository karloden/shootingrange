#include <limits>
#include "transformations.h"
#include "collision.h"

#undef max
#undef min

CollidableBox::CollidableBox(float p_InitMaxX, float p_InitMinX
	, float p_InitMaxY, float p_InitMinY
	, float p_InitMaxZ, float p_InitMinZ)
	: ACollidableBase(BoxCollision)
	, m_InitMaxZ(p_InitMaxZ), m_InitMinZ(p_InitMinZ)
	, m_InitMaxY(p_InitMaxY), m_InitMinY(p_InitMinY)
	, m_InitMaxX(p_InitMaxX), m_InitMinX(p_InitMinX)
	, m_CurrMaxZ(p_InitMaxZ), m_CurrMinZ(p_InitMinZ)
	, m_CurrMaxY(p_InitMaxY), m_CurrMinY(p_InitMinY)
	, m_CurrMaxX(p_InitMaxX), m_CurrMinX(p_InitMinX) {}

bool CollidableBox::checkCollision(const ACollidableBase * p_Other) const
{
	switch (p_Other->getType()) {
		case BoxCollision: return checkBoxCollision(static_cast<const CollidableBox*>(p_Other));
		case WorldPoint: return pointInCollidable(static_cast<const CollidablePoint*>(p_Other));
		default: return false;
	}
}

bool CollidableBox::pointInCollidable(const CollidablePoint* p_Point) const
{
	return (p_Point->m_Point.x >= m_CurrMinX && p_Point->m_Point.x <= m_CurrMaxX)
		&& (p_Point->m_Point.y >= m_CurrMinY && p_Point->m_Point.y <= m_CurrMaxY)
		&& (p_Point->m_Point.z >= m_CurrMinZ && p_Point->m_Point.z <= m_CurrMaxZ);
}

void CollidableBox::relocate(float p_MaxX, float p_MinX
	, float p_MaxY, float p_MinY
	, float p_MaxZ, float p_MinZ)
{
	m_CurrMaxX = p_MaxX;
	m_CurrMinX = p_MinX;
	m_CurrMaxY = p_MaxY;
	m_CurrMinY = p_MinY;
	m_CurrMaxZ = p_MaxZ;
	m_CurrMinZ = p_MinZ;
}

void CollidableBox::reset()
{
	m_CurrMaxX = m_InitMaxX;
	m_CurrMinX = m_InitMinX;
	m_CurrMaxY = m_InitMaxY;
	m_CurrMinY = m_InitMinY;
	m_CurrMaxZ = m_InitMaxZ;
	m_CurrMinZ = m_InitMinZ;
}

void CollidableBox::scale(const glm::vec3& p_Scales)
{
	float middleX = fabsf((m_CurrMaxX + m_CurrMinX) / 2.f);
	float diffX = ((m_CurrMaxX - m_CurrMinX) * p_Scales.x) / 2.f;
	m_CurrMaxX = middleX + diffX;
	m_CurrMinX = middleX - diffX;

	float middleY = fabsf((m_CurrMaxY + m_CurrMinY) / 2.f);
	float diffY = ((m_CurrMaxY - m_CurrMinY) * p_Scales.y) / 2.f;
	m_CurrMaxY = middleY + diffY;
	m_CurrMinY = middleY - diffY;

	float middleZ = fabsf((m_CurrMaxZ + m_CurrMinZ) / 2.f);
	float diffZ = ((m_CurrMaxZ - m_CurrMinZ) * p_Scales.z) / 2.f;
	m_CurrMaxZ = middleZ + diffZ;
	m_CurrMinZ = middleZ - diffZ;
}

void CollidableBox::applyTransform(const glm::mat4 & p_InitTransform)
{

	std::vector<glm::vec4> boundingToVerts =
	{
		glm::vec4(m_CurrMinX, m_CurrMaxY, m_CurrMinZ, 1.f),
		glm::vec4(m_CurrMinX, m_CurrMaxY, m_CurrMaxZ, 1.f),
		glm::vec4(m_CurrMaxX, m_CurrMaxY, m_CurrMinZ, 1.f),
		glm::vec4(m_CurrMaxX, m_CurrMaxY, m_CurrMaxZ, 1.f),
		glm::vec4(m_CurrMinX, m_CurrMinY, m_CurrMinZ, 1.f),
		glm::vec4(m_CurrMinX, m_CurrMinY, m_CurrMaxZ, 1.f),
		glm::vec4(m_CurrMaxX, m_CurrMinY, m_CurrMinZ, 1.f),
		glm::vec4(m_CurrMaxX, m_CurrMinY, m_CurrMaxZ, 1.f),
	};

	m_CurrMaxX = m_CurrMaxY = m_CurrMaxZ = -std::numeric_limits<float>::max();
	m_CurrMinX = m_CurrMinY = m_CurrMinZ = std::numeric_limits<float>::max();
	for (auto &vert : boundingToVerts) {
		vert = p_InitTransform * vert;
		if (vert.x > m_CurrMaxX) m_CurrMaxX = vert.x;
		if (vert.x < m_CurrMinX) m_CurrMinX = vert.x;
		if (vert.y > m_CurrMaxY) m_CurrMaxY = vert.y;
		if (vert.y < m_CurrMinY) m_CurrMinY = vert.y;
		if (vert.z > m_CurrMaxZ) m_CurrMaxZ = vert.z;
		if (vert.z < m_CurrMinZ) m_CurrMinZ = vert.z;
	}
}

bool CollidableBox::checkBoxCollision(const CollidableBox * p_Other) const
{
	return (m_CurrMinX <= p_Other->m_CurrMaxX && m_CurrMaxX >= p_Other->m_CurrMinX)
		&& (m_CurrMinY <= p_Other->m_CurrMaxY && m_CurrMaxY >= p_Other->m_CurrMinY)
		&& (m_CurrMinZ <= p_Other->m_CurrMaxZ && m_CurrMaxZ >= p_Other->m_CurrMinZ);
}

bool CollidablePoint::checkCollision(const ACollidableBase * p_Other) const
{
	switch (p_Other->getType()) {
	case BoxCollision: return checkBoxCollision(static_cast<const CollidableBox*>(p_Other));
	case WorldPoint: return pointInCollidable(static_cast<const CollidablePoint*>(p_Other));
	default: return false;
	}
}

bool CollidablePoint::pointInCollidable(const CollidablePoint* p_Point) const
{
	return fabsf(m_Point.x - p_Point->m_Point.x) < std::numeric_limits<float>::epsilon()
		&& fabsf(m_Point.y - p_Point->m_Point.y) < std::numeric_limits<float>::epsilon()
		&& fabsf(m_Point.z - p_Point->m_Point.z) < std::numeric_limits<float>::epsilon();
}
