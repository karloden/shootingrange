#include "misc.h"
#include "exceptions.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)

std::string getPathToBinary()
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string path = buffer;
	std::string::size_type pos = path.find_last_of("\\/");
	path = path.substr(0, pos);

	pos = path.find_last_of("\\/");

	std::string::size_type posDebug = path.find("Debug");
	std::string::size_type posRelease = path.find("Release");
	if ((posDebug != std::string::npos && path.length() - 5 == posDebug) ||
		(posRelease != std::string::npos && path.length() - 7 == posRelease)) return path.substr(0, pos);
	return path;
}

#else

std::string getPathToBinary() {
	char result[PATH_MAX];
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string path(result, (unsigned long)((count > 0) ? count : 0));
	path = path.substr(0, path.find_last_of("/") + 1);
	return path;
}

#endif


std::string convertBackspaces(const std::string &str)
{
	std::string retStr = str;
	auto iter = retStr.begin();
	auto end = retStr.end();
	while (iter != end) {
		iter = std::find(iter, end, '\b');
		if (iter == end) break;
		if (iter == retStr.begin())
			iter = retStr.erase(iter);
		else
			iter = retStr.erase(iter - 1, iter + 1);
		end = retStr.end();
	}
	return retStr;
}

std::string findFileOrThrow(const std::string &strBasename)
{
	std::ifstream testFile(strBasename.c_str(), std::ios::binary);
	char test;
	testFile.read(&test, sizeof(char));
	if (!testFile.is_open() || !testFile.good() || testFile.fail()) {
		testFile.close();
		throw FileException(strBasename, "file cannot be read.");
	}
	testFile.close();
	return strBasename;
}
