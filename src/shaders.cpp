#include <sstream>

#include "shaders.h"
#include "exceptions.h"
#include "parameters.h"


AShaderBase::~AShaderBase()
{
	glDeleteProgram(m_Program);
}

GLuint AShaderBase::compileShader(GLenum p_ShaderType, const std::string & p_ShaderFilename) const
{
	std::string strFilename = findFileOrThrow(g_ShadersPath + p_ShaderFilename);
	GLuint shader = glCreateShader(p_ShaderType);
	std::ifstream shaderFile(strFilename.c_str());
	std::stringstream shaderData;
	shaderData << shaderFile.rdbuf();
	shaderFile.close();
	std::string shaderText = shaderData.str();
	GLint textLength = (GLint)shaderText.size();
	const GLchar *pText = static_cast<const GLchar *>(shaderText.c_str());
	glShaderSource(shader, 1, &pText, &textLength);
	glCompileShader(shader);

	throwIfGLError(shader, false);

	return shader;
}

void AShaderBase::linkProgram(GLuint p_ShaderOne, GLuint p_ShaderTwo)
{
	m_Program = glCreateProgram();
	glAttachShader(m_Program, p_ShaderOne);
	glAttachShader(m_Program, p_ShaderTwo);
	glLinkProgram(m_Program);
	throwIfGLError(m_Program, true);
	glDetachShader(m_Program, p_ShaderOne);
	glDetachShader(m_Program, p_ShaderTwo);
	glDeleteShader(p_ShaderOne);
	glDeleteShader(p_ShaderTwo);
}

void AShaderBase::linkProgram(const std::vector<GLuint>& p_Shaders)
{
	m_Program = glCreateProgram();
	for (size_t loop = 0; loop < p_Shaders.size(); ++loop)
		glAttachShader(m_Program, p_Shaders[loop]);

	glLinkProgram(m_Program);
	throwIfGLError(m_Program, true);

	for (size_t loop = 0; loop < p_Shaders.size(); ++loop) {
		glDetachShader(m_Program, p_Shaders[loop]);
		glDeleteShader(p_Shaders[loop]);
	}
}

void AShaderBase::prepareAndCreateProgram(const std::vector<std::string>& p_Shaders)
{
	std::vector<GLuint> shaders;

	for (uint32_t i = 0; i < p_Shaders.size(); i++) {
		bool badName = false;
		std::string::size_type pos = p_Shaders[i].find_last_of(".");
		GLenum shaderType;
		if (pos != std::string::npos) {
			if (p_Shaders[i].substr(pos + 1) == "vert") shaderType = GL_VERTEX_SHADER; else
				if (p_Shaders[i].substr(pos + 1) == "frag") shaderType = GL_FRAGMENT_SHADER; else
					badName = true;
		}
		else badName = true;
		if (badName) throw FileException(p_Shaders[i], "Shader expected to have .vert or .frag extension");
		shaders.push_back(compileShader(shaderType, p_Shaders[i]));
	}
	linkProgram(shaders);
}

void AShaderBase::getLocations()
{
	m_PosLoc = glGetAttribLocation(m_Program, "position");
	m_TexCoordLoc = glGetAttribLocation(m_Program, "texCoord");

	GLuint projUnifBLCLoc = glGetUniformBlockIndex(m_Program, "Projection");
	glUniformBlockBinding(m_Program, projUnifBLCLoc, m_ProjUnifBLCBndIdx);

	GLuint lightCorrUnifBLCLoc = glGetUniformBlockIndex(m_Program, "LightCorrection");
	glUniformBlockBinding(m_Program, lightCorrUnifBLCLoc, m_LightCorrUnifBLCBndIdx);

}


void AShaderBase::throwIfGLError(GLuint p_Obj, bool p_IsProg) const
{
	GLint status;
	p_IsProg ? glGetProgramiv(p_Obj, GL_LINK_STATUS, &status) :
		glGetShaderiv(p_Obj, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE)
	{
		std::string message;
		GLint maxLength;
		p_IsProg ? glGetProgramiv(p_Obj, GL_INFO_LOG_LENGTH, &maxLength) :
			glGetShaderiv(p_Obj, GL_INFO_LOG_LENGTH, &maxLength);
		if (maxLength < 0) maxLength = 32;

		GLchar *strInfoLog = new GLchar[maxLength + 1];
		p_IsProg ? glGetProgramInfoLog(p_Obj, maxLength, &maxLength, &strInfoLog[0]) :
			glGetShaderInfoLog(p_Obj, maxLength, &maxLength, &strInfoLog[0]);


		message.assign(strInfoLog, maxLength);

		delete[] strInfoLog;
		if (p_IsProg) {
			glDeleteProgram(p_Obj);
			throw ShaderException("Shader program", message);
		}
		else {
			glDeleteShader(p_Obj);
			throw ShaderException("Shader", message);
		}
	}
}

StaticMeshShader::StaticMeshShader() : AShaderBase(ComplMaterialActor)
{
	prepareAndCreateProgram(m_ShaderFilenames);
	getLocations();

}


SkyboxShader::SkyboxShader() : AShaderBase(SkyboxActor)
{
	prepareAndCreateProgram(m_ShaderFilenames);
	getLocations();
}


void StaticMeshShader::getLocations()
{

	AShaderBase::getLocations();
	m_NormalLoc = glGetAttribLocation(m_Program, "normal");
	m_TangentLoc = glGetAttribLocation(m_Program, "tangent");
	m_BitangentLoc = glGetAttribLocation(m_Program, "bitangent");
	m_MVmatrixNormalUnifLoc = glGetUniformLocation(m_Program, "MVmatrixNormal");
	m_MVmatrixUnifLoc = glGetUniformLocation(m_Program, "MVmatrix");
	GLuint materialUnifBLCLoc = glGetUniformBlockIndex(m_Program, "Material");
	GLuint lightSourcesUnifBLCLoc = glGetUniformBlockIndex(m_Program, "LightSources");
	GLuint diffuseTexUnif = glGetUniformLocation(m_Program, "textureDiffuseColor");
	GLuint normalTexUnif = glGetUniformLocation(m_Program, "normalMap");

	glUniformBlockBinding(m_Program, materialUnifBLCLoc, m_MtlUnifBLCBndIdx);
	glUniformBlockBinding(m_Program, lightSourcesUnifBLCLoc, m_LgtSrcUnifBLCBndIdx);


	glUseProgram(m_Program);
	glUniform1i(diffuseTexUnif, m_DiffuseTexUnit);
	glUniform1i(normalTexUnif, m_NormalMapTexUnit);
	glUseProgram(0);

}

void SkyboxShader::getLocations()
{
	AShaderBase::getLocations();


	m_VmatrixUnifLoc = glGetUniformLocation(m_Program, "Vmatrix");

	GLuint skyBoxDataUnifBLCLoc = glGetUniformBlockIndex(m_Program, "SkyBoxData");

	glUniformBlockBinding(m_Program, skyBoxDataUnifBLCLoc, m_SkyBoxUnifBlcBndInd);
	
	glUseProgram(m_Program);

	GLuint texUnif = glGetUniformLocation(m_Program, "textureBigClouds");
	glUniform1i(texUnif, m_SkyboxBigCloudsTexUnit);
	texUnif = glGetUniformLocation(m_Program, "textureSmallClouds");
	glUniform1i(texUnif, m_SkyboxSmallCloudsTexUnit);
	texUnif = glGetUniformLocation(m_Program, "textureStars");
	glUniform1i(texUnif, m_SkyboxStarsTexUnit);

	glUseProgram(0);
	
}

FXShader::FXShader() : AShaderBase(FXActor)
{
	prepareAndCreateProgram(m_ShaderFilenames);

	getLocations();


}

void FXShader::getLocations()
{
	AShaderBase::getLocations();


	GLuint diffuseTexUnif = glGetUniformLocation(m_Program, "textureDiffuseColor");

	m_AlphaTimeUnifLoc = glGetUniformLocation(m_Program, "alphaTime");
	m_TexScaleUnifLoc = glGetUniformLocation(m_Program, "texScale");
	m_MVmatrixUnifLoc = glGetUniformLocation(m_Program, "MVmatrix");

	glUseProgram(m_Program);
	glUniform1i(diffuseTexUnif, m_DiffuseTexUnit);
	glUseProgram(0);
}
